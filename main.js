const api_credentials = require('./api_credentials.js');
const Arbgraph = require('./graph/arbgraph.js');
// const Exchanges = require('graph/exchanges.js');
// const Orderbooks = require('graph/Orderbooks.js');
// var socketCluster = require('socketcluster-client');
const Dbaccess = require('./db/dbaccess.js');
var dbo = new Dbaccess();
var request = require('request');
var socketCluster = require('socketcluster-client');

const options = {
  hostname: 'localhost',
  secure: false,
  port: 8000,
  rejectUnauthorized: false // Only necessary during debug if using a self-signed certificate
};

function testfunc1(x) {
  return new Promise(resolve => {
    setTimeout(() => {
      resolve(x * 2);
    }, 2000);
  });
}

function testfunc2(x) {
    return new Promise(resolve => {
      setTimeout(() => {
        resolve(x * 10);
      }, 2000);
    });
  }

  function testfunc3(x) {
    return new Promise(resolve => {
      setTimeout(() => {
        resolve(x * 100);
      }, 2000);
    });
  }

function addPromise(x){
  return new Promise(resolve => {
    testfunc1(x).then((a) => {
        console.log(a);
      testfunc2(a).then((b) => {
          console.log(b);
        testfunc3(b).then((c) => {
            console.log(c);
          resolve(x + a + b + c);
      	});
      });
    });
  });
}



async function addAsync(x) {
    const a = await testfunc1(x);
    const b = await testfunc2(a);
    const c = await testfunc3(b);
    return x + a + b + c;
  }



  var  wfees = {
    "KRKN": {"USDT": 5, "BCH": 0.0001, "DASH": 0.005, "EOS": 0.05, "ETH": 0.005, "LTC": 0.001, "BTC": 0.0005, "XLM": 0.00002, "XMR": 0.05, "XRP": 0.02, "ADA": 0.3},
    "BTHM": {"BCH": 0.001, "EOS": 0.1, "ETH": 0.01, "LTC": 0.01, "BTC": 0.001, "XMR": 0.05, "XRP": 1},
    "PLNX": {"USDT": 10, "BCH": 0.0001, "DASH": 0.01, "EOS": 0.00, "ETH": 0.01, "BTC": 0.0005, "XLM": 0.00001, "XMR": 0.015, "XRP": 0.15},
    "BTRX": {"USDT": 5, "BCH": 0.001, "DASH": 0.05, "ETH": 0.006, "LTC": 0.01, "BTC": 0.0005, "XLM": 0.01, "XMR": 0.04, "XRP": 0.02, "ADA": 0.3},
    "BINA": {"USDT": 3.3, "BCH": 0.0001, "DASH": 0.002, "EOS": 0.1, "ETH": 0.01, "LTC": 0.001, "BTC": 0.0005, "XLM": 0.01, "XMR": 0.0001, "XRP": 0.25, "ADA": 1},
    "HITB": {"USDT": 45, "BCH": 0.0018, "DASH": 0.03, "EOS": 0.01, "ETH": 0.00958, "LTC": 0.003, "BTC": 0.001, "XLM": 40, "XMR": 0.09, "XRP": 0.2},
    "HUBI": {"USDT": 20, "BCH": 0.0001, "DASH": 0.002, "EOS": 0.1, "ETH": 0.005, "LTC": 0.001, "BTC": 0.0005, "XLM": 0.01, "XRP": 0.1, "ADA": 1},
    "LIVE": {"BCH": 0.001, "DASH": 0.002,  "ETH": 0.01,  "BTC": 0.001},
    "EXMO": {"USDT": 5, "BCH": 0.001, "DASH": 0.01,  "ETH": 0.01, "LTC": 0.01, "BTC": 0.0005, "XLM": 0.01, "XMR": 0.05, "XRP": 0.02, "ADA": 1},
    "BTCM": {"BCH": 0.0001, "ETH": 0.001, "LTC": 0.001, "BTC": 0.0001, "XRP": 0.15},
    "CPIA": {"BTC": 0.0005, "ETH": 0.004, "LTC": 0.002,  "USDT": 5},
    "misc": {"minfee": 0.002, "usefees": true},
  };
 
  


  //Filter arbitrage loops and return only Base starting Currency starting point and higher than n expected return
  function filtered_loops(loops, startCurrency, resultThreshold){
    filtered = [];
    if (loops.length > 0) loops.forEach(function(arrayItem){
      if ((arrayItem.starting_asset == startCurrency) & (arrayItem.expected_result >= resultThreshold) & (arrayItem.path.length > 3)) filtered.push(arrayItem)
    });
    if (filtered == []) {
      return -1;
    } else {
      return filtered;
    }
  }

  function get_balance(auth_id,curr){
   //auth_id = 363693; //overriding variable for testing
    console.log('getting http balance...',auth_id,curr)
    return new Promise(resolve => {
    request({
        method: 'POST',
        url: 'https://api.coinigy.com/api/v1/refreshBalance',
        headers: {
          'Content-Type': 'application/json',
          'X-API-KEY': api_credentials.apiKey,
          'X-API-SECRET': api_credentials.apiSecret
        },
        body: JSON.stringify({'auth_id': auth_id})
      }, function (error, response, body) {
        //console.log(body)
          let result = JSON.parse(body)
          result.data.forEach(function (arrayItem) {
            if (arrayItem.balance_curr_code == curr){
              console.log("Retrieved Balances:",arrayItem.balance_curr_code,arrayItem.balance_amount_avail)
              resolve(arrayItem.balance_amount_avail)
            }
          })
          resolve(0)
      });
    });
  }

  //gets all balances and send back an object with all found balances (more efficient than get_balance)
  function get_exch_balances(auth_id){
    auth_id = 363693; //overriding variable for testing
    balances = {}
     //console.log('getting http balance...',auth_id,curr)
     return new Promise(resolve => {
     request({
         method: 'POST',
         url: 'https://api.coinigy.com/api/v1/refreshBalance',
         headers: {
           'Content-Type': 'application/json',
           'X-API-KEY': api_credentials.apiKey,
           'X-API-SECRET': api_credentials.apiSecret
         },
         body: JSON.stringify({'auth_id': auth_id})
       }, function (error, response, body) {
         //console.log('raw response:',body)
           let result = JSON.parse(body)
           result.data.forEach(function (arrayItem) {
             balances[arrayItem.balance_curr_code] = arrayItem.balance_amount_avail;
           })
           resolve(balances)
       });
     });
   }

function add_order(order){
  console.log('sending order:',order)
  return new Promise(resolve => {
    var debug_url = 'https://private-anon-babbd25894-coinigy.apiary-proxy.com/api/v1/addOrder';
    var mock_url =  'https://private-anon-babbd25894-coinigy.apiary-mock.com/api/v1/addOrder';
    var live_url = 'https://api.coinigy.com/api/v1/addOrder';
    request({
        method: 'POST',
        url: live_url,
        headers: {
          'Content-Type': 'application/json',
          'X-API-KEY': api_credentials.apiKey,
          'X-API-SECRET': api_credentials.apiSecret
        },
        // body: "{  \"auth_id\": 1234,  \"exch_id\": 62,  \"mkt_id\": 125,  \"order_type_id\": 1,  \"price_type_id\": 3,  \"limit_price\": 755,  \"order_quantity\": 0.01}"
        body: JSON.stringify(order)
      }, function (error, response, body) {
        // console.log('Status:', response.statusCode);
        // console.log('Headers:', response.headers);
        //console.log(error)
        resolve(JSON.parse(body));
      });
  });
}
//Seems to be successful order format but possibly insufficient balance: (?)
// {"data":{"internal_order_id":"0"},"notifications":[]}
//
// Order_quantity = 0 results in:
// {"err_num":"1057-33-01","err_msg":"Missing or empty parameters:"}

  async function exec_trade(order,tradesize){
    console.log('executing dummy order, returning',tradesize);
    await delay(1000);
    return (tradesize);
  }

  const delay = ms => new Promise(resolve => setTimeout(resolve, ms));
  
  async function get_starting_balances(exch,orders){
    var bals = await get_exch_balances(exch)
    for (let o = 0; o < orders.length; o++) {
      var curr = '';
          if (orders[o].direction == 'Sell'){
            curr = (orders[o].market.split('/',2))[1]
          } else {
            curr = (orders[o].market.split('/',2))[0]
          }
        orders[o]['starting_currency'] = curr;
        orders[o]['starting_balance'] =  bals.curr; //await get_balance(orders[o].auth_id,curr);
    }
    return orders
  }

  // { exch_name: 'Huobi Pro',
  // market: 'TRX/ETH',
  // direction: 'Buy',
  // auth_id: 410776,
  // exch_id: 77,
  // mkt_id: 6530,
  // order_type_id: 1,
  // price_type_id: 3,
  // limit_price: 0.00010863,
  // order_quantity: 346.4043231259526,
  // expected_result: 0.037629901621172225,
  // starting_currency: 'TRX',
  // starting_balance: 0 }

function convert_order(local_order){
 
  var rest_order = {  'auth_id': local_order.auth_id,  'exch_id':  local_order.exch_id,  'mkt_id': local_order.mkt_id,  'order_type_id': local_order.order_type_id,  'price_type_id': local_order.price_type_id,  'limit_price': local_order.limit_price,  'order_quantity': local_order.order_quantity};

  return (rest_order)
}

  async function exec_path(orders) {
    orders = await get_starting_balances(orders[0].auth_id,orders) //should fetch in parallel
    //console.log(orders)
      for (let t = 0; t < orders.length; t++) {
          console.log("Processing Trade:",t+1);
          console.log('Starting Balance:',orders[t].starting_balance,'Quantity Needed:',orders[t].order_quantity)
          var current_balance = orders[t].starting_balance;
          var quantity_traded = 0;
          if (current_balance > 0) {
            do {
            let tradesize = Math.min(Math.max(orders[t].order_quantity-quantity_traded),current_balance);
            let rest_order = convert_order(orders[t])
            //console.log(rest_order)
            rest_order.order_quantity = tradesize;
            //console.log(rest_order)
            let result = await add_order(rest_order);
            console.log('Trade Result:',JSON.stringify(result));
              if (result == -1) {
                  return('Trade Failed. Aborting Path.')
              } else {                 
                  quantity_traded = quantity_traded + result;
                  console.log('Filled:', quantity_traded,'of',orders[t].order_quantity)
                  if (quantity_traded < orders[t].order_quantity) {
                    console.log("Trade not filled. Retrying ...");
                    await delay(1000);
                    current_balance = await get_balance(orders[t].auth_id,orders[t].starting_currency);
                  } else {
                    console.log("Trade Complete.")
                  }
              }
            }
            while (quantity_traded < orders[t].order_quantity) //or time stop   
          } else {
            console.log('Insuficient Balance to execute trade. Aborting order execution.')
          }
        }
          

      let final = await get_balance(363693,'BTC')
      console.log("Path execution successful.");
      return(final)
  };

  async function exec_arbloops(loops) {
    for (let i = 0; i < loops.length; i++) { //this loop probably needs to be an async loop or might process async (see below)
        console.log("Processing Loop Path:",loops[i].path);
        let path_result = await exec_path(loops[i].orders);
        console.log("Loop Result for Path",i,":",path_result)
        console.log('----------------------------------')
    }
};

// Exampe of async URL fetching, then loggin the result in order. 
// Maybe convert urls to orders:
//
// async function logInOrder(urls) {
//   // fetch all the URLs in parallel
//   const textPromises = urls.map(async url => {
//     const response = await fetch(url);
//     return response.text();
//   });

//   // log them in sequence
//   for (const textPromise of textPromises) {
//     console.log(await textPromise);
//   }
// }

  async function setupGraph(exchange,minsize) {
    //await loop();
    
    //----------------Prepare New Graph with current exchanges, markets, accounts and fees -------------------
    myGraph = new Arbgraph(true);

    console.log('Tradesize value:',tradesize,'for',exchange);
    await myGraph.setTradeSize(tradesize,'BTC');
    //console.log('getting exchanges...')
    const exchanges = await dbo.getAllExchanges();
    //console.log('importing exchanges...')
    myGraph.importExchanges(exchanges);
    //console.log(myGraph.exchanges)
     myGraph.importwfees(wfees);
    //console.log('getting markets used...')
    var marketsUsed = await dbo.marketsUsed(exchange); 
    //console.log('Markets used:',marketsUsed)
    var marketDetails = await dbo.addMarketDetails(marketsUsed,dbo.lookupMarket);

    //Extracting the objects in the arrays and merge
    var marketDetailsObj = marketDetails.reduce(function (accumulator, currentValue) {
      accumulator = {...accumulator, ...currentValue}
      return accumulator;
    },{});

    myGraph.importMarkets(marketDetailsObj)
   // console.log(myGraph.markets);

    var accounts = await dbo.getAccounts();
    myGraph.importAccounts(accounts);

  //------------------ Import Orderbooks into Graph -----------------------
   const books = await dbo.getAllBooks(marketsUsed,dbo.getRecentBook);
   await myGraph.importOrderBooks(books);

   //------------- Process Graph and detect infinite arbitrage loops -------
   console.log('Done importing. Processing graph and filter results...');
   let rawloops = await myGraph.getArbLoopBooks(dbo.addLoopsToDB,'BTC');
   let loops = filtered_loops(rawloops,'BTC',0.0001);
   //console.log('loops dump:',loops)//await loop()
   //await exec_loop(loops[0])

//    //----------------Execute Arbitrage Loop Paths ----------------------------
    //Uses new pseudocode and skeleton framework for looping through paths, trades and orders
    await exec_arbloops(loops)

   
   //-------------- For Reporting Only ------------------//
   if (loops.length > 0) {
     console.log('Loops detected:',loops)
      var result = loops[0].quantity_result;
      console.log('Result:',result)
      if (result*12000 > minresult) total+=result; //only take trades greater than minresult USD profit
      console.log('Cummulative Profit:',(total*12000).toFixed(4))

   //--------------Adjust Trade Size up or down ----------//
   if (result > profit){
     tradesize = Math.min(tradesize * 2,maxsize);
     profit = result;
     console.log('Increasing tradesize to:',tradesize);
     console.log('...')
     return tradesize;
   } 
  } else {
    profit = 0;
    tradesize = Math.max(tradesize * 0.5, minsize);
    console.log('reducing tradesize to:',tradesize);
    console.log('...')
    return tradesize;
  }
   
  }



//----------------------Main Program------------------------

//Deprecated: Timer version of arb check. Problem is that loop timer is arbitrary and can start next execution prematurely
async function main(timer){
  const timeoutPromise = (timeout) => new Promise((resolve) => setTimeout(resolve, timeout));
  while(1){
      console.log('ARB Check: ',Date(),process.argv[2]);
      myGraph = new Arbgraph(true);
      setupGraph(process.argv[2]).then(console.err);
      await timeoutPromise(timer*1000);
  }
}




var exchlist = process.argv[2];
var usefees = process.argv[3];
var minsize = process.argv[4];
var maxsize = process.argv[5];
var startsize = process.argv[6];
var minresult = process.argv[7]; //minimum expected trade result in order to take trade

var tradesize = startsize; //BTC
var profit = 0;
var total = 0;

//console.log(process.argv[2])
if (process.argv[2]==null){
  console.log('Command Line args: node main.js exchlist usefees minsize maxsize startsize minresult');
} else {
  if (usefees=='nofees') {
    wfees.misc.usefees = false;
    console.log('fees turned off.')
  } else {
    wfees.misc.usefees = true;
    console.log('using fees.')
  }
  console.log('Exchange:',exchlist)
  
  // Main promoise Loop resolver will run continously after each execution
  Promise.resolve().then(function resolver() {
    return setupGraph(exchlist,minsize)
    .then(console.err)
    .then(resolver);
  })
  // .catch((error) => {
  //   console.log("Error Detected: " + error);
  // });
}



