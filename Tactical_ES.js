/* Tactical ES
Javscript Version 1.0

Binance Data Feed and Execution
Pulls Data on time interval

Futures versions should use real-time websocket Data

To Do: Buy down only on lower price, trade dump, commandline input, Join only if underwater, Fixed Fractional position size, Track by Order ID to work with other strategies
maybe funding check and multi symbol support

 */

const loopback = require('./loopback.js');
const BINANCE = require('./binancetradeservices.js')
const STRATEGIES = require('./strategies.js');
//loopback functions: createstrategy("BINANCE","FUT","TACTICAL_ES","BCHUSDT","1m"), addposition(strategy,newposition), gettotalunits(strategy), getallstrategies(), getstrategy(strategy), getstratsbysymbol(strategy), deletestrategy(strategy)

var psize = 10;
var length = 10;
var length2 = 30;
var mult = 1.5;
var mult2 = 2.5;
var trend = 100;
var log = false;

const BuyLevel = 60;
const SellLevel = 100;


const mavg = require('moving-averages') //ma, dma, ema, sma, wma

//console.log(mavg.ma([1, 2, 3, 4, 5], 2) )

const boll = require('bollinger-bands')

//console.log(boll([1, 2, 4, 8], 2, 2))
// {
//   upper: [, 2.5, 5, 10],
//   mid  : [, 1.5, 3, 6],
//   lower: [, 0.5, 1, 2]
// }
const sd = require('s-deviation');

//console.log(sd([1, 2, 4, 8], 2));         // [<1 empty item>, 0.5, 1, 2]

//console.log(sd([1, 2, 3, 4, 5, 6], 4));
// [
//   <3 empty items>,
//   1.118033988749895,
//   1.118033988749895,
//   1.118033988749895
// ]

//Functions to be moved into Module:

function getcloses(arr) {
  var closes = [];
  arr.forEach(function (bar) {
    closes.push(parseFloat(bar[4]))
  });
  return (closes);
}

function gettrenddir(arr) {
  if (arr[arr.length - 1] > arr[arr.length - 2]) {
    return 1;
  } else {
    return -1
  }
}

function siggen(candles, position, entryprice1) { //Calculate position for given Array Snapshot

  var signal = '***';
  var closes = getcloses(candles);
  var close = closes[closes.length - 1];
  var bb1 = boll(closes, length, mult); //boll(datum, period, sd size, options)
  var bb2 = boll(closes, length2, mult2);
  var lower = bb1.lower[bb1.lower.length - 1];
  var lower2 = bb2.lower[bb2.lower.length - 1];
  var upper = bb1.upper[bb1.upper.length - 1];
  var upper2 = bb2.upper[bb2.upper.length - 1];
  var ma = mavg.ma(closes, trend); //Simple Moving Average: ma(data, size)
  var StdDevIndex = (close - lower) / (upper - lower) * 100;
  var StdDevIndex2 = (close - lower2) / (upper2 - lower2) * 100;
  var oktobuy = false;
  var oktosell = false;
  var oktobuy2 = false;
  var oktosell2 = false;
  var trenddir = gettrenddir(ma);

  if (StdDevIndex > BuyLevel) { oktobuy = false } else { oktobuy = true }
  if (StdDevIndex < SellLevel) { oktosell = false } else { oktosell = true }
  if (StdDevIndex2 > BuyLevel) { oktobuy2 = false } else { oktobuy2 = true }
  if (StdDevIndex2 < SellLevel) { oktosell2 = false } else { oktosell2 = true }

  //console.log('Trend Direction:',trenddir,oktobuy,oktosell,oktobuy2,oktosell2,close,lower,lower2);

  // & (position < unitsize)) { // and strategy.position_size < 1)
  if ((oktobuy == true) & (trenddir == 1) & (close < lower) & (position < 1)) { signal = 'LE1' }
  if ((oktosell == true) & (trenddir == -1) & (close > upper) & (position > -1)) { signal = 'SE1' }
  if ((oktobuy2 == true) & (trenddir == 1) & (close < lower2) & (position == 1) & (close < entryprice1)) { signal = 'LE2' }
  if ((oktosell2 == true) & (trenddir == -1) & (close > upper2) & (position == -1) & (close > entryprice1)) { signal = 'SE2' }


  if ((close > upper) & (position > 0)) { signal = 'LEX' }
  if ((close < lower) & (position < 0)) { signal = 'SEX' }

  return (signal) //will need to change this to object to include price and time data, preferbly use the binance object
}

function printtrades(candles) {
  candles.forEach(function (candle) {
    var date = new Date(candle[0]);
    //alert(date.toString());
    console.log(date.toString(), parseFloat(candle[4]).toFixed(2), candle.signal, candle.position)
  })
}

function calchistorical(candles) {

  var roundOff = (num, places) => {
    const x = Math.pow(10, places);
    return Math.round(num * x) / x;
  }

  var position = 0;
  var entryprice1 = 0;
  var entryprice2 = 0;
  var avgprice = 0;
  var price = 0;
  var netprofit = 0;

  for (i = trend - 1; i < candles.length; i++) { //The reason for this loop is to calculate historical signals all the way up to the present. Nice for analyzing strategy results.

    var candleslice = candles.slice(0, i);
    //var signal = siggen(candleslice, position, entryprice1);
    var strategyresult = STRATEGIES.tactical_es(candleslice, position, entryprice1);
    signal = strategyresult.signal;
    price = parseFloat(candles[i][4]);
    if (signal == 'LE1') { console.log('RECEIVED LE1 Signal'); position = 1; entryprice1 = parseFloat(candles[i][4]) }
    if (signal == 'SE1') { position = -1; entryprice1 = parseFloat(candles[i][4]) }
    if (signal == 'LE2') { position = 2; entryprice2 = parseFloat(candles[i][4]) }
    if (signal == 'SE2') { position = -2; entryprice2 = parseFloat(candles[i][4]) }
    if ((signal == 'LEX') || (signal == 'SEX')) {
      avgprice = 0; position = 0; entryprice1 = 0; entryprice2 = 0;
    }

    candles[i].bartime = candles[i][0];
    candles[i].price = price;
    candles[i].signal = signal;
    candles[i].position = position;
    candles[i].entryprice1 = entryprice1;
    candles[i].entryprice2 = entryprice2;
    if (!(entryprice2 == 0)) { avgprice = (entryprice1 + entryprice2) * 0.5 } else { avgprice = entryprice1 }; // avg price = unitval in loopback
    candles[i].avgprice = avgprice;
    if (avgprice > 0) { netprofit = price - avgprice } else { netprofit = 0 }
    console.log(candles[i][0], i, roundOff(price, 4), signal, position, entryprice1, entryprice2, avgprice, roundOff(netprofit, 4));
  }
  //console.log('Calculation bars: ', candles.length)
  //printtrades(candles)
  return (candles);
}

function sleep(ms) {
  return new Promise(
    resolve => setTimeout(resolve, ms)
  );
}


async function main(symbol, timeframe, psize) {

  const timeoutPromise = (timeout) => new Promise((resolve) => setTimeout(resolve, timeout));
  var exchsymbols = await BINANCE.exchinfo(); //gets details about symbols such as quantity Precision and trade types available (needed for trading)
  console.log(symbol,exchsymbols);
  var quantPrecision = exchsymbols[symbol].quantityPrecision;
  var strategy = loopback.createstrategy('BINANCE', 'FUT', 'TACTICAL_ES', symbol, quantPrecision, arg_interval);

  //console.log('Unit Size: $' + psize + ' | Current Trade Size: ' + quant + ' ' + symbol + ' @ $' + price.toFixed(2));

  while (1) {
    var candles = await BINANCE.getcandles(symbol, timeframe);
    var interval = candles[candles.length - 2][0] - candles[candles.length - 3][0];
    //var livepos = await getposition(symbol); //Change to LB Check
    var candles = calchistorical(candles);
    var price = candles[candles.length - 1].price;
    var currentpos = candles[candles.length - 1].position; //Strategy position
    var signal = candles[candles.length - 1].signal;
    var lastclosetime = candles[candles.length - 1][0];
    var lastclosedate = new Date(lastclosetime);
    var nextclosetime = lastclosetime + interval;
    var nextclosedate = new Date(nextclosetime);
    var currenttime = new Date();
    var timetillclose = (nextclosetime - currenttime + 1000); //delay by 1s to make sure we get latest bar close

    //signal = 'LEX'; currentpos = 0;
    //signal = 'LEX'; currentpos = 0; //Test signal
    
    if (!(signal == '***')) {
      if ((signal == 'LE1') || (signal == 'LE2') || (signal == 'SE1') || (signal == 'SE2')) {
        var units = Number(((psize * currentpos / parseFloat(price))).toFixed(quantPrecision));
        var orders = [{ strategy: strategy, signal: signal, units: parseFloat(units), avgPrice: parseFloat(price), timestamp: lastclosedate }];
        console.log('RAW ORDERs object being fed into sendallstrategyorders: ', orders)
        console.log('Signal:', signal)
        var results = await loopback.sendallstrategyorders(orders);
        //console.log('ORDER result orders:', results.positions);
      }
      if ((signal == 'SEX') || (signal == 'LEX')){
        var closedpositions = await loopback.closeallstrategyorders([{strategy: strategy, signal: signal, avgPrice: parseFloat(price), timestamp: lastclosedate}]); //Doesn't have units but can get it from opening order.
        //var closestatus = await loopback.closeallpositions(strategy);
        console.log('Close Status:',closedpositions);
      }
      var status = await loopback.synclivepositions([strategy]);
      console.log('Sync status:', status)
    }
    console.log('Current Strategy Position: ' + currentpos + ' unit(s) | ' + symbol);
    //console.log('Last Bar:', lastclosedate.toString(), '\nNext Close: ', nextclosedate.toString(), 'Time till close: ', timetillclose / 60000, exec);
    await timeoutPromise(timetillclose);
  }
}

const arg_symbol = process.argv[2];
const arg_interval = process.argv[3];
const arg_psize = process.argv[4];
const arg_log= process.argv[5];

;
if (arg_log == true) { log = true } else { log = false }

console.log(arg_symbol, arg_interval, arg_psize,log)
//var strategy = loopback.createstrategy("BINANCE", "FUT", "TACTICAL_ES", arg_symbol, arg_interval);
//var strategy = loopback.createstrategy('BINANCE', 'FUT', 'TACTICAL_ES', arg_symbol, quantPrecision, arg_interval);

main(arg_symbol, arg_interval, psize);
//getposition('BTCUSDT').then(console.log)
//getpositions(1).then(console.log)

//main(arg_symbol,arg_interval,arg_psize,exec);

//console.log(strategy)
