//Module for Trade Services
var request = require('request');
const crypto = require('crypto');
const api_credentials = require('./api_credentials.js');


function signature(query_string) {
  return crypto
    .createHmac('sha256', api_credentials.binanceSecret)
    .update(query_string)
    .digest('hex');
}

async function getcandles(strategy) {
  //console.log(strategy);
  var symbol = strategy.symbol;
  var interval = strategy.interval;
  var url;
  if (strategy.exchtype == 'FUT') {  url = 'https://fapi.binance.com/fapi/v1/klines?symbol=' + symbol + '&limit=500&interval=' + interval }
  if (strategy.exchtype == 'SPOT') {  url = 'https://api.binance.com/api/v3/klines?symbol=' + symbol + '&limit=500&interval=' + interval }
  return new Promise((resolve, reject) => {
    request({
      method: 'GET',
      url: url,
      headers: {
        'Content-Type': 'application/json',
      },
      body: ""
    }, function (error, response, body) {
      if (!(body == undefined)) {
        var result = JSON.parse(body);
        resolve(result)
      } else {
        //console.log(error, body)
        reject(error)
      }
    });
  });
  /* Result Data is array of arrays
  [
  [
    1499040000000,      // Open time
    "0.01634790",       // Open
    "0.80000000",       // High
    "0.01575800",       // Low
    "0.01577100",       // Close
    "148976.11427815",  // Volume
    1499644799999,      // Close time
    "2434.19055334",    // Quote asset volume
    308,                // Number of trades
    "1756.87402397",    // Taker buy base asset volume
    "28.46694368",      // Taker buy quote asset volume
    "17928899.62484339" // Ignore.
  ]
] */
}

function filteredzero(positions) { //Filter out zero positions
  var filtered = [];
  positions.forEach(function (arrayItem) {
    if (!(parseFloat(arrayItem.positionAmt) == 0)) {
      filtered.push(arrayItem)
    }
  })
  return (filtered)
}

async function getlivepositions_fut(zerobal) { //0 paramater returns all symbols including zero positions. 1 will only return existing positions.

  /*  Returns array of objects:
  [{
    "symbol": "EOSUSDT",
    "positionAmt": "0.0",
    "entryPrice": "0.0000",
    "markPrice": "2.31001768",
    "unRealizedProfit": "0.00000000",
    "liquidationPrice": "0",
    "leverage": "20",
    "maxNotionalValue": "250000",
    "marginType": "cross",
    "isolatedMargin": "0.00000000",
    "isAutoAddMargin": "false",
    "positionSide": "BOTH"
}, ...]*/

  var ts = Date.now();
  //console.log(ts)
  var url = 'https://fapi.binance.com/fapi/v1/positionRisk?timestamp=' + ts + '&signature='
  var uri = 'timestamp=' + ts
  return new Promise((resolve, reject) => {
    request({
      method: 'GET',
      url: url + signature(uri),
      headers: {
        'Content-Type': 'application/json',
        'X-MBX-APIKEY': api_credentials.binanceKey,
      },
      body: ""
    }, function (error, response, body) {
      if (!(body == undefined)) {
        var result = JSON.parse(body);
        if (zerobal == 1) {
          let filtered = filteredzero(result) //just return symbols with positions
          resolve(filtered)
        } else {
          resolve(result)
        }
      } else {
        reject([error, response]);
      }

    });
  });
}

async function getliveposition(symbol) {
  var position = 0.0;
  var allpositions = await getlivepositions(0);
  allpositions.forEach(function (posObj) {
    if (posObj.symbol == symbol) {
      position = parseFloat(posObj.positionAmt);
    }
  })
  return (position);
}



/* SPOT Market order Query String: /api/v3/order?symbol=ETHBTC&side=BUY&type=MARKET&quantity=0.01&newClientOrderId=my_order_id_1&timestamp={{timestamp}}&signature={{signature}}
 result: {
  "symbol": "ETHBTC",
  "orderId": 1026252566,
  "orderListId": -1,
  "clientOrderId": "my_order_id_1",
  "transactTime": 1604970194523,
  "price": "0.00000000",
  "origQty": "0.01000000",
  "executedQty": "0.01000000",
  "cummulativeQuoteQty": "0.00029073",
  "status": "FILLED",
  "timeInForce": "GTC",
  "type": "MARKET",
  "side": "BUY",
  "fills": [
      {
          "price": "0.02907300",
          "qty": "0.01000000",
          "commission": "0.00001000",
          "commissionAsset": "ETH",
          "tradeId": 200542906
      }
  ]
} */

async function sendliveorder(order) { //Sends live market order
  console.log("Sending order: ",order)
  //console.log(order);
  const symbol = order.symbol;
  const side = order.side;
  const quantity = order.quantity;
  const newClientOrderId = order.clientOrderId;
  //const callingStrategy = order.strategy.id;
  var ts = Date.now();
  if (order.strategy.exchtype == 'FUT') {
    var baseurl = 'https://fapi.binance.com/fapi/v1/order'; //Base URL
    var responseType = 'RESULT';
  }
  if (order.strategy.exchtype == 'SPOT') {
    var baseurl = 'https://api.binance.com/api/v3/order'; //Base URL fix this with actual   }/api/v3/order?
    var responseType = 'FULL';
  }
  
  var querystring = 'symbol=' + symbol + '&side=' + side + '&type=MARKET&quantity=' + quantity + '&newClientOrderId=' + newClientOrderId + '&newOrderRespType=' + responseType + '&timestamp=' + ts;
  
  var signed = signature(querystring); //Encode querystring
  var fullurl = baseurl + '?' + querystring + '&signature=' + signed;
  console.log('full url:',fullurl);
  //console.log(fullurl)
  return new Promise((resolve, reject) => {
    request({
      method: 'POST',
      url: fullurl,
      headers: {
        'Content-Type': 'application/json',
        'X-MBX-APIKEY': api_credentials.binanceKey,
      },
      body: ""
    }, function (error, response, body) {
      //console.log('raw output:',response);
      if (!(body == undefined)) {
        try {
        var result = JSON.parse(body);
        } catch {
          console.log('failed to parse:',body);
          var result = 'error';
        }
        //result.callingstrategy = callingStrategy;
        //console.log('Result from Binance:',result)
        resolve(result)
      } else {
        reject([error, response]);
      }

    });
  });
}

/* filled order result from binance {
  "orderId": 4147198332,
  "symbol": "ETHUSDT",
  "status": "FILLED",
  "clientOrderId": "TACTICALES_ETHUSDT_1m",
  "price": "0",
  "avgPrice": "470.53000",
  "origQty": "0.021",
  "executedQty": "0.021",
  "cumQuote": "9.88113",
  "timeInForce": "GTC",
  "type": "MARKET",
  "reduceOnly": false,
  "closePosition": false,
  "side": "BUY",
  "positionSide": "BOTH",
  "stopPrice": "0",
  "workingType": "CONTRACT_PRICE",
  "priceProtect": false,
  "origType": "MARKET",
  "time": 1605115092277,
  "updateTime": 1605115092444
} */

async function sendbatchorders(orders,callingstrategy) {
  return new Promise((resolve, reject) => {
  console.log('total orders received in batch: ',orders.length);
  console.log('Batch received:',orders,callingstrategy.id);
  const promises = [];
  orders.forEach(function (order) {
    //order.newClientOrderId = ; //could use Array number of strategy but need to look up keys for this.
    order.callingStrategy = callingstrategy.id;
    promises.push(sendliveorder(order))
  })
  Promise.all(promises).then((results) => {
    //console.log('results from promise all:',results)
    resolve(results)
  })
})
}




async function batchtrade(batchOrders) {
  var ts = Date.now();
  var orders = createorders(batchOrders);
  var baseurl = 'https://fapi.binance.com/fapi/v1/batchOrders'
  var querystring = 'batchOrders=' + orders + '&timestamp=' + ts
  var signed = signature(querystring); //Encode querystring
  var fullurl = baseurl + '?' + querystring + '&signature=' + signed;
  console.log(fullurl)
  return new Promise((resolve, reject) => {
    request({
      method: 'POST',
      url: fullurl,
      headers: {
        'Content-Type': 'application/json',
        'X-MBX-APIKEY': api_credentials.binanceKey,
      },
      body: ""
    }, function (error, response, body) {
      if (!(body == undefined)) {
        var result = JSON.parse(body);
        resolve(result)
      } else {
        reject([error, response]);
      }

    });
  });
}



//Gets spot/margin ticket info including precision
/* { symbol: 'BTCUSDT',
  status: 'TRADING',
  baseAsset: 'BTC',
  baseAssetPrecision: 8,
  quoteAsset: 'USDT',
  quotePrecision: 8,
  quoteAssetPrecision: 8,
  baseCommissionPrecision: 8,
  quoteCommissionPrecision: 8} */

async function exchinfo(exchtype) { //get Exchange info for ticker Amt Precision 
  //console.log('Exchang Type lookup:',exchtype);
  if (exchtype == 'FUT') {
  var url = 'https://fapi.binance.com/fapi/v1/exchangeInfo';
  return new Promise((resolve, reject) => {
    request({
      method: 'GET',
      url: url,
      headers: {
        'Content-Type': 'application/json',
        'X-MBX-APIKEY': api_credentials.binanceKey,
      },
      body: ""
    }, function (error, response, body) {
      if (!(body == undefined)) {
        let result = JSON.parse(body);
        var resultsf = [];
        result.symbols.forEach(function (arrayItem) { //For building associative array use resultsf intead of results.
          if (!(arrayItem == undefined)) {
            if (!(arrayItem.symbol == 'lookuperr')) { //don't try to add failed lookups
              resultsf[arrayItem.symbol] = arrayItem;
            }
          }
        })
        resolve(resultsf)
      } else {
        reject([error, response]);
      }
    });
  });
} else { //The SPOT exchange info query doesn't include QuantityPrecision variable so we're using manaual result
  var fixedrates = [];
  fixedrates['ETHBTC'] = {quantityPrecision:3}; // Add new pairs here
  return (fixedrates);
}
}

async function getaccountinfo_fut() {
  var ts = Date.now();
  var baseurl = 'https://fapi.binance.com/fapi/v1/account'; //Base URL
  var querystring = 'timestamp=' + ts;
  var signed = signature(querystring); //Encode querystring
  var fullurl = baseurl + '?' + querystring + '&signature=' + signed;
  return new Promise((resolve, reject) => {
    request({
      method: 'GET',
      url: fullurl,
      headers: {
        'Content-Type': 'application/json',
        'X-MBX-APIKEY': api_credentials.binanceKey,
      },
      body: ""
    }, function (error, response, body) {
      if (!(body == undefined)) {
        var result = JSON.parse(body);
        resolve(result)
      } else {
        reject([error, response]);
      }

    });
  });
}

async function symlookup(symbol) {
  
  /*    Sample Query Result:
    {
      "symbol": "BTCUSDT",
      "markPrice": "13383.69000000",
      "indexPrice": "13387.42598644",
      "lastFundingRate": "-0.00003871",
      "interestRate": "0.00010000",
      "nextFundingTime": 1604390400000,
      "time": 1604384110000
  } */
  
    return new Promise((resolve, reject) => {
      request({
        method: 'GET',
        url: 'https://fapi.binance.com/fapi/v1/premiumIndex?symbol=' + symbol,
        headers: {
          'Content-Type': 'application/json',
          'X-API-KEY': api_credentials.binanceKey,
          'X-API-SECRET': api_credentials.binanceSecret,
          'signature': '13f1576a61a116036a48639f5cd613616ae90d9c82b50523bf012492bbd9c0dd'
        },
        body: ""
      }, function (error, response, body) {
        if (!(body == undefined)) {
          var result = JSON.parse(body);
          resolve(result)
        } else {
          reject(error);
        }
      });
    });
  }

  async function multisymlookup(symbols) {
    return new Promise(resolve => {
      const promises = [];
      //const keys = Object.keys(symbols);
      //console.log(keys)
      symbols.forEach(function (symbol) {
        promises.push(symlookup(symbol))
      })
  
      Promise.all(promises).then((results) => {
        var resultsf = [];
        results.forEach(function (arrayItem) { //For building associative array use resultsf intead of results.
          if (!(arrayItem == undefined)) {
            if (!(arrayItem.symbol == 'lookuperr')) { //don't try to add failed lookups
              resultsf[arrayItem.symbol] = arrayItem;
            }
          }
        })
        resolve(results); //for all results in flat object array
      })
    })
  }
//getliveposition('ETHUSDT').then(console.log)
async function getpositions(zerobal) { //0 paramater returns all symbols including zero positions which is needed so we can match with new orders with fewer calls. 1 will only return existing positions.
  /*  Returns array of objects:
    [{
      "symbol": "EOSUSDT",
      "positionAmt": "0.0",
      "entryPrice": "0.0000",
      "markPrice": "2.31001768",
      "unRealizedProfit": "0.00000000",
      "liquidationPrice": "0",
      "leverage": "20",
      "maxNotionalValue": "250000",
      "marginType": "cross",
      "isolatedMargin": "0.00000000",
      "isAutoAddMargin": "false",
      "positionSide": "BOTH"
  }, ...]*/
  
    var ts = Date.now();
    //console.log(ts)
    var url = 'https://fapi.binance.com/fapi/v1/positionRisk?timestamp=' + ts + '&signature='
    var uri = 'timestamp=' + ts
    return new Promise((resolve, reject) => {
      request({
        method: 'GET',
        url: url + signature(uri),
        headers: {
          'Content-Type': 'application/json',
          'X-MBX-APIKEY': api_credentials.binanceKey,
        },
        body: ""
      }, function (error, response, body) {
        if (!(body == undefined)) {
          var result = JSON.parse(body);
          if (zerobal == 1) {
            let filtered = filteredzero(result) //just return symbols with positions
            resolve(filtered)
          } else {
            resolve(result)
          }
        } else {
          reject([error, response]);
        }
  
      });
    });
  }

//Futures Functions
exports.getpositions = getpositions;
exports.symlookup = symlookup;
exports.multisymlookup = multisymlookup;
exports.exchinfo = exchinfo;
exports.getliveposition = getliveposition; //getliveposition('ETHUSDT')
exports.getlivepositions_fut = getlivepositions_fut; //getlivepositions(0) for all (1) for positions excluding flat
exports.sendliveorder = sendliveorder; //sends to binance: sendliveorder(order)
exports.sendbatchorders = sendbatchorders; //send batch orders
exports.getcandles = getcandles;

