/* Funding Backtester V 1.0

Generates Trading View Pine Script Program to backtest funding algorithm.
Divides output into smaller programs due to large arrays

Algorithm downloads historical funding data and buys or sell before the funding time */


//Variables and Required Modules

TestOrders = require('./schemas/TestOrders_s')
PremiumIndex = require('./schemas/PremiumIndex_s')
const { time } = require('console')
const { WSASERVICE_NOT_FOUND } = require('constants');
var request = require('request');
const api_credentials = require('./api_credentials.js');
//toptickers = ['UNIUSDT','SUSHIUSDT','YFIIUSDT','BNBUSDT','CRVUSDT','DEFIUSDT','HNTUSDT','ICXUSDT','WAVESUSDT','FLMUSDT'] //For info purposes. These had the best results in Trading View
toptickers = ['UNIUSDT','YFIIUSDT','SUSHIUSDT','BNBUSDT','CRVUSDT']
const lowrate = -0.002;  //historical filters for funding rates. 
const highrate = 0.002;
const filename = 'top66_';


//Functions

async function getOrders(){ 
    return new Promise(resolve => {
    var query = TestOrders.find().sort({timestamp: -1})
        query.exec(function (err, docs) {
            resolve(docs)
          });
        });
}


function printOrders(priceArray){
    priceArray.forEach( function (priceObj) {
        if (!(priceObj == undefined)){
        //console.log(priceObj);
        var samplesize = priceObj.prices.length;
        var lineout = [priceObj.symbol];
        //console.log(lineout,samplesize)
        var trial = 1;
        while (trial < samplesize){
            //console.log('trial:',trial);
            let profit = (parseFloat(priceObj.prices[trial].price) / parseFloat((priceObj.prices[0].price)) - 1)*100 - 0.21;
            lineout.push(profit.toFixed(7));
            trial++;
        }
        console.log(lineout.toString());
        };
    });
    console.log('Done.')
}

async function getPremiums(){ 
    return new Promise(resolve => {
    var query = PremiumIndex.find({symbol: 'YFIIUSDT'}).sort({timestamp: -1})
        query.exec(function (err, docs) {
            resolve(docs)
          });
        });
}


//  Get Historical Funding Rates, returns an Array if Price Objects
async function gethistorical(symbol){
  
  return new Promise((resolve, reject) => {
  request({
      method: 'GET',
      url: 'https://fapi.binance.com/fapi/v1/fundingRate?symbol=' + symbol,
      headers: {
        'Content-Type': 'application/json',
        'X-API-KEY': api_credentials.binanceKey,
        'X-API-SECRET': api_credentials.binanceSecret,
        'signature': '13f1576a61a116036a48639f5cd613616ae90d9c82b50523bf012492bbd9c0dd'
      },
      body: ""
    }, function (error, response, body) {
      if (!(body == undefined)){
        var result = JSON.parse(body);
        resolve(result)
      } else {
        //console.log('Bad Symbol: ',symbol,' Result: ',body)
        //reject('Problem Symbol:' + symbol);
        resolve({symbol: symbol, fundingTime: -1, errormsg: body}) //custom error so that entire promise all doesn't fail and crash script.
      }
     
    });
    });
}

async function gethistoricals(symbols){
  return new Promise(resolve => {
  const promises = [];
  symbols.forEach(function (symbol){
    promises.push(gethistorical(symbol))
  })
  Promise.all(promises).then((results)=> {

    resultsf = [];
    rejectsf = [];
    let goodcount = 0;
    let badcount = 0;
    results.forEach(function(arrayItem){
      if (arrayItem.fundingTime == -1){
        console.log(arrayItem)
        rejectsf.push(arrayItem.symbol)
        badcount ++
      } else {
        goodcount ++
        resultsf.push(arrayItem)
      }
    })
    console.log("Good records:",goodcount," Bad Records:", badcount)

    resolve(resultsf); //for all results in flat object array
  })
})
}

async function gettickers(detail){
  return new Promise(resolve => {
  request({
      method: 'GET',
      url: 'https://fapi.binance.com/fapi/v1/fundingRate?symbol=&limit=100',
      headers: {
        'Content-Type': 'application/json',
        'X-API-KEY': api_credentials.binanceKey,
        'X-API-SECRET': api_credentials.binanceSecret,
        'signature': '13f1576a61a116036a48639f5cd613616ae90d9c82b50523bf012492bbd9c0dd'
      },
      body: ""
    }, function (error, response, body) {
      //console.log('Status:', response.statusCode);
      // console.log('Headers:', response.headers);
      // console.log('Response:', JSON.parse(body));
      var result = JSON.parse(body);
      //result.sort((a, b) => (parseFloat(a.fundingRate) > parseFloat(b.fundingRate)) ? 1 : -1)
      /* result.forEach(function (arrayItem) {
              if (arrayItem.fundingRate > maxrate || arrayItem.fundingRate < minrate) {
                  console.log(arrayItem);
              }
      }); */
      //Need to de-duplicate and keep only latest rate
      var resultArr = [];

      if (detail == 0){
        //console.log('Flat array with only symbols')
        //Creates a flat array with just the tickers
        result.forEach(function(ArrayItem){
          resultArr.push(ArrayItem.symbol)
        })
      } else {
      //Creates an associative array with real-time fundingTime and funding Rate
        result.forEach(function(ArrayItem){
          if (ArrayItem.symbol in resultArr){ //if it's in the array then check for latest and update
           if (ArrayItem.fundingTime > resultArr[ArrayItem.symbol].fundingTime){
             resultArr[ArrayItem.symbol] = ArrayItem;
          }
        } else {
          resultArr[ArrayItem.symbol] = ArrayItem;
        }
      })
      }
      resolve(resultArr)
    });
    });
}


//Output to text file
function fileout(arr,multi,filename,fileno){

  const fs = require('fs')

  var content = `
// This source code is subject to the terms of the Mozilla Public License 2.0 at https://mozilla.org/MPL/2.0/
// © gpl3000
  
//@version=4
strategy("Funding Rate Stategy ${fileno}", overlay=true)
  
//Input Variables
i_lowRate = input(-0.001) //Minimum Rate % Threshold for Entry, lower means higher yield for long positions
i_highRate = input(0.00) //Higher means yield on short positions
i_entryMinutes =input(60) //Minutes before the next funding
i_exitMinutes = input(15) // Not used
i_exitBars = input(11) // exit bars after entry (default is based on 5 minute bars)
  
//Initialize Funding Arrays
strategyPos = array.new_int(0)
fundingRate = array.new_float(0)
fundingTime = array.new_int(0)
  
  
//Populate Funding Arrays
  `

  //var count = 0;
  var outerArr = [];
  if (multi == 0) {
    outerArr.push(arr)
  } else {
    outerArr = arr
  }

  var tickers = [];

  outerArr.forEach(function(arrayItem){
    tickers.push("BINANCE:" + arrayItem[0].symbol + "PERP")
  })
  content += "//  " + tickers + "\n"

  outerArr.forEach(function(innerArr) {
    let count = 0;
    content += "if (syminfo.tickerid == \"BINANCE:" + innerArr[0].symbol + "PERP\")\n";
    innerArr.forEach(function(arrayItem) {
      content += '    array.push(fundingTime,' + arrayItem.fundingTime + ')\n';
      content += '    array.push(fundingRate,' + arrayItem.fundingRate + ')\n';
    });
  });

  content += `
  
//Global variables
var barsInTrade = 0
var count = array.size(fundingRate)
trades = 0
var symChartMatch = 0
var symDataMatch = 0

//Functions
f_get_rate(_barTime) =>
    _fundingRate = 0.0000
    _fundingTime = 0
    _idx = 0
    for _j = 0 to count - 1
        _fundingTime := array.get(fundingTime, _idx)
        if (_barTime < _fundingTime)
            if (_idx == 0)
                _idx := 1
            _fundingRate := array.get(fundingRate, _idx - 1)
            _fundingTime := array.get(fundingTime, _idx - 1)
            break
        _idx := _idx + 1
    [_idx - 1, _barTime ,_fundingTime, _fundingRate]

//Calculations
if (syminfo.tickerid == 'BINANCE:ICXUSDTPERP')
    symChartMatch := 1
else
    symChartMatch := 0
    
    
longPosition = (strategy.position_size > 0)

t = time(timeframe.period, "0930-1600")

if not (strategy.position_size == 0)
    barsInTrade:= barsInTrade + 1
else
    barsInTrade:=0
    
[idx0,barTime0,fundingTime0,fundingrate0] = f_get_rate(time)

//Trading Logic
exitCondition = (time >= (fundingTime0 + (8 * 60 * 60 * 1000) - (i_exitMinutes * 1000 * 60)))
exitCondition2 = ((strategy.position_size > 0) and (barssince(strategy.position_size > 0) > 4))
exitCondition3 = (barsInTrade >= i_exitBars)

longCondition = ((fundingrate0 < i_lowRate) and (time >= (fundingTime0 + (8 * 60 * 60 * 1000) - (i_entryMinutes * 1000 * 60))) and (time < (fundingTime0 + (8 * 60 * 60 * 1000) - (i_exitMinutes * 1000 * 60))))
if (longCondition)
    strategy.entry("Entry", strategy.long)


shortCondition = ((fundingrate0 > i_highRate) and (time >= (fundingTime0 + (8 * 60 * 60 * 1000) - (i_entryMinutes * 1000 * 60))) and (time < (fundingTime0 + (8 * 60 * 60 * 1000) - (i_exitMinutes * 1000 * 60))))
if (shortCondition)
    strategy.entry("Entry", strategy.short)

strategy.close_all(when = exitCondition3, comment = "close all entries")


//Plots
plotchar(idx0, "Bar Index", "", location = location.top)
plotchar(barTime0, "Bar Time", "", location = location.top)
plotchar(fundingTime0, "Funding Time", "", location = location.top)
plotchar(fundingrate0, "Funding Rate", "", location = location.top)
plotchar(bar_index, "Bar Index", "", location = location.top)
plotchar(longPosition, "Position", "", location = location.top)
plotchar(barsInTrade, "Bars in trade", "", location = location.top)
plotchar(symChartMatch, "Symbol / Data Match", "", location = location.top)
`
 
  fs.writeFile('/Users/greglindberg/arbot/dump/' + filename, content, err => {
    if (err) {
     console.error(err)
      return
    }
   console.log(tickers.length,' Symbols processed: ');
   console.log(tickers);
    })

}

function findDupes (sourceArr){
  // Find duplicates using for loop

let duplicate = sourceArr.reduce((acc,currentValue,index, array) => {
  if(array.indexOf(currentValue)!=index && !acc.includes(currentValue)) acc.push(currentValue);
  return acc;
}, []);

if (duplicate.length > 0){
  console.log(duplicate.length,' duplicates found:' + duplicate.join(','));
} else {
  console.log(console.log('No duplciates found.'))
}
}



function removeDuplicateUsingFilter(arr){
  let unique_array = arr.filter(function(elem, index, self) {
      return index == self.indexOf(elem);
  });
  return unique_array
}


function filteredbyrate(lowrate,highrate,preArray){
var lowarrcount = 0;
var higharrcount = 0;
var result = [];
  preArray.forEach(function(Arr){
    let lowreccount = 0;
    let highreccount = 0;
    Arr.forEach(function(fundingRec){
      if (fundingRec.fundingRate < lowrate){
        lowreccount ++
      }
      if (fundingRec.fundingRate > highrate){
        highreccount ++
      }
    })
    if ((lowreccount > 0) || (highreccount > 0)){
      result.push(Arr)
    }
    if (lowreccount > 0) {
      lowarrcount ++
    }
    if (highreccount > 0) {
      higharrcount ++
    }
  })
  console.log("Tickers with low funding:",lowarrcount)
  console.log("Tickers with high funding:",higharrcount)
  return(result)
}

async function main(lowrate,highrate){
    //var orders = await getOrders();
    //console.log(orders)
   // await printOrders(orders);

   //If we only one to get one symbol for testing use this
  //var symbol ='YFIUSDT';
  //var premium = await gethistorical(symbol);
  //console.log(premium)
  //await fileout(premium,0);

  
  //For multiple symbol output to file:
  /* var symbols = await gettickers(0);
  symbols = removeDuplicateUsingFilter(symbols); */
  var symbols = toptickers;


  //findDupes(symbols);
  //var split = (symbols.length * 0.5);
  //console.log(symbols)
  //var symbols = ['YFIUSDT','BTCUSDT']
  //var premiums1 = await gethistoricals(symbols.slice(0,split));
  //var premiums2 = await gethistoricals(symbols.slice(split, symbols.length));
  //const histpremiums =  [].concat(premiums1, premiums2)
  //console.log(histpremiums)
  var histpremiums = await gethistoricals(symbols);
  var histpremiums = filteredbyrate(lowrate,highrate,histpremiums);
  console.log('Tickers Found:',histpremiums.length);
  
  var slicesize = 5;
  var slices = histpremiums.length / slicesize;
  var i;
  for (i = 0; i < slices; i++) {
    let fn = filename + (i + 1) + '.js'
    let bslice = i * slicesize;
    let nslice = (i + 1) * slicesize;
    console.log('writing file for lines: ',bslice,nslice)
    await fileout(histpremiums.slice(bslice,nslice),1,fn,(i + 1));
  }
  //await fileout(histpremiums.slice(0,split),1,'pineout1.js');
  //await fileout(histpremiums.slice(split,split * 2),1,'pineout2.js');
  //await fileout(histpremiums.slice(split * 2,split * 3),1,'pineout3.js');
  //await fileout(histpremiums.slice(split * 3,histpremiums.length),1,'pineout4.js');
   //console.log(premium)
  
/*    console.log("lastFundrate = array.new_float(0)");
    premiums.forEach(function(item){
        console.log("array.push(lastFundrate,"+item.lastFundingRate+")\n")
    });
    console.log("lastFundrate = array.new_int(0)");
    premiums.forEach(function(item){
        console.log("array.push(fundingTime,"+item.time+")\n")
    });
    console.log("lastFundrate = array.new_int(0)");
    premiums.forEach(function(item){
        console.log("array.push(nextFundingTime,"+item.nextFundingTime+")\n")
    });
    console.log("lastFundrate = array.new_float(0)");
    premiums.forEach(function(item){
        console.log("array.push(markPrice,"+item.markPrice+")\n")
    }); */
    
   //console.log("array.push(a, close)" + premiums.length)

   //console.log(premiums)
}
//printOrders(sample)
//getOrders(printOrders);
main(lowrate,highrate);
//db.close();



    