
db = require('./database')
ExchangeModel = require('../schemas/Exchange_s')
OrderbookModel = require('../schemas/Orderbook_s')
MarketModel = require('../schemas/Market_s')
AccountModel = require('../schemas/Account_s')
LoopModel = require('../schemas/Loop_s')
// const EventEmitter = require('events');
// const stream = require('stream-to-mongo-db').streamToMongoDB;


class Dbaccess {

    constructor(){
        this.exchanges = [];
        this.currencies = [];
        this.orderbooks = [];
        this.exchdetails = [];
        this.markets = [];
        this.accounts = [];
        this.minfee = 0;
        this.reversed_exchanges = [];

        console.log('contructor initiated')

        // this.dbo_exchanges = dbo_exchanges
        // this.db = require('./database')
        // this.ExchangeModel = require('./Exchange_s')
    }

    addExchToDB(data){
        console.log('Exchanges found: ', Object.keys(data).length)
        if(typeof data !== "undefined") {
            data.forEach(function (arrayItem) {
                let exch = new ExchangeModel({
                    exch_code: arrayItem.exch_code,
                    exch_id: arrayItem.exch_id,
                    exch_name: arrayItem.exch_name,
                    exch_fee: arrayItem.exch_fee,
                    exch_trade_enabled: arrayItem.exch_trade_enabled,
                    exch_balance_enabled: arrayItem.exch_balance_enabled,
                    exch_url: arrayItem.exch_url
                  })
                  exch.save()
                  .then(doc => {
                    console.log(doc)
                  })
                  .catch(err => {
                    console.error(err)
                  })
            })
        }	
    }

    dbo_addAccounts(data){
      //console.log('Exchanges found: ', Object.keys(data).length)
      if(typeof data !== "undefined") {
          data.forEach(function (arrayItem) {
              let account = new AccountModel({
                auth_id: arrayItem.auth_id,
                auth_key: arrayItem.auth_key,
                auth_optional1: arrayItem.auth_optional1,
                auth_nickname: arrayItem.auth_nickname,
                exch_name: arrayItem.exch_name,
                auth_secret: arrayItem.auth_secret,
                auth_updated: arrayItem.auth_updated,
                auth_active: arrayItem.auth_active,
                auth_trade: arrayItem.auth_trade,
                exch_trade_enabled: arrayItem.exch_trade_enabled,
                exch_id: arrayItem.exch_id
                })
                account.save()
                .then(doc => {
                  console.log(doc)
                })
                .catch(err => {
                  console.error(err)
                })
          })
      }	
  }

    addOrdersToDB(data, channel) { //Add new trade data pairs from web sockets to database
        var result = 0
        //console.log('.')
        if ((!(data == undefined)) && (!(data[0].exchange == undefined)) && (!(data[0].market == undefined))){
            var exchange = data[0].exchange;
            var market = data[0].market;
            var timestamp = data[0].timestamp;
            var ordersarray = []

            data.forEach(function (arrayItem) {
                var order = {ordertype: arrayItem.ordertype, price: arrayItem.price, quantity: arrayItem.quantity, total: arrayItem.total}
                ordersarray.push(order)
            })  
            console.log('creating orderbook Model: ', exchange,timestamp,market,channel,ordersarray.length, data)
            let orderbook = new OrderbookModel({ //<--- need to add mkt_id and exchmkd_id for easy lookup later
                exch_code: exchange,
                market: market,
                timestamp: timestamp,
                orders: ordersarray
            })
            orderbook.save()
                  .then(doc => {
                    console.log('saving orderbook to DB: ', channel)
                  })
                  .catch(err => {
                    console.error('save failed: ',err,channel,data)
                  })
            } else {
                console.log('INVALID orderbook: ', channel,data)
            }
    }

    addOrdersToDB2(data, channel) { //Add new trade data pairs from web sockets to database
        var result = 0
        //console.log('.')
        if ((!(data == undefined)) && (!(data[0].exchange == undefined)) && (!(data[0].market == undefined))){
            var exchange = data[0].exchange;
            var market = data[0].market;
            var timestamp = data[0].timestamp;
            var ordersarray = []

            data.forEach(function (arrayItem) {
                var order = {ordertype: arrayItem.ordertype, price: arrayItem.price, quantity: arrayItem.quantity, total: arrayItem.total}
                ordersarray.push(order)
            })  
            console.log('creating orderbook Model: ', exchange,timestamp,market,ordersarray.length)
            var orderbook = {
                exch_code: exchange,
                market: market,
                timestamp: timestamp,
                orders: ordersarray
            }
            ExchangeModel.find({ exch_code: exchange}, {'pairs.name':market}, function (err, doc){ 
                if(err){console.log('exchange pair not found:',exchange,market)}
                //console.log(doc.pairs.name,err)
                if (!(doc==null)){
                    //doc.orderbooks.push(orderbook)
                    console.log('found exchange...', market, Object.keys(doc))
                }
                //count ++
            })
        }
    }
 
    addsampleorderbook(data,channel){
        let orderbook = new OrderbookModel({
            exch_code: 'TEST',
            market: 'USD/USD',
            timestamp: '10/10/2000',
            orders: [{ordertype: 'Buy', price: 1, quantity: 100, total: 100}]
        })
        orderbook.save()
              .then(doc => {
                console.log('saving orderbook to DB: ', channel)
              })
              .catch(err => {
                console.error('save failed: ',err,channel,data)
              })

    }

    updateChannels(exchange,channels){
      let query = {exch_name: exchange}
      ExchangeModel.findOneAndUpdate(query, { 'channels': channels }, console.log)
    }

    addLoopsToDB(loops){
      //console.log('Loops to add DB:',loops.length,JSON.stringify(loops))
      loops.forEach(function(loop){
        let oneLoop = new LoopModel(loop)
        oneLoop.save()
            .then(doc => {
              //console.log('saving loop to DB: ') //,doc
            })
            .catch(err => {
              console.error('save failed: ',err)
            })
        })
    };

    
    addChannelsFromCache(filename){
        var fs = require('fs');
 
        var contents = fs.readFileSync(filename, 'utf8');
        this.addChannelsToDB(contents);
    }

    addChannelsToDB(input){
        //console.log('addChannels to DB function', data)
        //if ((!(data == undefined)) && (!(data.channel == undefined)) && (!(typof(data) == undefined))){
        var count = 1;
        //var data = input.slice(0,20)
        //var data = JSON.parse(input)
        var data = input
        if ((!(data == undefined)) && (!(typeof(data) == undefined))){
            //console.log('adding channels...')
            //copied 
            //console.log('records to be added: ', data.length)
            data.forEach(function (arrayItem) {
                if (!(arrayItem['channel'].search("ORDER") == -1)){
                    var channel = arrayItem['channel']
                    var matches = arrayItem['channel'].replace(/--/g,'-').split("-", 4);
                    var exch_code = matches[1]
                    var name = matches[2] + '/' + matches[3]
                    var from = matches[2]
                    var to = matches[3]  
                    var pair = {   
                        name: name,
                        from: from,
                        to: to,
                        enabled: true,
                        channel: channel} 
                    ExchangeModel.findOne({ exch_code: exch_code }, function (err, doc){
                        
                        doc.pairs.push(pair)
                        console.log('saving channel...', count)
                        count ++
                        doc.save();
                        //db.gc('ExchangeModel','ExchangeModels')
                        // delete mongoose.models['Exchange'];
                        // delete mongoose.connection.collections['Exchange'];
                        // delete mongoose.modelSchemas['Exchange'];

                      });

                    // ExchangeModel.findOneAndUpdate({ exch_code: exch_code},  // search query, 
                    // {
                    // pairs: pairs.push(pair)   // field:values to update
                    // },
                    // {
                    // new: true,                       // return updated doc
                    // runValidators: true              // validate before update
                    // })
                    // .then(doc => {
                    //     console.log(doc)
                    // })
                    // .catch(err => {
                    //     console.error(err)
                    // })

                }
            //copied
        }) 
        }
        console.log('exited channels add')
        return true
    }


    getorderbook(settings){
        var promise = OrderbookModel
        .find({
          exch_code: settings[0],
          market: settings[1], // search query
        }).sort({timestamp: -1})
        .then(doc => {
            console.log( doc[0])
        })
        .catch(err => {
          return failure(err)
        })
       return promise
    }

    getorderbook2(settings){
        var promise = OrderbookModel.find({
          exch_code: settings[0],
          market: settings[1], // search query
        }).sort({timestamp: -1}).then(doc => {
            return doc[0]
        }).catch(err => {
        return (err)
        })
    }

    doSomething() {
        // some code
        return {'1':'2'};
    }

    doSomething2(){
        var async = require('async');

        async function x (req, res) {
            var user;
            await users.findOne({}, function(err,pro){
                user=pro;
              });
            console.log(user); // it's define
          };
    }

    getAllExchanges() { //return all the exchangs in the database unfiltered
        return new Promise(resolve => {
            var query = ExchangeModel.find({}).sort({exch_code: -1})
            query.lean().exec(function (err, docs) {
                  var exchdict = {}
                  docs.forEach(function (arrayItem) {
                    exchdict[arrayItem.exch_code] = arrayItem
                  })
                 resolve(exchdict);
                })
            })
    }

    
    getAccounts() {
      return new Promise(resolve => {
        var query = AccountModel.find({}).sort({exch_id: -1})
        query.lean().exec(function (err, docs) {
              var accounts = {}
              docs.forEach(function (arrayItem) {
                accounts[arrayItem.exch_id] = arrayItem
              })
             resolve(accounts);
            })
        })
    }

    marketsUsed(exchange){ //read all orderbooks and aggregate based on exchange and market
      //console.log("marksedUsed DBO ***",exchange)
        return new Promise(resolve => {
        var query = OrderbookModel.aggregate( [ 
        { $group:
            {
              _id: { exch_code: "$exch_code", market: "$market"},
            }
        },
         ] )
        query.exec(function(err, docs) {
            var allgroups = []
            docs.forEach(function (arrayItem) {
              //console.log(arrayItem)
              if (exchange !== undefined){
                if (arrayItem._id.exch_code == exchange){
                  //console.log("Markets Used:",arrayItem._id.exch_code,arrayItem._id.market)
                  allgroups.push(arrayItem._id)
                } else {
                  //console.log("Not Equal:",arrayItem._id.exch_code,exchange)
                }
              } else {
                //console.log("Markets Used:",arrayItem._id.exch_code,arrayItem._id.market)
                allgroups.push(arrayItem._id)
              }
            })
            //console.log('Markets Used:',allgroups)
            resolve(allgroups);   
            })
        })
    }
    
    getRecentBook(ticker){ //return the latest orderbook for a ticker
        return new Promise(resolve =>{
            var query = OrderbookModel.find({
                exch_code: ticker.exch_code,
                market: ticker.market, // search query
              }).sort({timestamp: -1})
              query.lean().exec(function (err, docs) {
                //resolve(docs[0]._doc)
                resolve(docs[0])
              }) //query 
        }) //return promise
    } //getRecentBook

    lookupMarket(ticker){ //return market details for a ticker
      return new Promise(resolve =>{
          var query = MarketModel.find({
              exch_code: ticker.exch_code,
              mkt_name: ticker.market, // search query
            })
            query.lean().exec(function (err, docs) {
              //resolve(docs[0]._doc)
              //console.log('Found ID:',docs[0].mkt_id)
              var marketID = ticker.exch_code + '_' + ticker.market;
              var newMarket = []
              newMarket[marketID] = docs[0];
              //console.log(newMarket)
              resolve(newMarket)
            }) //query 
      }) //return promise
  } //lookupMarket

  addMarketDetails(tickers,lookupMarket){
    var allPromises = [];
    tickers.forEach(function(ticker){
      allPromises.push(lookupMarket(ticker))
      // var result = lookupMarket(ticker);
      // console.log(result)
      // allPromises[result[0]] = result[1];
    })
    return Promise.all(allPromises)
  }

    getAllBooks(tickers, getRecentBook){ //get all the latest orderbooks
        var allPromises = []
        tickers.forEach(function (ticker) {
          //console.log(ticker)
            allPromises.push(getRecentBook(ticker))
        })
        return Promise.all(allPromises)
    }

    // getAllRecentBooks(tickers, getOne) {
    //     var p = Promise.resolve();
    //     return new Promise (resolve => {
    //     var books = [];
    //     //var p = Promise.resolve();
    //     //for (let i = 0; i < tickers.length; i++) {
    //     tickers.forEach(function (arrayItem) {
    //             //             exchdict[arrayItem.exch_code] = arrayItem
    //             //           })
    //         p = p.then(_ => new Promise(resolve =>
    //             // setTimeout(function () {
    //             //     //console.log(i);
    //             //     resolve(i);
    //             // }, Math.random() * 1000)

    //                 resolve(getOne(arrayItem))
                
    //         ))//Promise then
    //     } )
    //         console.log(p)//for
    //         resolve(p)
    //       }) 
    //         resolve(p)
    //         //new promise
    //     //console.log(books)
        
       
        
    //     //return new Promise(resolve => {
    // //var settings = {exchange: arrayItem._id.exch_code, market: arrayItem._id.market}
    //     // var recentbooks = []
    //     // tickers.forEach(function (arrayItem) {
    //     //     const promise = func(arrayItem);
    //     //     promise.then(console.log)
    //         //console.log(recentbooks)
    //             // var query = OrderbookModel.find({
    //             //       exch_code: arrayItem.exch_code,
    //             //       market: arrayItem.market, // search query
    //             //     }).sort({timestamp: -1})

    //             //     query.exec(function (err, docs) {
    //             //         recentbooks[docs[0].exmarket] = docs[0]._doc
    //             //         //console.log(docs[0]._doc)
    //             //         //console.log(books[docs[0].exmarket])
    //             //         //docs[0]._doc.exch_fee = exchanges[docs[0]._doc.exch_code].exch_fee
    //             //         //console.log(docs[0].exmarket, books[docs[0].exmarket])
    //             //         //cb(docs[0])
    //             //         //this.updated.push(docs[0])
    //             //         //books[docs[0]._doc.exmarket]
    //             //         //resolve(docs[0]._doc)
    //             //       })
    //             //     })
    //         //console.log(recentbooks)
    //         //resolve(recentbooks);
    //     // }) 
    //     // return(recentbooks);//promise
    // //})
    // }
    


    // getAllExchanges(cb){
        
    //     var query = ExchangeModel.find({}).sort({exch_code: -1})
    //     query.exec(function (err, docs) {
    //           var exchdict = {}
    //           docs.forEach(function (arrayItem) {
    //             exchdict[arrayItem.exch_code] = arrayItem
    //           })
    //           cb(exchdict)
    //           //return exchdict
    //           //console.log(exchdict)
    //           //return {'1':'5'}
    //         }) 
    //         //cb(query.docs)
    //         //return {'1':'5'}
    //         //return exchdict
    // }

    getAllPairs(settings,cb){

    }

    getAllBooksRecent(settings,cb){
        
    }

    getUnique(exchanges,cb){
        var query = OrderbookModel.aggregate( [ 
        { $group:
            {
              _id: { exch_code: "$exch_code", market: "$market"},
            }
        },
         ] )
        query.exec(function(err, docs) {
            //updated = {}
            docs.forEach(function (arrayItem) {
                var settings = {exchange: arrayItem._id.exch_code, market: arrayItem._id.market, exchanges: exchanges}
                //getOrderBookRecent(options,function (){this.updated.push()})
              //  function getOrderBookRecent(settings, cb){ 
                    var query = OrderbookModel.find({
                      exch_code: settings.exchange,
                      market: settings.market, // search query
                    }).sort({timestamp: -1})
                    query.exec(function (err, docs) {
                        //docs[0]._doc.exch_fee = exchanges[docs[0]._doc.exch_code].exch_fee
                        //console.log(docs[0])
                        cb(docs[0])
                        //this.updated.push(docs[0])
                      })
              //  }

                
                //getOrderBookRecent(options,function(){updated.push()})

            })
            
        })
       return query.exec()
    }


    getOrderBookRecent(settings, cb){ 
        var query = OrderbookModel.find({
          exch_code: settings.exchange,
          market: settings.market, // search query
        }).sort({timestamp: -1})
        query.exec(function (err, docs) {
            cb(docs[0])
          })
    }

    getBookDepth(options, cb){
        var query = OrderbookModel.find({
          exch_code: options.exchange,
          market: options.market, // search query
        }).sort({timestamp: -1})
        query.exec(function (err, docs) {
            //cb(docs[0].bid)
            var quantity = 0
            var avgprice = 0
            function getDepth(orders,tradesize,dir){ 
                var vol = 0
                var depth = -1
                var sellbook = []
                var buybook =[]
                var book = []
                if ((typeof orders !== "undefined") && !(orders == 1)){
                  orders.forEach(function (arrayItem) {
                  if (arrayItem.ordertype == 'Sell') {
                      sellbook.unshift(arrayItem) 
                    }
                    if (arrayItem.ordertype == 'Buy') {
                      buybook.push(arrayItem) 
                    }
                })  
                }
                if (dir == 'Sell') book = buybook 
                  else book = sellbook;
              
                if(typeof book !== "undefined") {
                  book.forEach(function (arrayItem) {
                    vol = vol + parseFloat(arrayItem.total)
                    if (tradesize <= vol && depth == -1) {
                      depth = arrayItem.price
                      console.log(arrayItem.quantity)
                      quantity = quantity + ((tradesize - (vol - parseFloat(arrayItem.total))) / arrayItem.price)
                    }
                    if (tradesize > vol) {quantity = quantity + arrayItem.quantity}
                })
                }
                return depth
            }
            var depth = getDepth(docs[0].orders,options.quantity,options.ordertype)
            avgprice = options.quantity / quantity 
            cb(depth, quantity, avgprice)
          })
    }

    getbookstream(settings){
        // var s = OrderbookModel.find({
        //     exch_code: settings[0],
        //     market: settings[1], // search query
        //   }).sort({timestamp: -1}).stream()
        console.log(OrderbookModel.find({ exch_code: settings[0]}))
        return null;
     }
    // var currs = market.split("/", 2);
            // var from_curr = exchange + '_' + currs[0];
            // var to_curr = exchange + '_' + currs[1];
    
            // if (!(Object.keys(this.currencies).includes(currs[0]))){ //if it's a new currency rebuild links
            //     this.currencies[currs[0]] = []
            // }
            // if (!(Object.keys(this.currencies).includes(currs[1]))){
            //     this.currencies[currs[1]] = []
            // }
            // if (!(this.currencies[currs[0]].includes(exchange))){
            //     this.currencies[currs[0]].push(exchange)
            // }
            // if (!(this.currencies[currs[1]].includes(exchange))){
            //     this.currencies[currs[1]].push(exchange)
            // }

            // if (!(Object.keys(this.exchanges).includes(exchange))){ 
            //     this.exchanges[exchange] = []
            // }
            
            // if (!(this.exchanges[exchange].includes(market))){
            //     this.exchanges[exchange].push(market)
            // }

            // if (!(Object.keys(this.markets).includes(market))){ 
            //     this.markets[market] = []
            // }
            
            // if (!(this.markets[market].includes(exchange))){
            //     this.markets[market].push(exchange)
            // }


            // var currs = market.split("/", 2);
            // var from_curr = exchange + '_' + currs[0];
            // var to_curr = exchange + '_' + currs[1];

            // this.update_stor(from_curr,to_curr,orders) //need to add for reverse pair

            //this.update_stor(to_curr,from_curr,orders)
            //console.log(from_curr,to_curr,orders[0])
          
    
    


 
    // query_currencies(pairs){
    //     var books = []
    //     for (var pair in pairs){
    //         console.log(pair,pairs)
    //         if (this.markets.includes(pair)) {
    //             for (var exch in this.currencies[curr]){
    //                 var node = exch + '_' + curr
    //                 console.log(node,Object.keys(this.graph))
    //                 books.push(this.graph[node])
    //             }
    //         } 
    //     } //console.log(this.graph)
    //     return books
    // }
       
    

    // query_exchanges(exchanges){
    //     var books = []
    //     for (var exch in exchanges){
    //         if (this.exchanges.includes(exch)){
    //             //get all currencies and query books
    // }
    //     }
    //     return books
    // }

    // filtered(exch,curr){
    //     var books = []
    //     return books
    // }


}

module.exports = Dbaccess; 