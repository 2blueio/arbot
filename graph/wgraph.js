//wgraph
const Queue = require('./queue.js');
class Wgraph {
       // Array is used to implement a Queue
       constructor(uselogs)
       {
           this.graph = []
           this.graphdata = []
           this.loopcount = 0
           this.nloops = []
           this.vertices = []
           this.uselogs = uselogs
       }


update(v1,v2,weight,data){    //add vertices and weighted edge to graph
        if (this.uselogs = 1) {
            var w = -Math.log(parseFloat(weight))
        } else if (this.uselogs = 0) {
            var w = parseFloat(weight)
        } 
        if (v1 != v2){
            if (!(v1 in this.graph)) { 
                this.graph[v1] = {}
                this.graphdata[v1] = {}
            }
            if (!(v2 in this.graph)) {
                this.graph[v2] = {}
                this.graphdata[v2] = {}
            }
        this.graph[v1][v2] = w //add edge to vertices
        this.graphdata[v1][v2] = data
        }
        //keep track of all vertices
        if (!(this.vertices.includes(v1))){
            this.vertices.push(v1) 
        }
        if (!(this.vertices.includes(v2))){
            this.vertices.push(v2)
        }            
} 

initialize(source){
	var d = {} // Stands for destination
	var p = {} // Stands for predecessor
    for (var node in this.graph) {
        d[node] = Infinity // We start admiting that the rest of nodes are very very far
        p[node] = null
	}
    d[source] = 0 // For the source we know how to reach
    return [d, p]
}

relax(node, neighbour, d, p){
    // If the distance between the node and the neighbour is lower than the one I have now
	if (d[neighbour] > (d[node] + this.graph[node][neighbour])){
        // Record this lower distance
        d[neighbour]  = d[node] + this.graph[node][neighbour]
        p[neighbour] = node
    }
}

retrace_negative_loop(p, start){
	var arbitrageLoop = [start]
	var next_node = start
	while (true){
		next_node = p[next_node]
		if (!(arbitrageLoop.includes(next_node))){
			arbitrageLoop.push(next_node)
		}
		else {
			arbitrageLoop.push(next_node)
            arbitrageLoop = arbitrageLoop.slice(arbitrageLoop.indexOf(next_node))
            //console.log('loop retraced.')
			return arbitrageLoop
		}
	}
}

contains_undefined(checkarray){
    for (var i in checkarray){
        if (typeof checkarray[i] == 'undefined'){
            return 1
        }
    }
    return -1
}

bellman_ford(source){
	var dp = this.initialize(source)
	var d = dp[0]
    var p = dp[1]
    var vertices = Object.keys(this.graph).length
    for (var i in this.range(vertices - 1)){ //Run this until is converges up to V - 1 times
        for (var u in this.graph){ //run on each vertex
            for (var v in this.graph[u]){ //For each neighbour of u
                this.relax(u, v, d, p) //Lets relax it
	    	}
	 	}
     }

     // Step 3: check for negative-weight cycles
     for (var u in this.graph){
        for (var v in this.graph[u]){
            if ((d[v] > (d[u] + this.graph[u][v])) ){ // && !(p[0] == null) added p[0] check for null
                //console.log('loop found:')
                //console.log(p, source)
                var path = this.retrace_negative_loop(p, source).reverse()
                if ((this.searchForArray(this.nloops, path) == -1) && (this.contains_undefined(path) == -1)) {
                    this.nloops.push(path)
                    this.loopcount ++
                }
            }
            else {
                //console.log('no loops found')
            }
        }  
     //console.log(loopcount.toString() + ' loops found for vertex: ' + source.toString())
     //return [d, p]
    }
}

range(start, stop, step) {
    if (typeof stop == 'undefined') {
        // one param defined
        stop = start;
        start = 0;
    }
    if (typeof step == 'undefined') {
        step = 1;
    }
    if ((step > 0 && start >= stop) || (step < 0 && start <= stop)) {
        return [];
    }
    var result = [];
    for (var i = start; step > 0 ? i < stop : i > stop; i += step) {
        result.push(i);
    }
    return result;
}
searchForArray(haystack, needle){ //Custom search for nested array check
  var i, j, current;
  if (!(needle === null) && !(haystack === null)) {
	  for(i = 0; i < haystack.length; ++i){
		  if (!(haystack[i] === null)){
   	   		if(needle.length === haystack[i].length){
     	   	 	current = haystack[i];
      	 		for(j = 0; j < needle.length && needle[j] === current[j]; ++j);
      				if(j === needle.length)
       				return i;
    		} //if needle.length
		} //if haystack not null
  		} //for i
	} //if !needle
  return -1;
}

 // function to performs BFS <--need to add weighted search
bfs(startingNode, destinationNode){
    var vertices = Object.keys(this.graph).length
    // create a visited array
    var visited = [];
    var path = [];
    var paths = [];
    for (var i = 0; i < vertices; i++)
        visited[i] = false;
 
    // Create an object for queue
    var q = new Queue();
 
    // add the starting node to the queue
    visited[startingNode] = true;
    q.enqueue(startingNode);
 
    // loop until queue is element
    while (!q.isEmpty()) {
        // get the element from the queue
        var getQueueElement = q.dequeue();
        path.push(getQueueElement)
        // passing the current vertex to callback funtion
        //console.log(getQueueElement);
 
        // get the adjacent list for current vertex
        //var get_List = this.AdjList.get(getQueueElement);
        var get_List = this.graph[getQueueElement];
 
        // loop through the list and add the elemnet to the
        // queue if it is not processed yet
        for (var i in get_List) {
            var neigh = i;
            if (!visited[neigh]) {
                visited[neigh] = true;
                console.log('visiting node: ',neigh)
                if (neigh == destinationNode) {
                    path.push(neigh)
                    console.log('Path found: ',path)
                    return path
                }
                q.enqueue(neigh);
                
            }
        }
    }
    return null
}

getnloops(){
    this.nloops = []
    this.loopcount = 0
    for (var key in this.graph) {
        this.bellman_ford(key)
    }
    
    if (this.nloops.length > 0){
        // console.log('Arbitrage loops found:')
        // console.log(this.nloops)
        return(this.nloops)
    } else {
        // console.log('No Opportunities found')
        return null
    }
}

getloopcount(){
    this.getnloops()
    return this.loopcount
}
getvertices(){
    return this.vertices
}
}

module.exports = Wgraph;

// var vertices1 = readfile('simple.txt')	  //No loops
// var vertices2 = readfile('simple_nl.txt')  //Arb Loop
// var vertices3 = readfile('priceonomics0.txt')	  //
// var vertices4 = readfile('priceonomics.txt')	  //
// var vertices5 = Wgraph.readfile('priceonomics2.txt')	  //
// var jsrates = JSON.parse(JSON.stringify(vertices5))
// //var nloops  = [] 
// graph2 = new Wgraph()
// graph2.update(jsrates,1)


// //console.log(graph2)
// graph2.getnloops(graph2)
// console.log(graph2.bfs("CAD","EUR"))
// console.log(graph2.loopcount)
// console.log(graph2.nloops)
// console.log(graph2.vertices)
// console.log(graph2.getvertices())



// Missing for Arb Class version
// exchanges
// order books