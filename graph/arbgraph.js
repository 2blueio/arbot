const Wgraph = require('./wgraph.js');
const Queue = require('./queue.js');

class Arbgraph extends Wgraph {
    constructor() {
      super(1);
      this.satoshi = 0.00000001; //1 Satoshi default
      this.tradesizes = {}; //{symbol : USDValue}
      this.USDValue = 0;
      this.exchanges = [];
      this.currencies = [];
      this.tradeSize = this.satoshi;
      this.baseCurrency = 'BTC';
      this.orderbooks = [];
      this.exchdetails = [];
      this.minfee = 0.00;
      this.reversed_exchanges = [];
      this.wfees = {};
      //this.storage = new Wgraph(3) //create parallel graph for storing latest order books and additional data
      //console.log('storage graph set to: ',this.storage.uselogs)
      this.markets = {};
      this.usefees = true;
      this.accounts = {}
    }

    testfunc9(x) {
        return new Promise(resolve => {
            var query = ExchangeModel.find({}).sort({exch_code: -1});
            query.exec(function (err, docs) {
                  var exchdict = {};
                  docs.forEach(function (arrayItem) {
                    exchdict[arrayItem.exch_code] = arrayItem;
                  });
                // this.exchanges = exchdict
                 //cb(exchdict)
                 //this.exchanges = exchdict;
                 resolve(exchdict);
                  //return exchdict
                  //console.log(exchdict)
                  //return {'1':'5'}
                })
            })
    }

    getAllExchangesCallback(cb){
        
        var query = ExchangeModel.find({}).sort({exch_code: -1})
        query.exec(function (err, docs) {
              var exchdict = {}
              docs.forEach(function (arrayItem) {
                exchdict[arrayItem.exch_code] = arrayItem
              });
            // this.exchanges = exchdict
             cb(exchdict);
              //return exchdict
              //console.log(exchdict)
              //return {'1':'5'}
            });
            //cb(query.docs)
            //return {'1':'5'}
            //return exchdict
    }

    getAllPairs(settings,cb){

    }
     testfunc1(x) {
        return new Promise(resolve => {
          setTimeout(() => {
              this.testfunc = this.testfunc + x * 2
            resolve(x * 2);
          }, 2000);
        });
      }
      
       testfunc2(x) {
          return new Promise(resolve => {
            setTimeout(() => {
                this.testfunc = this.testfunc + x * 10
              resolve(x * 10);
            }, 2000);
          });
        }
      
         testfunc3(x) {
          return new Promise(resolve => {
            setTimeout(() => {
                this.testfunc = this.testfunc + x * 100;
              resolve(x * 100);
            }, 2000);
          });
        }

    importwfees(wfees){
        this.wfees = wfees;
        if (!(this.wfees.misc.usefees)) { 
            console.log('Disabling Fees');
            this.usefees = false;
        } else {
            console.log('Using fees schedule Min fee set to: ', this.wfees.misc.minfee);
            console.log('Total fees imported: ', Object.keys(this.wfees).length);
            this.minfee = this.wfees.misc.minfee;
            this.usefees = true;
        }
    }

    importExchanges(exch){
        this.exchanges = exch;
        //need to update withdrawal info for later use
        console.log('Total exchanges imported: ',Object.keys(this.exchanges).length);

    }

    importMarkets(markets){
        //console.log(exchanges)
        this.markets = markets;
        console.log('Total markets imported: ', Object.keys(this.markets).length);
        //console.log(markets)
    }

    importAccounts(accounts){
        this.accounts = accounts;
        console.log('Total accounts imported: ', Object.keys(this.accounts).length);
    }

    setTradeSize(BtcValue, basecurr){
        this.tradeSize = BtcValue;
        this.baseCurrency = basecurr;
    }

    importOrderBooks(books){
        if (!(books == null)) {
            this.orderbooks = books;
        } else {
            console.log('no orderbooks found to import');
        }

        for (var arrayItem in this.orderbooks){
            this.processBook(arrayItem,0);
        }
        this.exchangelinks();
        console.log('Total orderbooks imported: ',this.orderbooks.length);
    }

    findBook(targetmarket,exchange){
        var book;
        for (var arrayItem in this.orderbooks){
            //if (this.orderbooks[arrayItem].exch_code == 'BTRX') console.log('Testing: ',exchange,targetmarket, this.orderbooks[arrayItem].exch_code,this.orderbooks[arrayItem].market);
            //NEED to parse market and exchange separately
            if (this.orderbooks[arrayItem].market == targetmarket && this.orderbooks[arrayItem].exch_code == exchange){
                //console.log('found orderbook for: ', exchange, targetmarket);
                book = this.orderbooks[arrayItem].orders;
                //console.log('!!!! FOUND BASE BOOK !!!! ',this.orderbooks[arrayItem].market, targetmarket);
                //console.log(book);
                return book;
            }
        }
        //console.log('!!!! NOT FOUND !!!! ',exchange, targetmarket);
        return -1;
    }

    updateGraph(){ //update graph using orderbooks while looking up tradesizes
        for (var arrayItem in this.orderbooks){
            this.processBook(arrayItem);
        }
    }

    usefile(filename){
		var fs = require('fs');
		var data = JSON.parse(fs.readFileSync(filename, 'utf8'));
        this.parseratefile(data)
    }

    fees(exch){ //lookup fee for exchange
        return this.exchdetails[exch]['exch_fee']
       }
    
    parseratefile(rates){
        for (var key in rates){
            var matches = key.split("_", 2);
            var from_rate = matches[0];
            var to_rate = matches[1];
            this.update(from_rate,to_rate,rates[key],'ratefile')
        }
    }

    addExchDetails(data){
        this.exchdetails = data
    }

    addReversed(data){
        this.reversed_exchanges = data
    }

    checkPairs(c1, c2, r1, r2, exch){ //check that pairs aren't reversed
        //console.log(exch, this.reversed_exchanges)
        if (this.reversed_exchanges.includes(exch)){
            console.log(exch + ' exchange has reversed Pairs')
            return false;
        } else {
            if (((c1 == 'BTC') && (r1 < r2)) || (c2 == 'BTC' && (r1 > r2))) {
                console.log('Uncaught INVALID BTC RATIO. Exiting program...')
                process.exit(c1,c2,r1,r2,exch) //EXIT out of the app in order to investigate pair
                return false;
            } 
        }
        //console.log('pairCheck OK')   
        return true;
    } //check pairs

    //function to lookup base currency equivalent USD, USDT, or BTC
    getBaseValue(basevalue, basecurr, targetcurr,exchange){
        var feepct = this.exchanges[exchange].exch_fee;
        var net = this.calcNet(exchange);
        var feepct = 1 - net;
        var dir = 'Sell';
        var quantity = 0;
        var targetmarket;
        if (targetcurr == 'USD'){
            targetmarket = basecurr + '/' + targetcurr;
            dir = 'Buy';
        } else if (targetcurr == 'USDT'){
            targetmarket = basecurr + '/' + targetcurr;
            dir = 'Buy';
        } else {
            targetmarket = targetcurr + '/' + basecurr;
        }
        
        if ((basecurr == targetcurr) || (targetcurr == 'BTC')){
            //console.log('base and target are equal. returning ',basevalue);
            quantity = basevalue;
        } else {
            var book = this.findBook(targetmarket,exchange);
            if ((book !== -1) & (book.length > 1) & (book !== undefined) & (book[0]!==undefined)){ 
                quantity = this.getDepth(book,basevalue,dir).quantity;
                
                //var from_rate = frate.avgprice * net;
            } else {
                //console.log('*** BASE CURRENCY NOT FOUND FOR ***: ',exchange, targetmarket);
                if (targetcurr == 'USD'){
                    console.log('checking for USD equivalent');
                    targetmarket = basecurr + '/' + 'USDT';
                    book = this.findBook(targetmarket,exchange);
                    if (book !== -1) {
                        console.log('found USDT equivalent');
                        quantity = this.getDepth(book,basevalue,dir).quantity;
                    } else {
                        console.log('quantity not found for test. using 1 satoshi. ')
                        quantity = this.satoshi //should be changed to reflect realistic quantity
                    }
                }    
            }  
        }
        return quantity;
    }

    calcNet(exchange){
        var net = 1;
        if (this.usefees) {
            //console.log('deducting fees...');
            //console.log('processing:',exchange)
            var feepct = this.exchanges[exchange].exch_fee;
            if (feepct < this.minfee) {
                //console.log('fee not found for exchange: '+exchange+'. setting to default: ' + this.minfee)
                //console.log('exchanges array length: ',Object.keys(this.exchanges).length)
                feepct = this.minfee;
            } else {  
                //console.log('fee for',exchange,' set to:',feepct);
            }
            net = 1 - feepct;
        } else {
            //console.log('not using fees...');
            net = 1;
        }
        return net;
    }

 
    // if basevalue = 0 then skeleton graph is created using best bid/ask regardless of trade size
    //  if basevalue > 0 then it will use orderbook depth to get actual rates
    // Add a return value so that it can be used for future inputs when calculating trade loops
    // Add boolean for whether to update graph so that it can be used for final trade loop calc witout updating the graph
    processBook(booknum, testTradeSize, dir){
            var testResult;
            var exchange = this.orderbooks[booknum].exch_code;
            var market = this.orderbooks[booknum].market;
            //var feepct = this.orderbooks[booknum].exch_fee;
            var timestamp = this.orderbooks[booknum].timestamp;
            var orders = this.orderbooks[booknum].orders;
            var net = this.calcNet(exchange);
            var feepct = 1 - net;
            var to_tradesize = this.satoshi;
            var from_tradesize = this.satoshi;

            var currs = market.split("/", 2);
            var from_curr = exchange + '_' + currs[0];
            var to_curr = exchange + '_' + currs[1];

            if (this.tradeSize !== this.satoshi) {
               from_tradesize = this.getBaseValue(this.tradeSize,this.baseCurrency,currs[0],exchange);
               to_tradesize = this.getBaseValue(this.tradeSize,this.baseCurrency,currs[1],exchange);
               //console.log("using updated tradesize");
            } 

            var frate = this.getDepth(orders,from_tradesize,'Buy');
            var from_rate = frate.avgprice * net;
            var trate = this.getDepth(orders,to_tradesize,'Sell');
            var to_rate = trate.avgprice * net;

            // Calculate result for Loop then return result immediately. Do not update graph.
            if (testTradeSize > 0) { 
                testResult = this.getDepth(orders,testTradeSize,dir);
                testResult.quantity = testResult.quantity * net;
                //testTo = this.getDepth(orders,1/testTradeSize,'Sell');
                //console.log('processing testTradeSize:',testFrom)
                return testResult;
            }

            //console.log('Process Book-->',from_curr,to_curr,from_rate,to_rate);
            var data1 = {'direction':'Buy', 'market': market, 'exchange': exchange, 'tfee': feepct, 'booknum': booknum, 'graphBasis': frate, 'timestamp':timestamp};
            var data2 = {'direction':'Sell', 'market': market, 'exchange': exchange, 'tfee': feepct, 'booknum': booknum,'graphBasis': trate, 'timestamp':timestamp};
            this.update(from_curr,to_curr,from_rate,data1);
            this.update(to_curr,from_curr,to_rate,data2); //add reverse edge

        if (!(Object.keys(this.currencies).includes(currs[0]))){ //if it's a new currency rebuild links
            this.currencies[currs[0]] = [];
        }
        if (!(Object.keys(this.currencies).includes(currs[1]))){
            this.currencies[currs[1]] = [];
        }
        if (!(this.currencies[currs[0]].includes(exchange))){
            this.currencies[currs[0]].push(exchange);
        }
        if (!(this.currencies[currs[1]].includes(exchange))){
            this.currencies[currs[1]].push(exchange);
        }
        if (!(Object.keys(this.exchanges).includes(exchange))){ 
            console.log('cant find:',exchange,Object.keys(this.exchanges));
            this.exchanges[exchange] = [];
        }
        if (!(this.exchanges[exchange].pairs.includes(market))){
            this.exchanges[exchange].pairs.push(market);
        }
        
    }

    addOrderBook(orders, tradesize) { //Deprecated: Add new trade data pairs from web sockets, needs to use singlar
        var result = 0;
        if ((!(orders == undefined)) && (!(orders[0].exchange == undefined)) && (!(orders[0].market == undefined))){

        //console.log(orders)
        // console.log('-------------')
        // console.log(orders)
        var exchange = orders[0].exchange;
        var market = orders[0].market;
        var feepct = this.fees(exchange)
        // var feepct = .002
        if (feepct < this.minfee) {
            console.log('fee not found for exchange: '+exchange+'. setting to default: ' + this.minfee)
            feepct = this.minfee
            //console.log(this.exchdetails)
        }

 
        var net = 1 - feepct
        //net = 2 // ******* override for testing without any fee deductions. *********
        var buildlinks = false
        var currs = market.split("/", 2);
        var from_curr = exchange + '_' + currs[0];
        var to_curr = exchange + '_' + currs[1];

        //console.log(exchange,market,feepct)
        var from_rate = this.getDepth(orders,tradesize,'Sell')*net;
        var to_rate = 1/this.getDepth(orders,tradesize,'Buy')*net;

        if (!(this.checkPairs(currs[0],currs[1],from_rate,to_rate,exchange))){ //check for reversed pairs
            var to_rate = this.getDepth(orders,tradesize,'Sell')*net;
            var from_rate = 1/this.getDepth(orders,tradesize,'Buy')*net;
        }

        if (!(Object.keys(this.currencies).includes(currs[0]))){ //if it's a new currency rebuild links
            this.currencies[currs[0]] = []
            buildlinks = true
        }
        if (!(Object.keys(this.currencies).includes(currs[1]))){
            this.currencies[currs[1]] = []
            buildlinks = true
        }
        if (!(this.currencies[currs[0]].includes(exchange))){
            this.currencies[currs[0]].push(exchange)
            buildlinks = true
        }
        if (!(this.currencies[currs[1]].includes(exchange))){
            this.currencies[currs[1]].push(exchange)
            buildlinks = true
        }
        if (!(Object.keys(this.exchanges).includes(exchange))){ 
            console.log('cant find:',exchange,Object.keys(this.exchanges))
            this.exchanges[exchange] = []
            buildlinks = true
        }

        if (!(this.exchanges[exchange].includes(market))){
            this.exchanges[exchange].push(market)
            buildlinks = true
        }

        if (buildlinks && this.exchanges.length > 1){ //rebuild links if triggered by buildlinks bool
           this.exchangelinks()
        }

        // console.log("Adding/Updating Order book for: " + exchange);
        // console.log(from_curr,to_curr,from_rate,to_rate)
        // console.log(this.getDepth(orders,0,'Sell'),this.getDepth(orders,0,'Sell')*net)
        // console.log(this.getDepth(orders,0,'Buy'),this.getDepth(orders,0,'Buy')*net)
        // console.log(1/this.getDepth(orders,0,'Buy'),1/this.getDepth(orders,0,'Buy')*net)
        // console.log(orders)
        this.update(from_curr,to_curr,from_rate,'Buy')
        this.update(to_curr,from_curr,to_rate,'Sell') //add reverse edge
        // this.storage.update_stor(from_curr,to_curr,orders)
        // this.storage.update_stor(to_curr,from_curr,orders)
        //console.log(this.storage)
        result = tradesize * from_rate
        //console.log('from rate: ',from_rate)
        } return result
    } //addorderbook

    exchangelinks(usetradesize){ //builds the linkages across exchanges for the same asset. Uses .001 wfees or wfees book. Need to add tradesize input
        var from_tradesize = 1000000
        var to_tradesize = 1000000
        for (var c in this.currencies){
                //console.log('exchange link builder:')
                //console.log(c,this.currencies[c])
                var clist = this.currencies[c]
                for (var e1 in clist){
                    for (var e2 in clist){
                        if (!(clist[e1] == clist[e2])){
                            //console.log(c,clist[e1],clist[e2])
                            var from_curr = clist[e1] + '_' + c
                            var to_curr = clist[e2] + '_' + c
                            if (usetradesize) {
                                from_tradesize = this.tradesizes[from_curr]
                                to_tradesize  = this.tradesizes[to_curr]
                            } 

                            var from_market = from_curr + '/' + to_curr
                            var to_market = to_curr + '/' + from_curr
                            var from_exchange = from_curr.split("_", 2)[0];
                            var to_exchange = to_curr.split("_",2)[0];
                            var from_wfee = null;
                            var to_wfee = null;
                            //console.log(Object.keys(this.wfees,c))
                            if (this.usefees) {
                                console.log('deducting fees...')
                                if (Object.keys(this.wfees).includes(from_exchange)){
                                    from_wfee = this.wfees[from_exchange][c]
                                    console.log(from_curr,from_wfee)
                                } else {
                                    console.log('no wfees found for ',from_curr,', setting to minfee: ', this.minfee)
                                    from_wfee = this.minfee;
                                }
                                if (Object.keys(this.wfees).includes(to_exchange)){
                                    to_wfee = this.wfees[to_exchange][c]
                                    console.log(to_curr,to_wfee)
                                } else {
                                    console.log('no wfees found for ',to_curr,' setting to minfee: ', this.minfee)
                                    to_wfee = this.minfee;
                                }
                            } else {
                                console.log('fees disabled...')
                                from_wfee = 0;
                                to_wfee = 0;
                            }
                            
                            var from_net =  (from_tradesize - from_wfee) / from_tradesize
                            var to_net = (to_tradesize - to_wfee) / to_tradesize
                            //console.log('building exchange link for: ' + from_curr + ' ' + to_curr)
                            var data1 = {'direction':'Transfer', 'market': from_market, 'exchange': from_exchange, wfee: from_wfee};
                            this.update(from_curr,to_curr,from_net,data1)
                            //console.log('******', from_curr)
                            var data2 = {'direction':'Transfer', 'market': to_market, 'exchange': to_exchange, wfee: to_wfee};
                            this.update(to_curr,from_curr,to_net,data2) //add reverse edge
                

                        }  
                    }
                }
            }
    }
    
    //basevalue = quantity, basecurr = asset
    UpdateTradesizes(basevalue,basecurr){
        console.log('tradesize value to:',basevalue,basecurr)
        this.updateOrderBooks(basevalue,basecurr)
        console.log('------')
    }

    CalcUSDTradesize(asset, exch, ticker, USDValue){ //Deprecated. May not be used
        
        var masterUSD = [
            "BTRX","GDAX","KRKN"
        ]
        var masterUSDT = [
            "BTRX","BINA","CPIA","KRKN"
        ]
        var masterBTC = [
            "BTRX","BINA","CPIA","KRKN","GDAX"
        ]
        //var exchange = ticker.split("_", 2)[0];
        var asset = ticker.split("_", 2)[1];
        //var asset = ticker.market
        var USD = asset + "/" + "USD"
        var USDT = asset + "/" + "USDT"
        var BTC = asset + "/" + "BTC"
        var newquantity = USDValue

        //first check if there is a USD or USDT pair on the target exchange:
        console.log('USD book: ' ,this.getBook(exch,USD));
        console.log('USDT book: ', this.getBook(exch,USDT));

        for (var exchUSD in masterUSD){ //Check for USD pair in Master List
            if (this.exchanges[masterUSD[exchUSD]].pairs.includes(USD) && (newquantity == -1)){
                let exch_name = this.exchanges[masterUSD[exchUSD]].exch_code
                //console.log('found USD pair in exchange records. Looking up recent orderbook...')
                let tempquantity = this.getDepth(this.getBook(exch_name,USD),USDValue,'Sell').quantity
                if (tempquantity > 0) newquantity = tempquantity
                //console.log('Found MasterUSD pair on',exch_name,'. $',USDValue,'=',newquantity,asset)
            }
        }

        if (newquantity == -1) { //If no USD pair use USDT pair and convert using master USD/USDT Rate (Bittrex)
            var USDTRate = this.getDepth(this.getBook('BTRX','USDT/USD'),USDValue,'Buy').avgprice
            for (var exchUSDT in masterUSDT){
                if (this.exchanges[masterUSDT[exchUSDT]].pairs.includes(USDT) && (newquantity == -1)){
                    let exch_name = this.exchanges[masterUSDT[exchUSDT]].exch_code
                    //console.log('using BTRX USDT Rate:',USDTRate)
                    //console.log('found',USDT,'on',exch_name,' Looking up recent orderbook...')
                    newquantity = this.getDepth(this.getBook(exch_name,USDT),USDValue * USDTRate,'Buy').quantity
                    //console.log('Using USDT/USD rate',USDTRate,'$',USDValue,'=',newquantity,asset)
                    
                }
            }
        }

        if (newquantity == -199) { //Use BTC pair and convert using master USD Rates (not working yet)
            console.log('trying BTC rate.....')
            var BTCRate = this.getDepth(this.getBook('GDAX','BTC/USD'),1,'Buy').avgprice
            //console.log('BTC rate:',BTCRate)
            for (var exchUSD in masterBTC){
                //console.log('???',this.exchanges[masterBTC[exchUSD]].pairs,BTC)
                if (this.exchanges[masterBTC[exchUSD]].pairs.includes(BTC) && (newquantity == -1)){
                    let exch_name = this.exchanges[masterBTC[exchUSD]].exch_code
                    console.log('using GDAX BTC Rate:',BTCRate)
                    let BTCValue = USDValue / BTCRate
                    console.log('using BTC quantity:',BTCValue)
                    console.log('found',BTC,'on',exch_name,' Looking up recent orderbook...')
                    newquantity = this.getDepth(this.getBook(exch_name,BTC),BTCValue,'Sell').quantity
                    console.log('$',USDValue,'=',newquantity,asset)
                    console.log(this.getBook(exch_name,BTC),BTCValue,'Sell')
                    
                }
            }
        }
        

        if (newquantity == -1) {console.log('ERROR: unable to calculate USD Value of asset:',ticker)}
        return newquantity

    }

    getBook(exchange,pair){
        var v1 = exchange + '_' + pair.split('/',2)[0]
        var v2 = exchange + '_' + pair.split('/',2)[1]
        var book = []
        console.log(v1,v2)
        if ((Object.keys(this.graphdata).includes(v1) && Object.keys(this.graphdata[v1]).includes(v2)) || (Object.keys(this.graphdata).includes(v2) && Object.keys(this.graphdata[v2]).includes(v1))){
            console.log('asset found in graphdata: ',v1,v2, this.graphdata[v1][v2])
            //console.log(this.graphdata)
            book = this.orderbooks[this.graphdata[v1][v2].orderbooknum].orders
            //console.log('orderbook: ',book )
        } else {
            console.log('asset not found in graphdata.')
        }
        return book
    }

    getDepth(orders,tradesize,dir){ //use only bids to determine best price in graph. Sell direction will swap bid/ask

                function swapbook (inbook){
                    let outbook = JSON.parse(JSON.stringify(inbook));
                    outbook.forEach(function (arrayItem) {
                        let tempq = parseFloat(arrayItem.quantity)
                        arrayItem.quantity = parseFloat(arrayItem.total)
                        arrayItem.total = tempq
                        //outbook.push(arrayItem)
                    })
                    //console.log('outbook:', outbook)
                    return outbook
                }

                var result = {}
                var basecurr = tradesize //base currency used to purchase other currency
                var tocurr = 0 //currency to be purchased
                var depth = -1
                var sellbook = []
                var buybook =[]
                var book = []
                //console.log('order pre:',orders)
                if (orders.length == 0) {console.log('0 size order array')}
                if ((typeof orders !== undefined) && !(orders == 1) && !(orders.length == 0)){
                  orders.forEach(function (arrayItem) {
                  if (arrayItem.ordertype == 'Sell') {
                      sellbook.unshift(arrayItem) 
                    }
                  if (arrayItem.ordertype == 'Buy') {
                      buybook.push(arrayItem) 
                    }
                })  
                } else {
                    console.log('orders error:',orders)
                }
                if (dir == 'Buy') book = buybook //<--- maybe create a function to swap market/ticker, quantity and total and possibly sort order
                  else book = swapbook(sellbook);
                //console.log(book)
                if(typeof book !== "undefined") {
                  book.forEach(function (arrayItem) {
                    //if (arrayItem.hasOwnProperty('price')){
                    if (arrayItem !== undefined) if (arrayItem.hasOwnProperty('price')){
                        if (basecurr > 0) {
                            if (basecurr >= arrayItem.quantity) {
                              depth = arrayItem.price
                              basecurr = basecurr - arrayItem.quantity
                              tocurr = tocurr + parseFloat(arrayItem.total)
                              //console.log(arrayItem.price, arrayItem.quantity, arrayItem.total, arrayItem.quantity/arrayItem.total,basecurr, tocurr)
                            } else {
                                depth = arrayItem.price
                                tocurr = tocurr + (basecurr/arrayItem.quantity * parseFloat(arrayItem.total)) 
                                //console.log(arrayItem.price, arrayItem.quantity, arrayItem.total, arrayItem.quantity/arrayItem.total, basecurr, tocurr)
                                //console.log('order filled.')
                                basecurr = 0
                            }
                        }
                    } else {
                        console.log('Bad Orderbook Detected')
                        tocurr = 0;
                    }
                })
                }
                //keep this for determining whether book was eaten
                //if (basecurr > 0) {console.log('Not enough volume in orderbook to fullfill order. Base currency left over:',basecurr)}
                
                result.tradesize = tradesize
                result.filled = basecurr <= 0 
                result.dir = dir
                
                // if ((buybook[0]!==undefined) & (sellbook[0]!==undefined)){
                    if (dir == 'Buy') {
                        if ((buybook[0]!==undefined)&(sellbook[0]!==undefined)) if ((buybook[0].hasOwnProperty('price'))&(sellbook[0].hasOwnProperty('price'))){
                            result.maxdepth =  depth
                            result.bid =  buybook[0].price
                            result.ask =  sellbook[0].price
                            result.avgprice = (tocurr / (tradesize))
                        } else {
                            tocurr = 0;
                            result.filled = false;
                            console.log('UNDEFINED buybook:',buybook,result)
                        }
                    } else {
                        if ((buybook[0]!==undefined)&(sellbook[0]!==undefined)) if ((buybook[0].hasOwnProperty('price'))&(sellbook[0].hasOwnProperty('price'))){
                            result.bid = 1 /sellbook[0].price
                            result.ask = 1 / buybook[0].price 
                            result.maxdepth = 1 / depth
                            result.avgprice =   (tocurr / (tradesize))
                        } else {
                            tocurr = 0;
                            result.filled = false;
                            console.log('UNDEFINED sellbook:',sellbook,result)
                        }
                    }
                // } else {
                //     result.filled = false;
                //     tocurr = 0;
                //     console.log('UNDEFINED book values:',buybook,sellbook,result)
                // }
                
                
                result.quantity = tocurr //<--- need to fix! If it's a sell then has to assume opposite tradesize vs Quantity
                //console.log(result) // Potential reverse book: Swap Quantity and Total,  use 1/price, use opposite book
                // console.log('Buybook:', buybook)
                // console.log('Sellbook:', sellbook)
                //console.log(orders)
                return result
    }

    // getBid(orders, tradesize){ //<--- old, has error
    //     var bestbid = 0;
    //     if(typeof orders !== "undefined") {
    //         orders.forEach(function (arrayItem) {
    //             if (arrayItem.ordertype == 'Buy' && arrayItem.price > bestbid) {
    //                 bestbid = arrayItem.price
    //             }
    //         })
    //        // console.log("Returning BestBid: " + bestbid)
    //     }
    //     return bestbid;
    // }

    // getAsk(orders, tradesize){ // old has error
    //     var bestask = Infinity;
    //     if(typeof orders !== "undefined") {
    //         orders.forEach(function (arrayItem) {
    //             if (arrayItem.ordertype == 'Sell' && arrayItem.price < bestask) {
    //                 bestask = arrayItem.price
    //             }
    //         })
    //         //console.log("Returning BestAsk: " + bestask)
    //     }
    //     return bestask;
    // }	

    usdLoops(paths){ //get Loops starting with USD
        return paths
    }

    btcLoops(paths){ //get BTC entry loops
        return paths
    }

    altLoops(paths){ //get loops with only alt coins
        return paths
    }

    primeExchLoops(paths){ //get loops on grade A exchanges with USD 
        return paths
    }

    singleExchLoops(paths){ //get loops that exist on a single exchange
        return paths
    }

    binaryExchLoops(paths){ //get loops that occur on 2 exchanges
        return paths
    }

    loopInLoops(paths,path){ //does path exist in array with different entry/exit.
        var xpath = path
        var count = 0
        for(var i = 0; i < path.length; ++i){
            if (this.searchForArray(paths, xpath) > -1 ) count += 1
            xpath.unshift(xpath.pop())
        }
        return count //return countn of variations of loop cycle found in paths
      }

    arbProfit(path){ //percentage profit on arb loop after fees
        var profit = 0
        return profit
    }

    getCurr(ticker){
        var cur = ticker.split("_", 2);
        return cur[1]
    }

    isUSD(cur){
        if ((this.getCurr(cur) == 'USD') || (this.getCurr(cur) == 'USDT')) return true
            else return false
    }

    bfupdate(startingNode, tradesize){
        //addOrderBook(orders, tradesize)
        //use fees too -- need to add ****
        var vertices = Object.keys(this.graph).length
        var coins = tradesize
        // create a visited array
        var visited = [];
        var path = [];
        for (var i = 0; i < vertices; i++)
            visited[i] = false;
     
        // Create an object for queue
        var q = new Queue();
     
        // add the starting node to the queue
        visited[startingNode] = true;
        q.enqueue(startingNode);
     
        // loop until queue is element
        while (!q.isEmpty()) {
            // get the element from the queue
            var getQueueElement = q.dequeue();
            path.push(getQueueElement)
            var get_List = this.graph[getQueueElement];
     
            // loop through the list and add the elemnet to the
            // queue if it is not processed yet
            for (var i in get_List) {
                var neigh = i;
                if (!visited[neigh]) {

                    console.log(getQueueElement,neigh,this.graph[getQueueElement][neigh])
                    visited[neigh] = true;
                    q.enqueue(neigh);
                }
            }
        }
        return null
    }

    getArbLoops(USD,BTC){
        
        var paths = this.getnloops()
        console.log(paths)
        if (!(paths == null)){
            console.log('found ' + paths.length + ' paths:')
            // console.log(this.storage)
            // console.log(this.graph)
            //console.log(paths)
        
            // for (var i = 0; i < paths.length; i++) {
            for (var i = paths.length - 1; i >= 0; i--) {
                //var path = paths[i].reverse()
                var path = paths[i]
                //console.log(i + ': ', path)
                if (path == null) {
                    console.log("No opportunity here :(")
                    //console.log(path)
                }
                else {
                    var money = USD
                    console.log("Starting with " + money.toFixed(2) + " in " + path[0])
                    console.log('Path variations count: ', this.loopInLoops(paths,path))
                    for (let [idx, val] of path.entries()) {
                        if (idx+1 < path.length) {
                            var start = path[idx]
                            var end = path[idx+1]
                            var rate = Math.exp(-this.graph[start][end])

                            //Audit Code on storage graph:
                            // if (!(this.storage.graph[start][end][0] == 1) && !(this.storage.graph[start][end][0] == undefined)) {
                            //     var exchange = this.storage.graph[start][end][0].exchange
                            //     if (!(this.checkPairs(start,end,0,0,exchange))){
                            //      var rate2 = this.getDepth(this.storage.graph[start][end],0,'Sell')
                            //     } else {
                            //      var rate2 = this.getDepth(1/this.storage.graph[start][end],0,'Buy')
                            //     }
                            // }
                            var rate2 = 0 //audited rate

                            money *= rate
                            console.log(start + " to " + end + " at [" + -this.graph[start][end] + "] " + rate.toFixed(20) + "("+ rate2.toFixed(20)+") = " + money.toFixed(20))
                            //console.log(this.getDepth(this.storage.graph[start][end],money,'Sell'))
                            //console.log(this.storage.graph[start][end])

                            //need to add depth audit filter here
                            //probably need to dump a full graph and load
                        }
                    }
                }
            console.log("\n")
            //if (this.isUSD(start)) this.bfupdate(start,100)
            }
        }
        
    }


    getArbLoopBooks(addLoopsToDB,startingcurr){
     
        //console.log(this.accounts)
        var startCurr,startExch,fixedmoney,varmoney;
        var maxbtc = 0;
        var paths = this.getnloops()
        //console.log(paths)
        if (!(paths == null)){
            // console.log('found ' + paths.length + ' paths:')
            // console.log('PATHS:',paths)
            var loops = [];   

            for (var i = paths.length - 1; i >= 0; i--) {
                var path = paths[i]
                var loop = {
                    status: 'pending',
                    expected_result: 0,
                    actual_result: 0,
                    orders: [],
                    path: paths[i]
                }
                //console.log('PATH:',paths[i])
                if (path == null) {
                    console.log("No opportunity here :(")
                }
                else {                    
                    startExch = path[0].split("_", 2)[0];
                    startCurr = path[0].split("_", 2)[1];
                    fixedmoney = this.getBaseValue(this.tradeSize,this.baseCurrency,startCurr,startExch);
                    varmoney = fixedmoney;
                    //console.log('FIRST currency: ',startCurr,startExch)
                    // console.log("Starting with " + this.tradeSize + " of " + this.baseCurrency + ' = ' + fixedmoney.toFixed(2) + ' ' + startCurr)
                    //console.log('Path variations count: ', this.loopInLoops(paths,path))
                    var oldestLoop = new Date();
                    for (let [idx, val] of path.entries()) {
                        //create orders here
                        //console.log('INDEX:',idx)
                        
                        if (idx+1 < path.length) {
                            var start = path[idx]
                            var end = path[idx+1]
                            var rate = Math.exp(-this.graph[start][end])
                            //var book = this.orderbooks[this.graphdata[start][end].orderbooknum]
                            var details = this.graphdata[start][end]
                            var timestamp = details.timestamp;
                            //console.log(oldestLoop)
                            if (timestamp < oldestLoop) oldestLoop = timestamp;
                            //console.log(timestamp,oldestLoop)
                            //console.log('VAR MONEY 1:',varmoney)
                            if (details.direction !== 'Transfer') {
                                let testBasis = this.processBook(details.booknum,varmoney,details.direction)
                                let exchmarket = details.exchange + '_' + details.market;
                                //var marketlookup = this.markets[exchmarket].market;
                                if (this.markets[exchmarket]!==undefined){
                                    var mkt_id = this.markets[exchmarket].mkt_id;
                                    var exch_id = this.markets[exchmarket].exch_id;
                                    var exch_name = this.markets[exchmarket].exch_name;
                                }
                                
                                //default auth_id is 0 to indicate we don't have an account set up with the exchange
                                var auth_id = 0;
                                if (this.accounts.hasOwnProperty(exch_id)){
                                    auth_id = this.accounts[exch_id].auth_id;
                                }
                                
                                let order_type_id;
                                if (testBasis.dir == 'Buy') {
                                    order_type_id = 1;
                                } else {
                                    order_type_id = 2;
                                }
                                var order = { 'exch_name': exch_name, 'market': details.market, 'direction': testBasis.dir, 'auth_id': auth_id, 'exch_id': exch_id, 'mkt_id': mkt_id, 'order_type_id': order_type_id, 'price_type_id': 3, 'limit_price': testBasis.maxdepth,  'order_quantity': testBasis.tradesize, 'expected_result': testBasis.quantity};
                                //console.log('RAW order:',order);
                                varmoney = testBasis.quantity;
                            } else {
                                //console.log("TRANSFER:",details)//build transfer order here
                                let exchmarkets = details.market.split('/',2);
                                let from_exchmarket = exchmarkets[0];
                                let to_exchmarket = exchmarkets[1];
                                let from_exch = from_exchmarket.split('_',2)[0]
                                let currency = from_exchmarket.split('_',2)[1];
                                let to_exch = to_exchmarket.split('_',2)[0]
                                var order = { 'direction': 'Transfer', 'exch_name': from_exch, 'to_exch': to_exch, 'currency': currency, 'order_quantity': varmoney, 'expected_result': varmoney * (1 - details.wfee), 'wfee': details.wfee, 'status': 'pending'}
                                varmoney = (1 - details.wfee) * varmoney;
                            }
                            loop.orders.push(order)
                            //console.log('VAR MONEY 2:',varmoney)
                            //console.log('Details:',details)

                            fixedmoney *= rate
                            // console.log(start + " to " + end + " at [" + -this.graph[start][end] + "] " + rate.toFixed(20) + ' = ' + fixedmoney.toFixed(20))
                            // console.log(start + " to " + end + " = " + varmoney.toFixed(20))
                            // // console.log('LOOP:',i,'Order:',idx,loop)
                        }
                        

                        //loop.expected_result = loop.orders[loop.orders.length-1].expected_result / //loop.orders[0].order_quantity
                        //console.log('Loop length:',loop.orders.length,loop)
                        //console.log('LOOP:',loop.orders[loop.orders.length-1])
                        //console.log('LOOP:',loop)
                        
                    } 
                    let pct_result = (((loop.orders[loop.orders.length-1].expected_result-loop.orders[0].order_quantity)/loop.orders[0].order_quantity)*100)
                    loop.expected_result = pct_result;
                    let quantity_result = varmoney - loop.orders[0].order_quantity;

                    //show only paths starting with BTC:
                    let now = new Date();
                    
                    loop['oldestbook'] = oldestLoop;
                    loop['age_detected'] = now - oldestLoop;
                    loop['quantity_result'] = quantity_result;
                    loop['starting_asset'] = loop.path[0].split('_',2)[1];
                    
                    if (loop['starting_asset'] == startingcurr) {
                        console.log('--------------------------------------------------')
                        console.log('Path:',loop.path);
                        console.log('Oldest Book Age:',loop.age_detected/1000,'seconds');
                        console.log('orders:',loop.orders.length,'result:');
                        console.log('% Result:',loop.expected_result.toFixed(4),'%');
                        console.log('Tadesize: ',fixedmoney.toFixed(7),'$',(fixedmoney*12000).toFixed(4));
                        console.log('BTC profit:',quantity_result.toFixed(7));
                        console.log('USD Profit:',(quantity_result*12000).toFixed(4));
                        console.log('---------------------------------------------------')
                        
                        if (quantity_result > maxbtc) maxbtc = quantity_result;
                    }
                    
                    loops.push(loop);
                }
            }
            addLoopsToDB(loops)
            ///console.log(loops);
            return loops;
            
        } else {
            console.log('No profitable arbitrage loops detected.')
            return -1;
        }
        
    }
    
    
} //class
module.exports = Arbgraph;  


