/* 
Module for database functions for trading strategies.
Requires Loopback.io server to be running on port 3000 with database for storing realtime strategy information
Purpose is to have real-time information on competing strategies using same symbol, and for storing strategy positions persistently,
and for historical stats.

Will be able to obtain all units for symbol across strategies and use auditing live vs locally calculated quantities and adjust brokerage as needed
*/

const request = require('request');
const BINANCE = require('./binancetradeservices.js')


//creates basic strategy object from commandline args
function createstrategyobj(arg) {

    var res = arg.split("_");
    const exchange = res[0];
    const exchtype = res[1];
    const strategy = res[2];
    const symbol = res[3];
    const interval = res[4];
    const psize = res[5];
    const log = res[6];

    var strategyobj = {
        id: exchange + '_' + exchtype + '_' + strategy + '_' + symbol + '_' + interval,
        exchange: exchange,
        exchtype: exchtype,
        strategy: strategy,
        symbol: symbol,
        interval: interval,
        psize: parseFloat(psize),
        avgPrice: 0,
        units: 0,
        openPl: 0,
        cumPl: 0,
        positions: [],
        historical: [],
        details: [],
        log: log
    }

    return (strategyobj);
}

async function initstrategy(strategyobj) {
    //initialize strategyobj with live data such as precision and other details.
    if (strategyobj.exchange == 'BINANCE') {
        console.log('Fetching exch info from BINANCE...')
        var exchsymbols = await BINANCE.exchinfo(strategyobj.exchtype); //gets details about symbols such as quantity Precision and trade types available (needed for trading). May need to changed this to a fixed array.
        strategyobj.quantityPrecision = exchsymbols[strategyobj.symbol].quantityPrecision; //Need to manually include SPOT quantityPreicsion in Binance Services
        strategyobj.details = exchsymbols[strategyobj.symbol]; //raw data from exchange with additional details
        var check = await checkid(strategyobj.id);
        console.log('CHECK result', check);
        if (!(check == 'STRATEGY_FOUND')) {
            console.log('check failed for strat runner. Creating strat from runner...');
            await createpos(strategyobj);
        }
    }
    return(strategyobj);
}

async function deletestrategy(strategy) {
    var clientServerOptions = {
        uri: 'http://' + 'localhost:3000' + '/trades/' + strategy.id,
        method: 'DELETE',
        headers: {
            'Content-Type': 'application/json'
        }
    }
    return new Promise((resolve, reject) => {
        request(clientServerOptions, function (error, response, body) {
            if (!(body == undefined)) {
                resolve(body);
            } else {
                reject(error, response);
            }
        });
    });
}

// Delete All Strategies
async function deleteallstrategies() {
    strategies = await getallstrategies();
    return new Promise((resolve, reject) => {
        const promises = [];
        strategies.forEach(function (strategy) {
            promises.push(deletestrategy(strategy))
        })
        Promise.all(promises).then((results) => {
            resolve(results);
            //reject(error);
        });
    });
}

async function flattenstrategy(strategy) { //Update position on existing strategy
    var d = new Date().toISOString();
    var updatefields = {
        units: 0,
        currentpos: 0,
        positions: [],
        timestamp: d
    }
    var clientServerOptions = {
        uri: 'http://' + 'localhost:3000' + '/trades/' + strategy.id,
        method: 'PATCH',
        headers: {
            'Content-Type': 'application/json'
        },
        body: JSON.stringify(updatefields)
    }
    return new Promise((resolve, reject) => {
        request(clientServerOptions, function (error, response, body) {
            console.log('FLATTEN Strategy:', body)
            resolve(body);
            //reject(error);
        });
    });
}

// Delete All Strategies
async function flattenstrategies(filter) { //filter object: filter.exchtype, filter.id
    var exchfilter = 'OFF';
    var stratfilter = 'OFF';
    if (filter.hasOwnProperty('exchtype')) { exchfilter = filter.exchtype } else { exchfilter = 'ALL' }
    if (filter.hasOwnProperty('strategy')) { stratfilter = filter.strategyid } else { stratfilter = 'ALL' }

    strategies = await getallstrategies();
    return new Promise((resolve, reject) => {
        const promises = [];
        strategies.forEach(function (strategy) {
            if (((exchfilter == 'ALL') || (exchfilter == strategy.exchtype)) & ((stratfilter == 'ALL') || (stratfilter == strategy.id))) {
                promises.push(flattenstrategy(strategy))
            }
        })
        Promise.all(promises).then((results) => {
            resolve(results);
            //reject(error);
        });
    });
}

async function getstratsbysymbol(strategy) {
    var clientServerOptions = {
        uri: 'http://' + 'localhost:3000' + '/trades' + '?filter[where][symbol]=' + strategy.symbol + '&filter[where][exchange]=' + strategy.exchange + '&filter[where][exchtype]=' + strategy.exchtype,
        method: 'GET',
        headers: {
            'Content-Type': 'application/json'
        }
    }
    return new Promise((resolve, reject) => {
        request(clientServerOptions, function (error, response, body) {
            if (!(body == undefined)) {
                resolve(JSON.parse(body));
            } else {
                reject(error, response);
            }
        });
    });
}

async function updatepos(strategy) { //Update position on existing strategy
    console.log('updating with:', strategy);
    var d = new Date().toISOString();
    var updatefields = {
        units: strategy.units,
        avgPrice: parseFloat(strategy.avgPrice),
        currentpos: strategy.currentpos,
        positions: strategy.positions,
        historical: strategy.historical,
        cumPl: parseFloat(strategy.cumPl),
        psize: strategy.psize,
        //openPl: strategy.openPl,
        timestamp: d
    }
    var clientServerOptions = {
        uri: 'http://' + 'localhost:3000' + '/trades/' + strategy.id,
        method: 'PATCH',
        headers: {
            'Content-Type': 'application/json'
        },
        body: JSON.stringify(updatefields)
    }
    return new Promise((resolve, reject) => {
        request(clientServerOptions, function (error, response, body) {
            console.log('UPDATING STRATEGY on LB SERVER:', body)
            resolve(body);
            //reject(error);
        });
    });
}

function calchistorical(strategy) {
    var cumPl = 0;
    strategy.historical.forEach(trade => {
        //console.log(trade.netprofit);
        cumPl = cumPl + trade.netprofit
    });
    //strategy.cumPl = cumPl;
    return (cumPl);
}

var roundOff = (num, places) => {
    const x = Math.pow(10, places);
    return Math.round(num * x) / x;
}

function calcavgprice(fills) {
    var netunits = 0;
    var totalunits = 0;
    var val = 0;
    var pos = 0;
    var comm = 0;
    var avgPrice = 0;
    fills.forEach(fill => {
        val = val + Math.abs((parseFloat(fill.price) * parseFloat(fill.qty)));
        netunits = netunits + pos;
        totalunits = totalunits + Math.abs(parseFloat(fill.qty));
        comm = comm + parseFloat(fill.commission); //This might be useful later for stats
    });
    avgPrice = val / totalunits;
    console.log('SPOT avg price:', avgPrice)
    return (avgPrice);
}

function calcposition(strategy) {
    //var strategy = await getstrategy(strategyid);
    var netunits = 0;
    var totalunits = 0;
    var val = 0;
    var pos = 0;
    var result = {};
    //console.log('CALC STRAT:',strategy);
    if (strategy.hasOwnProperty('positions')) {
        strategy.positions.forEach(position => {
            if (!(position.executedQty == undefined)) {
                if (strategy.exchtype == 'SPOT') { position.avgPrice = calcavgprice(position.fills) };
                if (position.side == 'SELL') { pos = parseFloat(position.executedQty) * -1 } else { (pos = parseFloat(position.executedQty)) };
                val = val + Math.abs((parseFloat(position.avgPrice) * parseFloat(position.executedQty)));
                netunits = netunits + pos;
                totalunits = totalunits + Math.abs(parseFloat(position.executedQty));
            }
        });
    } else {
        console.log('Strategy has no positions:', strategy)
    }

    strategy.avgPrice = val / totalunits;
    strategy.units = roundOff(netunits, strategy.quantityPrecision);

    return (strategy);
}

async function createpos(strategy, newposition) { //Create new strategy on position server with initial position ({side: "BUY/SELL/CLOSE", units: n, avgPrice: n2, timestamp: n3})
    var d = new Date().toISOString();
    strategy.timestamp = d;
    strategy.positions = [];
    strategy.historical = [];
    strategy.details = [];
    strategy.avgPrice = 0;
    strategy.units = 0;
    strategy.openPl = 0;
    strategy.cumPl = 0;
    strategy.currentpos = 0;
    if (!(newposition == undefined)) {
        if (newposition.status == 'FILLED')
        strategy.positions.push(newposition);
        //strategy = calcposition(strategy);
    };

    //console.log('creating strategy:',strategy,newposition)
    var clientServerOptions = {
        uri: 'http://localhost:3000/trades',
        method: 'POST',
        headers: {
            'Content-Type': 'application/json'
        },
        body: JSON.stringify(strategy)
    }
    return new Promise((resolve, reject) => {
        request(clientServerOptions, function (error, response, body) {
            console.log('CREATING STRATEGY on LB SERVER:', body)
            if (!(body == undefined)) {
                //console.log(body);
                resolve(strategy);
            } else {
                reject(error);
            }
        });
    });
}

async function getstrategy(strategyid) { //get all information about existing strategy on position server
    // Input is strategy
    // Output is strategy Object: 
    /*     { id: 'BINANCE|FUT|TRENDWEAVER|ETHUSDT|1m',
        exchange: 'BINANCE',
        exchtype: 'FUT',
        strategy: 'TRENDWEAVER',
        quantityPrecision: 3,
        interval: '1m',
        symbol: 'ETHUSDT',
        units: 0.022,
        psize: 10,
        openPl: 0,
        cumPl: 0;
        avgPrice: 445.33,
        positions: [ [Object] ],
        historical: [ [Object] ],
        timestamp: '2020-11-06T21:07:02.452Z' }  */
    //console.log('Get strat func:',strategyid)
    var clientServerOptions = {
        uri: 'http://' + 'localhost:3000' + '/trades/' + strategyid,
        method: 'GET',
        headers: {
            'Content-Type': 'application/json'
        },
        body: ""
    }
    return new Promise((resolve, reject) => {
        request(clientServerOptions, function (error, response, body) {
            //console.log(body);
            if (!(body == undefined)) {
                resolve(JSON.parse(body));
            } else {
                console.log([error, JSON.parse(response)])
                reject(-1);
            }
        });
    });
}


async function getallstrategies() { //get all registered strategies
    var clientServerOptions = {
        uri: 'http://' + 'localhost:3000' + '/trades',
        method: 'GET',
        headers: {
            'Content-Type': 'application/json'
        },
        body: ""
    }
    return new Promise((resolve, reject) => {
        request(clientServerOptions, function (error, response, body) {
            if (!(body == undefined)) {
                resolve(JSON.parse(body));
            } else {
                reject([error, JSON.parse(response)]);
            }
        });
    });
}

async function getallstrategies_binance_fut() {
    var strategies = await getallstrategies();
    var fut = [];
    strategies.forEach(strategy => {
        if ((strategy.exchtype == 'FUT') & (strategy.exchange == 'BINANCE')) {
            fut.push(strategy)
        }
    })
    return (fut);
}


async function checkid(id) { //Checks to see if the strategy exists on position server

    var check = await getstrategy(id)
    if (check.hasOwnProperty('error')) {
        return (check.error)
    }
    else {
        if (check.hasOwnProperty('id')) {
            return ('STRATEGY_FOUND')
        } else {
            return ('UNKNOWN_ERROR')
        }
    }
}

async function addposition(order) {
    var result;

    //var openpl = (price - filled.avgPrice) * filled.units; //might have to adjust for short positions
    /*     var stratexists = await checkid(order.strategy.id)
        if (stratexists == 'ENTITY_NOT_FOUND') {
            console.log('Registering Strategy.')
            console.log('strategy: ', order.strategy.id);
            console.log('new pos:', order.executedQty)
            result = await createpos(strategy, order);
        } else {
            console.log('strategy ID exists. getting strategy ');
            //console.log('looking for:',order.strategyid) */
    strategy = await getstrategy(order.strategy.id); // get existing positions
    console.log('STRATEGY:', strategy.id)
    delete order.strategy;
    strategy.positions.push(order); // add new position
    //strategy = calcposition(strategy); // calculate total position
    strategy = calcposition(strategy);
    strategy.currentpos = order.currentpos;
    result = await updatepos(strategy);
    return (result)
}


async function gettotalunits(order) { //get total units for combined strategies trading same symbol on same excahnge
    var strategies = await getallstrategies();
    var totalunits = 0;
    strategies.forEach(element => {
        if ((element.exchange == order.exchange) & (element.exchtype == order.exchtype) & (element.symbol == order.symbol)) {
            totalunits = totalunits + element.units;
        }
    });
    return (totalunits)
}

function searchliveposition(target, allpositions) {
    var result = -1;
    allpositions.forEach(liveposition => {
        //console.log(liveposition,searchposition);
        if (liveposition.symbol == target.symbol) {
            result = parseFloat(liveposition.positionAmt);
            return (result);
        }
    })
    return (result);
}


function LB_mergedunits(target, strategies) { //merge assets from multiple strategies trading same asset
    totalunits = 0;
    strategies.forEach(strategy => {
        if (target.symbol == strategy.symbol) {
            totalunits = totalunits + strategy.units;
        }
    })
    return (totalunits);
}

function filterbyasset(strategies) { //curate list of strategies by removing same asset strategies
    assets = [];
    result = [];
    strategies.forEach(strategy => {
        var sym = strategy.symbol;
        assets[sym] = strategy;
    });
    for (const key in assets) {
        result.push(assets[key]);
    }
    return (result);
}


async function syncliveposition(strategy, newClientOrderId) { //Sync one strategy.
    var orders = []
    var LB_strategies = await getallstrategies_binance_fut();
    var mergedunits = LB_mergedunits(strategy, LB_strategies);

    var liveunits = searchliveposition(strategy, Live_strategies);
    var adjustquantity = (mergedunits - liveunits).toFixed(strategy.quantityPrecision);
    console.log("Symbol:", strategy.symbol, "LB units:", mergedunits, "Live units:", liveunits, 'Adjustment Order:', adjustquantity, 'Precision:', strategy.quantityPrecision);
    if (!(adjustquantity == 0)) {
        var order = BINANCE.createmarketorder(strategy.symbol, adjustquantity, newClientOrderId);
        orders.push(order);
    }
    //console.log('ORDERS for Binance:',orders);
    await BINANCE.sendbatchorders(orders); //uses promise all
    //console.log('Result:',result);
    return (orders);
}

function getsyncorder(strategy, livepositions, LB_strategies, newClientOrderId) { //Sync one strategy.
    var order;
    var mergedunits = LB_mergedunits(strategy, LB_strategies);
    var liveunits = searchliveposition(strategy, livepositions);
    var adjustquantity = (mergedunits - liveunits).toFixed(strategy.quantityPrecision);
    console.log("Symbol:", strategy.symbol, "LB units:", mergedunits, "Live units:", liveunits, 'Adjustment Order:', adjustquantity, 'Precision', strategy.quantityPrecision);
    if (!(adjustquantity == 0)) {
        order = BINANCE.createmarketorder(strategy.symbol, adjustquantity, strategy.exchtype, newClientOrderId);
    } else {
        order = -1;
    }
    console.log('Gensyncorder:', order)
    return (order);
}

async function synclivepositions(callingstrategy, newClientOrderId) { //need to pass thru the calling strategy so we can tag with actual fills
    console.log('LOoking for orderID in Synclivepositions:', newClientOrderId);
    var livepositions = await BINANCE.getlivepositions_fut(0);
    var LB_strategies = await getallstrategies_binance_fut();
    var LB_nodupes = filterbyasset(LB_strategies); //no duplicate strategies with same asset
    var orders = []
    LB_nodupes.forEach(strategy => {
        var order = getsyncorder(strategy, livepositions, LB_strategies, newClientOrderId);
        if (!(order == -1)) { orders.push(order) }
    });
    console.log("Order Array:", orders);
    var results = await BINANCE.sendbatchorders(orders, callingstrategy);
    console.log('result of orders. should have Order ID:', results)
    return (results);
}


async function sendallstrategyorders(orders) { //order format:  { strategy: strategy, signal: order.signal, units: parseFloat(order.positionSize), avgPrice: parseFloat(order.avgPrice), timestamp: dt }
    return new Promise(resolve => {
        const promises = [];
        orders.forEach(order => {
            //var dt = new Date().toISOString();
            var strategy = order.strategy; //used just for looking up existing strategy via id or register new strat. Will wipe out orders with LB version then add new position
            var position = { signal: order.signal, units: parseFloat(order.units), avgPrice: parseFloat(order.avgPrice), timestamp: order.timestamp, clientOrderId: order.clientOrderId };
            promises.push(addposition(strategy, position))
        })
        Promise.all(promises).then((results) => {
            resolve(results)
        });
    });
}

function createcloseorder(strategy, currentprices) {
    var markPrice = -1;
    var d = new Date().toISOString();
    currentprices.forEach(currentprice => {
        if (currentprice.symbol == strategy.symbol) {
            markPrice = currentprice.markPrice;
        }
    });
    var closeorder = { signal: 'CLOSE', avgPrice: parseFloat(markPrice), timestamp: d };

    return (closeorder)
}

function calcprofit(positions) {
    var val = 0;
    var dir = 0;
    var exitprice = 0;
    var netprofit = 0;
    var units = 0;
    positions.forEach(trade => {
        if (!(trade.quantity == 0)) {
            if ((trade.signal == 'LE1') || (trade.signal == 'LE2')) {
                val = val + (parseFloat(trade.executedQty) * parseFloat(trade.avgPrice));
                units = units + parseFloat(trade.executedQty);
                dir = 1;
            }
            if ((trade.signal == 'SE1') || (trade.signal == 'SE2')) {
                val = val + (parseFloat(trade.executedQty) * parseFloat(trade.avgPrice));
                units = units + parseFloat(trade.executedQty);
                dir = -1;
            }
            if ((trade.signal == 'LEX') || (trade.signal == 'SEX')) {
                exitprice = parseFloat(trade.avgPrice);
            }
        } else { console.log('quantity less than 0. skipping trade calc.') }
    });

    netprofit = (exitprice * units) - val;
    if (dir == -1) { netprofit = netprofit * -1 }
    return (netprofit)
}

async function archivetrades(strategyid) {
    console.log('archiving trade...');
    var result;
    var stratexists = await checkid(strategyid)
    if (stratexists == 'ENTITY_NOT_FOUND') {
        console.log('Strategy not registered.', strategyid);
        result = 'Strategy not found. No trades to archive.';
    } else {
        strategy = await getstrategy(strategyid); // get existing positions
        if (strategy.positions.length > 0) {
            if (!(strategy.hasOwnProperty('historical'))) { console.log('creating historical attribute.'); strategy.historical = [] }
            var tradeobj = {};
            tradeobj.trades = strategy.positions;
            tradeobj.netprofit = calcprofit(strategy.positions);
            //console.log('CALC NET Profit:', strategy.positions);
            strategy.historical[strategy.historical.length] = tradeobj;
            strategy.positions = []; // flattens all positions -- need to first capture these and move them into a transaction field or other model
            strategy.units = 0;
            strategy.avgPrice = 0;
            strategy.cumPl = calchistorical(strategy);
            //console.log('updated strategy with closed positions:', strategy)
            result = await updatepos(strategy);
        } else {
            result = 'No open trades found for strategy. Nothing to archive.';
        }
    }
    return (result);
}

function closetrade(strategy, currentprices, positions, historical) { //create close order and archive trades to historical archive
    //console.log('CloseTrade received positions/historical:',positions,historical);
    var closeorder = createcloseorder(strategy, currentprices);
    positions.push(closeorder);
    //console.log("new positions array:",positions)
    var tradeobj = {};
    tradeobj.trades = positions;
    tradeobj.netprofit = calcprofit(positions);
    historical[historical.length] = tradeobj;
    console.log('Trade Obj:', tradeobj)
    return (historical);
}

async function closeallpositions(strategy, currentprices) { //Input is [{strategy:strategy}] 

    var result;
    var stratexists = await checkid(strategy.id)
    if (stratexists == 'ENTITY_NOT_FOUND') {
        console.log('Strategy not registered.', strategy);
        result = [];
    } else {
        console.log('Closing Positions for Strategy:');
        strategy = await getstrategy(strategy); // get existing positions
        console.log('Retrieved Strategy:', strategy);
        //var closeorder = createcloseorder(strategy,currentprices);

        if (!(strategy.hasOwnProperty('historical'))) { console.log('creating historical attribute.'); strategy.historical = [] }
        //console.log('trying to push:',strategy.positions[0]);

        strategy.historical = closetrade(strategy, currentprices, strategy.positions, strategy.historical);
        strategy.positions = []; // flattens all positions -- need to first capture these and move them into a transaction field or other model
        strategy.units = 0;
        strategy.avgPrice = 0;
        strategy.cumPl = calchistorical(strategy);
        result = await updatepos(strategy);
    }
    return (result)
}

async function closeallstrategyorders(orders) { //closes all orders for multiple strategies, or single strategy. order must have .strategy property for lookup
    /*     Orders from Tactical ES is simple strategy object:
        [{strategy: strategy, signal: signal, avgPrice: parseFloat(price), timestamp: lastclosedate},...] */
    var currentprices = await BINANCE.getlivepositions_fut(0); //not really interested in positions, just using the same query to get prices
    return new Promise(resolve => {
        const promises = [];
        orders.forEach(order => {
            var strategy = order.strategy;
            promises.push(closeallpositions(strategy, currentprices))
        })
        Promise.all(promises).then((results) => {
            console.log('Promise.all results:', results);
            resolve(results);
        });
    });
}

async function updatefill(strategy, liveresult) {
    console.log('updating fill data...')
    var lbstrat = await getstrategy(strategy);
    return new Promise(resolve => {
        var orderID = liveresult[0].clientOrderId;
        positions = lbstrat.positions;
        var updated = 0;
        positions.forEach(pos => {
            if (pos.clientOrderId == orderID) {
                pos.fillPrice = parseFloat(liveresult[0].avgPrice);
                updated = 1;
            }
        })
        if (updated == 1) {
            lbstrat.positions = positions;
            var result = updatepos(lbstrat);
            resolve(result);
        }
    });
}

async function storeorder(strategy, order) {

}

function createmarketorder(order) {

    if (order.quantity > 0) { order.side = 'BUY' } else { order.side = 'SELL' }
    const symbol = order.symbol;
    const quantity = Math.abs(order.quantity);
    const newClientOrderId = order.orderid;
    const callingStrategy = order.strategy.id;

    var symbol = order.strategy.symbol;
    var exchtype = order.strategy.exchtype;


    order.type = 'MARKET';
    order.newOrderRespType = 'RESULT';
    order.exchtype = exchtype;
    order.clientOrderId = order.orderid;
    return (order)
}

async function getnetposition(strategyid) {
    var strat = getstrategy(strategyid);
    var netpos = calcposition(strat);
}

async function execorder(order) {
    /*     //var order = { 
            strategy: strategy, 
            signal: signal, 
            quantity: parseFloat(units), <-- is this absolute or not?
            tradeid: tradeid, 
            clientOrderId: clientOrderId, 
            barclose: parseFloat(price), 
            currentpos: currentpos,
            bartime: lastclosetime, 
            timestamp: lastclosedate, 
            tradetype: tradetype,
             ordertype: 'MARKET',
             symbol: symbol, //added
            side: 'BUY'
            };  */
    var result;
    order.symbol = order.strategy.symbol;
    if (order.tradetype == 'CLOSEALL') {
        var strategy = await getstrategy(order.strategy.id);
        order.quantity = strategy.units * -1;
    }
    if (order.tradetype == 'REVERSE') {
        var strategy = await getstrategy(order.strategy.id);
        order.quantity = order.quantity + (strategy.units * -1);
    }
    if (order.tradetype == 'SYNC') { //Need to prioritize SYNC orders to update LB before sending any live orders
        var strategy = await getstrategy(order.strategy.id);
        if (Math.abs(order.currenpos) == 2) { order.quantity = order.quantity * 2 } //double trade size if we're in a second entry
        order.quantity = order.quantity + (strategy.units * -1); //reverse out any existing position on server
    }

    if (!(order.quantity == 0)) {
        if (order.quantity > 0) { order.side = 'BUY' } else { order.side = 'SELL' }
        order.quantity = Math.abs(order.quantity);
        var orderresult = await BINANCE.sendliveorder(order); //send order to Binance. This may need to be changed to a aggregated live order
/*         var combinedOrder = {
            ...order,
            ...orderresult
        }; */
        order.result = orderresult;
        //console.log(combinedOrder);
        console.log('adding position to LB');
        result = await addposition(order);

        if (order.tradetype == 'CLOSEALL') {
            console.log('Archiving closed trade...')
            result = await archivetrades(order.strategy.id);
        }
    } else {
        result = 'quantity of order is 0. Not executed.'
    }
    return (result);

    /* filled order result from binance {
      "orderId": 4147198332,
      "symbol": "ETHUSDT",
      "status": "FILLED",
      "clientOrderId": "TACTICALES_ETHUSDT_1m",
      "price": "0",
      "avgPrice": "470.53000",
      "origQty": "0.021",
      "executedQty": "0.021",
      "cumQuote": "9.88113",
      "timeInForce": "GTC",
      "type": "MARKET",
      "reduceOnly": false,
      "closePosition": false,
      "side": "BUY",
      "positionSide": "BOTH",
      "stopPrice": "0",
      "workingType": "CONTRACT_PRICE",
      "priceProtect": false,
      "origType": "MARKET",
      "time": 1605115092277,
      "updateTime": 1605115092444
    } */
}


//synclivepositions()
//var strategy = createstrategy("BINANCE", "FUT", "TESTSTRAT", "EOSUSDT", 3, "1h");
//getstratsbysymbol(strategy).then(console.log);
//gettotalunits(strategy).then(console.log);
//getallstrategies().then(console.log);
//var newpos = { units: -1, avgPrice: 250 };
//addposition(strategy, newpos).then(console.log)
//getstrategy(strategy).then(console.log)
//getallstrategies().then(console.log)
//closeallpositions(strategy).then(console.log)
//deletestrategy(strategy).then(console.log)
//deleteallstrategies().then(console.log)

exports.initstrategy = initstrategy;
exports.flattenstrategies = flattenstrategies;
exports.calcposition = calcposition;
exports.execorder = execorder;
exports.archivetrades = archivetrades;
exports.updatefill = updatefill;
exports.syncliveposition = syncliveposition;
exports.sendallstrategyorders = sendallstrategyorders;
exports.closeallstrategyorders = closeallstrategyorders;
/* exports.createstrategy = createstrategy; // Create strategy object: createstrategy("BINANCE","FUT","TACTICAL_ES","BCHUSDT", 2, "1m");
exports.createstrategy_bin = createstrategy_bin; */
exports.createstrategyobj = createstrategyobj;
exports.addposition = addposition; // create or update position for strategy: updatestrategy(strategy,newpos) newpos = {units: n, avgPrice: n2, timestamp: n3}
exports.gettotalunits = gettotalunits; // Gets all combioned units of symbol on same exchange: gettotalunits(strategy)
exports.getallstrategies = getallstrategies; // Gets every strategy on every symbol and exchange and returns array of strategy objects [strategy1, strategy2]
exports.getstrategy = getstrategy; // Get specific strategy info: getstrategy(strategy)
exports.getstratsbysymbol = getstratsbysymbol; // Get all strategies by symbol on same exchange: getstratsbysymbol(strategy)
exports.deletestrategy = deletestrategy; //deletestrategy(strategy)
exports.closeallpositions = closeallpositions; //close all open positions for strategy
exports.getallstrategies_binance_fut = getallstrategies_binance_fut;
exports.synclivepositions = synclivepositions; //syncs LB with Live positions. Should make option to sync only specific strategy symbol positions instead of all.
exports.deleteallstrategies = deleteallstrategies;
exports.createpos = createpos;
exports.checkid = checkid;

/* [{ id: 'BINANCE|FUT|FUNDING_TRADER|YFIIUSDT|60m',
exchange: 'BINANCE',
exchtype: 'FUT',
strategy: 'FUNDING_TRADER',
quantityPrecision: 3,
interval: '60m',
symbol: 'YFIIUSDT',
units: 0,
avgPrice: 0,
positions: [],
timestamp: '2020-10-30T15:03:57.205Z' } ] */