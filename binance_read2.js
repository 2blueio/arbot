//const { db } = require('./schemas/TestOrders_s');

db = require('./db/database')
TestOrders = require('./schemas/TestOrders_s')
PremiumIndex = require('./schemas/PremiumIndex_s')

async function getOrders(){ 
    return new Promise(resolve => {
    var query = TestOrders.find().sort({timestamp: -1})
        query.exec(function (err, docs) {
            resolve(docs)
          });
        });
}


function printOrders(priceArray){
    priceArray.forEach( function (priceObj) {
        if (!(priceObj == undefined)){
        //console.log(priceObj);
        var samplesize = priceObj.prices.length;
        var lineout = [priceObj.symbol];
        //console.log(lineout,samplesize)
        var trial = 1;
        while (trial < samplesize){
            //console.log('trial:',trial);
            let profit = (parseFloat(priceObj.prices[trial].price) / parseFloat((priceObj.prices[0].price)) - 1)*100 - 0.21;
            lineout.push(profit.toFixed(7));
            trial++;
        }
        console.log(lineout.toString());
        };
    });
    console.log('Done.')
}

async function getPremiums(){ 
    return new Promise(resolve => {
    var query = PremiumIndex.find({symbol: 'YFIIUSDT'}).sort({timestamp: -1})
        query.exec(function (err, docs) {
            resolve(docs)
          });
        });
}

var sample = { symbol: 'YFIIUSDT',
  fundingTime: 1602259200004,
  fundingRate: '-0.00750000',
  prices:
   [ { description: 0,
       time:'2020-10-09T22:24:02.041Z',
       price: '2181.1' },
     { description: 1,
       time: '2020-10-09T22:39:01.259Z',
       price: '2169.0' },
     { description: 2,
       time: '2020-10-09T22:54:01.505Z',
       price: '2227.0' },
     { description: 3,
       time: '2020-10-09T23:09:01.928Z',
       price: '2304.7' },
     { description: 4,
       time:' 2020-10-09T23:24:01.410Z',
       price: '2366.5' },
     { description: 5,
       time: '2020-10-09T23:39:01.676Z',
       price: '2277.6' } ] }



async function main(){
    //var orders = await getOrders();
    //console.log(orders)
   // await printOrders(orders);
   var premiums = await getPremiums();
   console.log("lastFundrate = array.new_float(0)");
    premiums.forEach(function(item){
        console.log("array.push(lastFundrate,"+item.lastFundingRate+")\n")
    });
    console.log("lastFundrate = array.new_int(0)");
    premiums.forEach(function(item){
        console.log("array.push(fundingTime,"+item.time+")\n")
    });
    console.log("lastFundrate = array.new_int(0)");
    premiums.forEach(function(item){
        console.log("array.push(nextFundingTime,"+item.nextFundingTime+")\n")
    });
    console.log("lastFundrate = array.new_float(0)");
    premiums.forEach(function(item){
        console.log("array.push(markPrice,"+item.markPrice+")\n")
    });
   //console.log("array.push(a, close)" + premiums.length)

   //console.log(premiums)
}
//printOrders(sample)
//getOrders(printOrders);
main();
//db.close();



    