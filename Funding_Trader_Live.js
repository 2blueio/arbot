/* Funding Trader Live V 1.0

Algorithm downloads realtime funding data and buys or sell before the funding time 

To Do: Track positions by order so we can trade same symbols on othe strategies, calculate positionsize based fixed fractional
Create Test Mode for easy testing (change highlow rates, delay and scheduler).
Send Orders to Transaction Database

*/



//Variables and Required Modules


const loopback = require('./loopback.js');
//loopback functions: createstrategy("BINANCE","FUT","TACTICAL_ES","BCHUSDT","1m"), addposition(strategy,pos), gettotalunits(strategy), getallstrategies(), getstrategy(strategy), getstratsbysymbol(strategy), deletestrategy(strategy)//
var request = require('request');
const crypto = require('crypto');
var schedule = require('node-schedule');
const api_credentials = require('./api_credentials.js');
const { resolve } = require('path');

const toptickers = ['UNIUSDT', 'SUSHIUSDT', 'YFIIUSDT', 'BNBUSDT', 'CRVUSDT', 'DEFIUSDT', 'HNTUSDT', 'ICXUSDT', 'WAVESUSDT', 'TRBUSDT', 'FILUSDT','OCEANUSDT']; //For info purposes. These had the best results in Trading View
const psize = 10; //Dollar Position Size
var lowrate = -0.002;  //historical filters for funding rates. 
var highrate = 0.002;
const entryminutes = 60; //entry minutes before funding
const exitminutes = 2; //exit before funding


//Functions

function signature(query_string) {
  return crypto
    .createHmac('sha256', api_credentials.binanceSecret)
    .update(query_string)
    .digest('hex');
}

function printOrders(priceArray) {
  priceArray.forEach(function (priceObj) {
    if (!(priceObj == undefined)) {
      //console.log(priceObj);
      var samplesize = priceObj.prices.length;
      var lineout = [priceObj.symbol];
      //console.log(lineout,samplesize)
      var trial = 1;
      while (trial < samplesize) {
        //console.log('trial:',trial);
        let profit = (parseFloat(priceObj.prices[trial].price) / parseFloat((priceObj.prices[0].price)) - 1) * 100 - 0.21;
        lineout.push(profit.toFixed(7));
        trial++;
      }
      console.log(lineout.toString());
    };
  });
  console.log('Done.')
}


function filteredsymbols(lowrate, highrate, Arr, psize, symbolsinfo, dump) {
  //fundingRec.symbol, .quantityPrecision, .lastFundingRate, positionSize, markPrice.toFixed(quantPrecision), .side, 
  //Filter results by rates and return results.
  var result = [];
  //console.log(Arr);
  console.log('filtering by:', lowrate, highrate)

  var lowreccount = 0;
  var highreccount = 0;
  Arr.forEach(function (fundingRec) {
    //console.log('comp:',parseFloat(fundingRec.lastFundingRate),lowrate)
    
    //var quantPrecision = symbolsinfo[fundingRec.symbol].quantityPrecision;
    //fundingRec.quantityPrecision = quantPrecision;
    //fundingRec.strategy = loopback.createstrategy('BINANCE', 'FUT', 'FUNDING_TRADER', fundingRec.symbol, fundingRec.quantityPrecision, '60m');
    fundingRec.strategy = LOOPBACK.createstrategy_bin('FUNDING_TRADER', 'FUT', fundingRec.symbol, '60m', psize);
    if (parseFloat(fundingRec.lastFundingRate) <= lowrate) {
      if (dump > 0) {
        console.log('dump:', fundingRec)
      }
      fundingRec.positionSize = Number((psize / parseFloat(fundingRec.markPrice))).toFixed(quantPrecision); //  - parseFloat(fundingRec.positionAmt)).toFixed(quantPrecision); extra filter to check if there's already a position
      if (fundingRec.positionSize > 0) {
        if (toptickers.includes(fundingRec.symbol)) {
          fundingRec.side = 'BUY'; 
          fundingRec.signal = 'LE1';
          fundingRec.avgPrice = fundingRec.markPrice; //Normalizing fields to match database names
          fundingRec.units = fundingRec.positionSize; //Normalizing fields to match database names
          result.push(fundingRec);
          lowreccount++
        } else {
          console.log(fundingRec.symbol, ' not found in allowed tickers.')
        }
      } else {
        console.log('Position already exists for ', fundingRec)
      }
    }
    if (parseFloat(fundingRec.lastFundingRate) >= highrate) {
      if (dump > 0) {
        console.log('dump:', fundingRec)
      }
      fundingRec.positionSize = Number(((psize / parseFloat(fundingRec.markPrice))) * -1).toFixed(quantPrecision);//  - parseFloat(fundingRec.positionAmt)).toFixed(quantPrecision); //extra filter to check if there's already a position
      if (fundingRec.positionSize < 0) {
        if (toptickers.includes(fundingRec.symbol)) {
          console.log(fundingRec.symbol, ' allowed. Adding Trade to Queue.')
          fundingRec.positionSize = Math.abs(fundingRec.positionSize);
          fundingRec.side = 'SELL';
          fundingRec.signal = 'SE1';
          result.push(fundingRec);
          highreccount++
        } else {
          console.log(fundingRec.symbol, ' not found in allowed tickers.')
        }
      } else {
        console.log('Position already exists for ', fundingRec)
      }
    }
  })

  console.log("Tickers with low funding:", lowreccount)
  console.log("Tickers with high funding:", highreccount)
  return (result)
}




//Filter out zero positions
function filteredzero(positions) {
  var filtered = [];
  positions.forEach(function (arrayItem) {
    if (!(parseFloat(arrayItem.positionAmt) == 0)) {
      filtered.push(arrayItem)
    }
  })
  return (filtered)
}


async function getpositions(zerobal) { //0 paramater returns all symbols including zero positions which is needed so we can match with new orders with fewer calls. 1 will only return existing positions.
/*  Returns array of objects:
  [{
    "symbol": "EOSUSDT",
    "positionAmt": "0.0",
    "entryPrice": "0.0000",
    "markPrice": "2.31001768",
    "unRealizedProfit": "0.00000000",
    "liquidationPrice": "0",
    "leverage": "20",
    "maxNotionalValue": "250000",
    "marginType": "cross",
    "isolatedMargin": "0.00000000",
    "isAutoAddMargin": "false",
    "positionSide": "BOTH"
}, ...]*/

  var ts = Date.now();
  //console.log(ts)
  var url = 'https://fapi.binance.com/fapi/v1/positionRisk?timestamp=' + ts + '&signature='
  var uri = 'timestamp=' + ts
  return new Promise((resolve, reject) => {
    request({
      method: 'GET',
      url: url + signature(uri),
      headers: {
        'Content-Type': 'application/json',
        'X-MBX-APIKEY': api_credentials.binanceKey,
      },
      body: ""
    }, function (error, response, body) {
      if (!(body == undefined)) {
        var result = JSON.parse(body);
        if (zerobal == 1) {
          let filtered = filteredzero(result) //just return symbols with positions
          resolve(filtered)
        } else {
          resolve(result)
        }
      } else {
        reject([error, response]);
      }

    });
  });
}

//get Exchange info for ticker Amt Precision 
async function exchinfo() {
  var ts = Date.now();
  //console.log(ts)
  var url = 'https://fapi.binance.com/fapi/v1/exchangeInfo'
  var uri = 'timestamp=' + ts
  return new Promise((resolve, reject) => {
    request({
      method: 'GET',
      url: url, // + signature(uri),
      headers: {
        'Content-Type': 'application/json',
        'X-MBX-APIKEY': api_credentials.binanceKey,
      },
      body: ""
    }, function (error, response, body) {
      if (!(body == undefined)) {
        let result = JSON.parse(body);
        var resultsf = [];
        result.symbols.forEach(function (arrayItem) { //For building associative array use resultsf intead of results.
          if (!(arrayItem == undefined)) {
            if (!(arrayItem.symbol == 'lookuperr')) { //don't try to add failed lookups
              resultsf[arrayItem.symbol] = arrayItem;
            }
          }
        })
        resolve(resultsf)
      } else {
        reject([error, response]);
      }
    });
  });
}

//Looks up funding data for one symbol and returns new data merged with paramater object
async function updatesymbol(symbolobj) {
  
/*    Sample Query Result:
  {
    "symbol": "BTCUSDT",
    "markPrice": "13383.69000000",
    "indexPrice": "13387.42598644",
    "lastFundingRate": "-0.00003871",
    "interestRate": "0.00010000",
    "nextFundingTime": 1604390400000,
    "time": 1604384110000
} */

  return new Promise((resolve, reject) => {
    request({
      method: 'GET',
      url: 'https://fapi.binance.com/fapi/v1/premiumIndex?symbol=' + symbolobj.symbol,
      headers: {
        'Content-Type': 'application/json',
        'X-API-KEY': api_credentials.binanceKey,
        'X-API-SECRET': api_credentials.binanceSecret,
        'signature': '13f1576a61a116036a48639f5cd613616ae90d9c82b50523bf012492bbd9c0dd'
      },
      body: ""
    }, function (error, response, body) {
      if (!(body == undefined)) {
        var result = JSON.parse(body);
        let merged = {
          ...symbolobj,
          ...result
        };
        resolve(merged)
      } else {
        //console.log('Bad Symbol: ',symbol,' Result: ',body)
        //reject('Problem Symbol:' + symbol);
        resolve({ symbol: 'lookuperr' }) //custom error so that entire promise all doesn't fail and crash script.
      }

    });
  });
}

//This little function promisifies all the separate requests for realtime funding data on each symbol
//and returns an array of objects containing realtime funding data
async function updatesymbols(symbols, minrate, maxrate) {
  return new Promise(resolve => {
    const promises = [];
    //const keys = Object.keys(symbols);
    //console.log(keys)
    symbols.forEach(function (symbol) {
      promises.push(updatesymbol(symbol))
    })

    Promise.all(promises).then((results) => {
      var resultsf = [];
      results.forEach(function (arrayItem) { //For building associative array use resultsf intead of results.
        if (!(arrayItem == undefined)) {
          if (!(arrayItem.symbol == 'lookuperr')) { //don't try to add failed lookups
            resultsf[arrayItem.symbol] = arrayItem;
          }
        }
      })
      resolve(results); //for all results in flat object array
    })
  })
}

async function LB_sendorders(orders) {
  return new Promise(resolve => {
    const promises = [];
    orders.forEach(order => {
      var dt = new Date().toISOString();
      var strategy = loopback.createstrategy('BINANCE', 'FUT', 'FUNDING_TRADER', order.symbol, order.quantityPrecision, '60m');
      var position = { units: parseFloat(order.positionSize), avgPrice: parseFloat(order.markPrice), timestamp: dt };
      promises.push(loopback.addposition(strategy, position))
    })
    Promise.all(promises).then((results) => {
      resolve(results)
    });
  });
}

async function LB_closeall(orders) {
  return new Promise(resolve => {
    const promises = [];
    orders.forEach(order => {
      var strategy = loopback.createstrategy('BINANCE', 'FUT', 'FUNDING_TRADER', order.symbol, order.quantityPrecision, '60m');
      promises.push(loopback.closeallpositions(strategy))
    })
    Promise.all(promises).then((results) => {
      resolve(results)
    });
  });
}


async function open_positions(lowrate, highrate, psize, testonly) {
  var exchsymbols = await exchinfo(); //gets details about symbosl such as quantity Precision and trade types available (needed for trading)
  var positions = await getpositions(0); //0 paramater returns all symbols including zero positions which is needed so we can match with new orders with fewer calls. 1 will only return existing positions.
  var results = [];
  var symbols = await updatesymbols(positions); //adds funding rates to all symbols
  var orders = filteredsymbols(lowrate, highrate, symbols, psize, exchsymbols, testonly); //filter symbols by rate thresholds add precision
  //orders.symbol, .quantityPrecision, .lastFundingRate, positionSize, markPrice.toFixed(quantPrecision), .side,
  if (orders.length > 0) {
    //var results = await LB_sendorders(orders);
    var results = await loopback.sendallstrategyorders(orders);
    console.log('Sent to LB. Results:', results);
    var status = await loopback.synclivepositions();
    console.log('Sync status:', status)
  }
  return (orders);
}

function sleep(ms) {
  return new Promise(
    resolve => setTimeout(resolve, ms)
  );
}

async function timetillfunding(){
  var fundinginfo = {};
  fundinginfo.symbol = 'BTCUSDT';
  fundinginfo = await updatesymbol(fundinginfo);
  var delay = (fundinginfo.nextFundingTime - (exitminutes * 60000)) - Date.now();
  return(delay)
}

async function initateTradeSequence(exitminutes) {

  console.log("Initiating Trade Sequence\n", Date());
  var newpositions = await open_positions(lowrate, highrate, psize, 0); //Opens Positions for multiple symbols
  console.log('New positions:' + newpositions.length);
  console.log(newpositions);

  if (newpositions.length > 0) {

    var delay = await timetillfunding();
    if (testmode == 1) { delay = 30000 };

    console.log('Sleeping for ', (delay / 60000), ' minutes ', Date())

    await sleep(delay); 
    //await sleep(30000);
   
    console.log('Closing all open positions: ', Date())
    //var closedpositions = await LB_closeall(newpositions);
    //console.log('positions being sent to closeallstrategyorders newpositions);
    //Need to update the price so that closed trades can be calculated
    var closedpositions = await loopback.closeallstrategyorders(newpositions);
    var status = await loopback.synclivepositions();
    console.log('Closed ' + closedpositions.length + ' position(s):',status);
    //console.log(status);
  } else {
    console.log('No new positions. Waiting for next funding window.')
  }

  return (Date().toString())
}


function scheduleTrades() {
  //Funding Times are at 09:00, 01:00, and 17:00
  console.log('Scheduling Trades for Funding Times: 09:00, 01:00, and 17:00');
  var rule1 = new schedule.RecurrenceRule();
  //rule.dayOfWeek = [0, new schedule.Range(4, 6)];
  //rule.hour = [0000,0800,1600];
  rule1.hour = 7; //Enter 1 hour before funding
  rule1.minute = 0;
  //console.log(rule1);

  var rule2 = new schedule.RecurrenceRule();
  rule2.hour = 15; //Enter 1 hour before funding
  rule2.minute = 0;

  var rule3 = new schedule.RecurrenceRule();
  rule3.hour = 23; //Enter 1 hour before funding
  rule3.minute = 0;

  console.log('Hours: ',rule1.hour,rule2.hour,rule3.hour);

  var r1 = schedule.scheduleJob(rule1, function () {
    console.log('Schedule 1 Triggered ', Date());
    initateTradeSequence(exitminutes).then(console.log);
  });

  var r2 = schedule.scheduleJob(rule2, function () {
    console.log('Schedule 2 Triggered ', Date());
    initateTradeSequence(exitminutes).then(console.log);
  });

  var r3 = schedule.scheduleJob(rule3, function () {
    console.log('Schedule 3 Triggered ', Date());
    initateTradeSequence(exitminutes).then(console.log);
  });
};

function scheduletest(hour, minute) {
  var testrule1 = new schedule.RecurrenceRule();
  testrule1.hour = hour;
  testrule1.minute = minute;
  var tr = schedule.scheduleJob(testrule1, function () {
    console.log('Test Rule 1 Triggered ', Date());;
    initateTradeSequence(exitminutes).then(console.log);
  });
}

const testmode = process.argv[2];

if (testmode == 1) {
  console.log('Test Mode Activated...')
  lowrate = 0;
  highrate = 0.01;
  initateTradeSequence(2).then(console.log); //Use this to initate trade immediately for testing
} else {
  scheduleTrades() //Schedule Daily Funding Trades for live use
}

//scheduletest(11,0) //use for testing scheduler




console.log('Trade Program Running...')