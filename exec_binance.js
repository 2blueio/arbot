//Standalone script to test simple strategy to buy futures with negative funding rates before the funding time


var schedule = require('node-schedule');
const api_credentials = require('./api_credentials.js');
var request = require('request');
db = require('./db/database')
TestOrders = require('./schemas/TestOrders_s');
PremiumIndex = require('./schemas/PremiumIndex_s')




async function gettickers(minrate,maxrate){
    return new Promise(resolve => {
    request({
        method: 'GET',
        url: 'https://fapi.binance.com/fapi/v1/fundingRate?symbol=&limit=100',
        headers: {
          'Content-Type': 'application/json',
          'X-API-KEY': api_credentials.binanceKey,
          'X-API-SECRET': api_credentials.binanceSecret,
          'signature': '13f1576a61a116036a48639f5cd613616ae90d9c82b50523bf012492bbd9c0dd'
        },
        body: ""
      }, function (error, response, body) {
        console.log('Status:', response.statusCode);
        // console.log('Headers:', response.headers);
        // console.log('Response:', JSON.parse(body));
        var result = JSON.parse(body);
        //result.sort((a, b) => (parseFloat(a.fundingRate) > parseFloat(b.fundingRate)) ? 1 : -1)
        /* result.forEach(function (arrayItem) {
                if (arrayItem.fundingRate > maxrate || arrayItem.fundingRate < minrate) {
                    console.log(arrayItem);
                }
        }); */
        //Need to de-duplicate and keep only latest rate
        var resultArr = [];
        result.forEach(function(ArrayItem){
          if (ArrayItem.symbol in resultArr){ //if it's in the array then check for latest and update
            if (ArrayItem.fundingTime > resultArr[ArrayItem.symbol].fundingTime){
              resultArr[ArrayItem.symbol] = ArrayItem;
            }
          } else {
            resultArr[ArrayItem.symbol] = ArrayItem;
          }
        })
        //console.log(resultArr)
        //const resultf = result.filter(item => item.fundingRate < minrate);

        resolve(resultArr)
      });
      });
}

//Looks up funding data for one symbol and returns object
async function updatesymbol(symbol){
  
  return new Promise((resolve, reject) => {
  request({
      method: 'GET',
      url: 'https://fapi.binance.com/fapi/v1/premiumIndex?symbol=' + symbol,
      headers: {
        'Content-Type': 'application/json',
        'X-API-KEY': api_credentials.binanceKey,
        'X-API-SECRET': api_credentials.binanceSecret,
        'signature': '13f1576a61a116036a48639f5cd613616ae90d9c82b50523bf012492bbd9c0dd'
      },
      body: ""
    }, function (error, response, body) {
      if (!(body == undefined)){
        var result = JSON.parse(body);
        resolve(result)
      } else {
        console.log('Bad Symbol: ',symbol,' Result: ',body)
        //reject('Problem Symbol:' + symbol);
        resolve({symbol: 'lookuperr'}) //custom error so that entire promise all doesn't fail and crash script.
      }
     
    });
    });
}

//This little function promisifies all the separate requests for realtime funding data on each symbol
//and returns an array of objects containing realtime funding data
async function updatesymbols(symbols,minrate,maxrate){
  return new Promise(resolve => {
  const promises = [];
  const keys = Object.keys(symbols);
  //console.log(keys)
  keys.forEach(function (key) {
    promises.push(updatesymbol(key))
  })

  Promise.all(promises).then((results)=> {
    var resultsf = [];
    results.forEach(function(arrayItem){
      if (!(arrayItem == undefined)){
        if (!(arrayItem.symbol == 'lookuperr')){ //don't try to add failed lookups
          if (parseFloat(arrayItem.lastFundingRate) < minrate) {
          resultsf[arrayItem.symbol] = arrayItem;
      }
      } 
      }
    })
    //resolve(resultsf); //for filtered results in associative array
    console.log(results)
    resolve(results); //for all results in flat object array
  })
})
}

//Looks up price (not used since we need funding data)
async function get_price(symbol){
    return new Promise(resolve => {
    request({
        method: 'GET',
        url: 'https://fapi.binance.com/fapi/v1/ticker/24hr?symbol=' + symbol,
        headers: {
          'Content-Type': 'application/json',
          'X-API-KEY': api_credentials.binanceKey,
          'X-API-SECRET': api_credentials.binanceSecret,
          'signature': '13f1576a61a116036a48639f5cd613616ae90d9c82b50523bf012492bbd9c0dd'
        },
        body: ""
      }, function (error, response, body) {
        console.log('Status:', response.statusCode);
        // console.log('Headers:', response.headers);
        // console.log('Response:', JSON.parse(body));
        var result = JSON.parse(body);
        //result.sort((a, b) => (parseFloat(a.fundingRate) > parseFloat(b.fundingRate)) ? 1 : -1)
        /* result.forEach(function (arrayItem) {
                if (arrayItem.fundingRate > maxrate || arrayItem.fundingRate < minrate) {
                    console.log(arrayItem);
                }
        }); */
       // const resultf = result.filter(item => item.fundingRate < minrate);
        //console.log(result)
        resolve(result)
      });
      });
}

async function addprices(symbols, description){
    return new Promise(resolve => {
        symbols.forEach(async function (arrayItem) {
            var priceObj = await get_price(arrayItem.symbol);
            let d = new Date();
            //arrayItem[description] = priceObj.lastPrice;
            const testPrice = {
              description: description,
              time: d,
              price: priceObj.lastPrice
            }; 
            if (description == 'entryprice'){
                arrayItem.entryTime = d;
                //arrayItem.prices = [testPrice]
            }
            if (description == 'exitprice'){
                arrayItem.exitTime = d;
                let profit = ((parseFloat(arrayItem.exitprice) / parseFloat(arrayItem.entryprice)) - 1)*100 - 0.21;
                arrayItem.profitPct = profit;
            }
            if (arrayItem.prices == undefined) {
              arrayItem.prices = []
            }
            arrayItem.prices.push(testPrice)
            console.log(arrayItem)
         });
    resolve(symbols);
    
    });
}

function getkeys(symbols){
    var newsymArr = [];
    symbols.forEach(function (arrayItem){
        var symname = arrayItem.symbol;
        newsymArr[symname] = arrayItem;
    })
    return newsymArr;
}


async function dbo_addPremiumIndex(premiumIndexArr) { //Add new fund info snapshot data to database
  console.log('adding snapshots');

    if (!(premiumIndexArr == undefined)){
/*       const keys = Object.keys(premiumIndexArr); //for associative array
        keys.forEach(function (key) {
        let premiumIndex = new PremiumIndex({ 
            symbol: premiumIndexArr[key].symbol,
            markPrice: premiumIndexArr[key].markPrice,
            indexPrice: premiumIndexArr[key].indexPrice,
            lastFundingRate: premiumIndexArr[key].lastFundingRate,
            nextFundingTime: premiumIndexArr[key].nextFundingTime,
            time: premiumIndexArr[key].time
        }) */
        premiumIndexArr.forEach(function(arrayItem){
          let premiumIndex = new PremiumIndex({ 
            symbol: arrayItem.symbol,
            markPrice: arrayItem.markPrice,
            indexPrice: arrayItem.indexPrice,
            lastFundingRate: arrayItem.lastFundingRate,
            nextFundingTime: arrayItem.nextFundingTime,
            time: arrayItem.time,
            updated: Date.now()
        })
        premiumIndex.save()
              .then(doc => {
                console.log('saving premiumIndex to DB: ', premiumIndex)
              })
              .catch(err => {
                console.error('save failed: ')
              })
        })
}
}

async function dbo_addTestOrders(testOrders) { //Add new trade data to database
  console.log('adding prices to db...');
  console.log(testOrders);
    if (!(testOrders == undefined)){
        testOrders.forEach(function (arrayItem) {
        let testOrder = new TestOrders({ 
            symbol: arrayItem.symbol,
            fundingRate: arrayItem.fundingRate,
            prices: arrayItem.prices
        })
        testOrder.save()
              .then(doc => {
                console.log('saving testOrder to DB: ', testOrder)
              })
              .catch(err => {
                console.error('save failed: ')
              })
        })
}
}


var minrate = -0.0020;
var maxrate = 0.0025;
//const interval = 900000; //15 minutes ms
const interval = 5000; //5 seconds/
const count = 3;

async function tradeall (){
    const timeoutPromise = (timeout) => new Promise((resolve) => setTimeout(resolve, timeout));
    var symbols = await gettickers();
    var rtsymbols = await updatesymbols(symbols,minrate,maxrate);
    //var symkeys = getkeys(symbols)
    //console.log('received symbols...',rtsymbols);
    await dbo_addPremiumIndex(rtsymbols)
    //var prices = await addprices(symbols,'entryprice');
 /*    var n = 0;
    while(n < count){
      console.log('Price Check: ',Date())
      symbols = await addprices(symbols,n);
      n++;
      await timeoutPromise(interval);
    }
    await dbo_addTestOrders(symbols);
    console.log('price check done.') */
    //var exitprices = await addprices(prices,'exitprice');

    //console.log(prices)
    

}


//tradeall().then(console.log('One execution completed.'))


//main().then(console.err)
 
var rule = new schedule.RecurrenceRule();
//rule.dayOfWeek = [0, new schedule.Range(4, 6)];
//rule.hour = [23,7,15];
rule.minute = [59,14,29,44];
 
var j = schedule.scheduleJob(rule, function(){
  console.log('Price Check Binance Futures Premiums ',Date());;
  tradeall();
});