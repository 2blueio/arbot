
const mavg = require('moving-averages') //ma, dma, ema, sma, wma

//console.log(mavg.ma([1, 2, 3, 4, 5], 2) )

const boll = require('bollinger-bands')

//console.log(boll([1, 2, 4, 8], 2, 2))
// {
//   upper: [, 2.5, 5, 10],
//   mid  : [, 1.5, 3, 6],
//   lower: [, 0.5, 1, 2]
// }
const sd = require('s-deviation');

//console.log(sd([1, 2, 4, 8], 2));         // [<1 empty item>, 0.5, 1, 2]

//console.log(sd([1, 2, 3, 4, 5, 6], 4));
// [
//   <3 empty items>,
//   1.118033988749895,
//   1.118033988749895,
//   1.118033988749895
// ]

//Functions to be moved into Module:
function getbartime(arr) {
    var time = [];
    arr.forEach(function (bar) {
        time.push(parseFloat(bar[0]))
    });
    return (time);
}

function gethighs(arr) {
    var highs = [];
    arr.forEach(function (bar) {
        highs.push(parseFloat(bar[2]))
    });
    return (highs);
}

function getlows(arr) {
    var lows = [];
    arr.forEach(function (bar) {
        lows.push(parseFloat(bar[3]))
    });
    return (lows);
}

function getcloses(arr) {
    var closes = [];
    arr.forEach(function (bar) {
        closes.push(parseFloat(bar[4]))
    });
    return (closes);
}


function gettrenddir(arr) {
    if (arr[arr.length - 1] > arr[arr.length - 2]) {
        return 1;
    } else {
        return -1
    }
}

async function timetillfunding() {
    var fundinginfo = {};
    fundingInfo[0].symbol = 'BTCUSDT';
    fundinginfo = await updatesymbol(fundinginfo);
    var delay = (fundingInfo[0].nextFundingTime - (exitminutes * 60000)) - Date.now();
    return (delay)
}

var roundOff = (num, places) => {
    const x = Math.pow(10, places);
    return Math.round(num * x) / x;
}

function calchistoricalfunding(bartime, nextFundingTime) {
    var eighthr = 28800000;
    var totalsessions = roundOff(nextFundingTime / eighthr, 0);
    for (x = 0; x < totalsessions; x++) {
        let targetsession = x * eighthr;
        let prevsession = (x + 1) * eighthr;
        if ((bartime >= (nextFundingTime - prevsession)) & (bartime < nextFundingTime - targetsession)) {
            console.log('found session:', x)
            return (x)
        }
    }
}

function filteredsymbols(lowrate, highrate, fundingArray, toptickers) {

    var result = [];
    var lowrecs = 0;
    var highrecs = 0;
    fundingArray.forEach(function (fundingRec) {
      if (parseFloat(fundingRec.lastFundingRate) <= lowrate) {
          if (toptickers.includes(fundingRec.symbol)) {
            fundingRec.side = 'BUY'; 
            fundingRec.signal = 'LE1';
            result.push(fundingRec);
            lowrecs++
          } else {
            console.log(fundingRec.symbol, ' not found in allowed tickers.')
          }
        }
        if (parseFloat(fundingRec.lastFundingRate) >= highrate) {
            if (toptickers.includes(fundingRec.symbol)) {
              fundingRec.side = 'SELL'; 
              fundingRec.signal = 'SE1';
              result.push(fundingRec);
              highrecs++
            } else {
              console.log(fundingRec.symbol, ' not found in allowed tickers.')
            }
          } 
      });
    console.log("Tickers with low funding:", lowrecs)
    console.log("Tickers with high funding:", highrecs)
    return (result)
  }

function fundtrader(candles, position, fundingInfo, logout) { //Calculate position for given Array Snapshot
    const toptickers = ['UNIUSDT', 'SUSHIUSDT', 'YFIIUSDT', 'BNBUSDT', 'CRVUSDT', 'DEFIUSDT', 'HNTUSDT', 'ICXUSDT', 'WAVESUSDT', 'TRBUSDT', 'FILUSDT', 'OCEANUSDT']; //For info purposes. These had the best results in Trading View
    var lowrate = -0.002;  //historical filters for funding rates. 
    var highrate = 0.002;
    const entryminutes = 435; //entry minutes before funding
    const exitminutes = 2; //exit before funding
    var strategyresult = {};
    var eighthr = 28800000;
    var bartimes = getbartime(candles);
    var bartime = bartimes[bartimes.length - 1];


    console.log('ttf:',(fundingInfo[0].nextFundingTime - bartime)/60000);
    if (bartime < (fundingInfo[0].nextFundingTime - eighthr)) { //if the bartime is longer than 8 hours ago then it's historical 
        var sessionsback = calchistoricalfunding(bartime, fundingInfo[0].nextFundingTime)
        //console.log('historical bar calc', sessionsback);
    } else { //It's a real time bar
        //console.log('realtime bar');
        if ((bartime >= fundingInfo[0].nextFundingTime - (entryminutes * 60000)) & position == 0 & (!(fundingInfo[0].nextFundingTime == fundingInfo[0].lastTradeSession))) { //Check for Funding rates
            var orders = filteredsymbols(lowrate, highrate, fundingInfo, toptickers);
            console.log(orders);
            //strategyresult.signal = 'LE1';
            //strategyresult.lastTradeSession = fundingInfo[0].nextFundingTime;
        }
        if ((bartime >= fundingInfo[0].nextFundingTime - (exitminutes * 60000)) & (!(position == 0))) { //in exit range and position exists
            strategyresult.signal = 'LEX';
        }
        console.log('Trade', strategyresult.signal, position)
    }
    //strategyresult.signal = 0
    return (strategyresult);
}

function tactical_es(candles, position, entryprice1, logout) { //Calculate position for given Array Snapshot

    var length = 10;
    var length2 = 30;
    var mult = 1.5;
    var mult2 = 2.5;
    var trend = 100;
    var strategyresult = {};


    const BuyLevel = 60;
    const SellLevel = 100;
    var signal = '***';
    var closes = getcloses(candles);
    var close = closes[closes.length - 1];
    var bb1 = boll(closes, length, mult); //boll(datum, period, sd size, options)
    var bb2 = boll(closes, length2, mult2);
    var lower = bb1.lower[bb1.lower.length - 1];
    var lower2 = bb2.lower[bb2.lower.length - 1];
    var upper = bb1.upper[bb1.upper.length - 1];
    var upper2 = bb2.upper[bb2.upper.length - 1];
    var ma = mavg.ma(closes, trend); //Simple Moving Average: ma(data, size)
    var StdDevIndex = (close - lower) / (upper - lower) * 100;
    var StdDevIndex2 = (close - lower2) / (upper2 - lower2) * 100;
    var oktobuy = false;
    var oktosell = false;
    var oktobuy2 = false;
    var oktosell2 = false;
    var trenddir = gettrenddir(ma);
    //var position = candles[candles.length -1].position;
    //console.log(position);

    if (StdDevIndex > BuyLevel) { oktobuy = false } else { oktobuy = true }
    if (StdDevIndex < SellLevel) { oktosell = false } else { oktosell = true }
    if (StdDevIndex2 > BuyLevel) { oktobuy2 = false } else { oktobuy2 = true }
    if (StdDevIndex2 < SellLevel) { oktosell2 = false } else { oktosell2 = true }

    //console.log('Trend Direction:',trenddir,oktobuy,oktosell,oktobuy2,oktosell2,close,lower,lower2);

    // & (position < unitsize)) { // and strategy.position_size < 1)
    if ((oktobuy == true) & (trenddir == 1) & (close < lower) & (position < 1)) { signal = 'LE1' }
    if ((oktosell == true) & (trenddir == -1) & (close > upper) & (position > -1)) { signal = 'SE1' }
    if ((oktobuy2 == true) & (trenddir == 1) & (close < lower2) & (position == 1) & (close < entryprice1)) { signal = 'LE2' }
    if ((oktosell2 == true) & (trenddir == -1) & (close > upper2) & (position == -1) & (close > entryprice1)) { signal = 'SE2' }


    if ((close > upper) & (position > 0)) { signal = 'LEX' }
    if ((close < lower) & (position < 0)) { signal = 'SEX' }

    strategyresult.signal = signal;
    //console.log(strategyresult);
    return (strategyresult) //will need to change this to object to include price and time data, preferbly use the binance object
}

function trendweaver(candles, position, trenddirection, logout) {
    //inputs
    var strategyresult = {};
    var TrendLength = 150;
    var ShortPercent = 10;
    var LongPercent = 90;
    var ExitLong_Percent = 40;
    var ExitShort_Percent = 60;
    var signal = '***';

    //vars
    var TradeRange = 0.0;
    var Long_Entry_Price = 0.0;
    var Short_Entry_Price = 0.0;
    var LongExit = 0.0;
    var ShortExit = 0.0;
    var Top_Range = 0.0;
    var Bottom_Range = 0.0;

    var highs = gethighs(candles);
    var lows = getlows(candles);
    var high = highs[highs.length - 1];
    var low = lows[lows.length - 1];

    var maxhighrange = Math.max(...highs.splice(-1 * TrendLength));
    var minlowrange = Math.min(...lows.splice(-1 * TrendLength));

    var TradeRange = (maxhighrange - minlowrange) / 100;
    var Long_Entry_Price = minlowrange + (TradeRange * LongPercent);
    var Short_Entry_Price = minlowrange + (TradeRange * ShortPercent);
    var Top_Range = minlowrange + ((maxhighrange - minlowrange) * .99);
    var Bottom_Range = minlowrange + ((maxhighrange - minlowrange) * .01);
    var ShortExit = minlowrange + (TradeRange * ExitShort_Percent);
    var LongExit = minlowrange + (TradeRange * ExitLong_Percent);


    if (high > Top_Range) { trenddirection = 1 }
    if (low < Bottom_Range) { trenddirection = -1 }

    if (((trenddirection == 1 & high > Long_Entry_Price) || (trenddirection == -1 & high > Top_Range)) & (position < 1)) { signal = 'LE1'; position = 1 };
    if (((trenddirection == -1 & low < Short_Entry_Price) || (trenddirection == 1 & low < Bottom_Range)) & (position > -1)) { signal = 'SE1'; position = -1 };

    if ((position > 0) & (low < LongExit)) { signal = 'LEX' };
    if ((position < 0) & (high > ShortExit)) { signal = 'SEX' };

    if (logout == 1) { console.log('Trend:', trenddirection) };

    strategyresult.signal = signal;
    strategyresult.trenddirection = trenddirection;

    return (strategyresult)

}



exports.tactical_es = tactical_es;
exports.trendweaver = trendweaver;
exports.fundtrader = fundtrader;