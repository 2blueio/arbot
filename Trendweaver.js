//@version=4
//strategy("Trend Weaver", overlay=true)

const api_credentials = require('./api_credentials.js');
const request = require('request');
const loopback = require('./loopback.js');
const BINANCE = require('./binancetradeservices.js')
const mavg = require('moving-averages') //ma, dma, ema, sma, wma //console.log(mavg.ma([1, 2, 3, 4, 5], 2) )

//inputs
var TrendLength = 150;
var TradeLength = 20;
var ShortPercent = 10;
var LongPercent = 90;
var ExitLong_Percent = 40;
var ExitShort_Percent = 60;
var CounterTrend = 0;
var ContractsSet = 1;

//vars
var TradeRange = 0.0;
var Long_Entry_Price = 0.0;
var Short_Entry_Price = 0.0;
var LongExit = 0.0;
var ShortExit = 0.0;
var TrendDirection = 0.0;
var Top_Range = 0.0;
var Bottom_Range = 0.0;
var ContractsToTrade = (1);
var ContractsTraded = (0);
var LongTrigger = false;
var ShortTrigger = false;

//functions

function calchistorical(candles) {

    var roundOff = (num, places) => {
      const x = Math.pow(10, places);
      return Math.round(num * x) / x;
    }
  
    var position = 0;
    var entryprice1 = 0;
    var entryprice2 = 0;
    var avgprice = 0;
    var price = 0;
    var netprofit = 0;
  
    for (i = trend - 1; i < candles.length; i++) {
  
      var candleslice = candles.slice(0, i);
      var signal = siggen(candleslice, position, entryprice1);
      price = parseFloat(candles[i][4]);
      if (signal == 'LE1') { position = 1; entryprice1 = parseFloat(candles[i][4]) }
      if (signal == 'SE1') { position = -1; entryprice1 = parseFloat(candles[i][4]) }
      if (signal == 'LE2') { position = 2; entryprice2 = parseFloat(candles[i][4]) }
      if (signal == 'SE2') { position = -2; entryprice2 = parseFloat(candles[i][4]) }
      if ((signal == 'LEX') || (signal == 'SEX')) {
        avgprice = 0; position = 0; entryprice1 = 0; entryprice2 = 0;
      }
  
      candles[i].bartime = candles[i][0];
      candles[i].price = price;
      candles[i].signal = signal;
      candles[i].position = position;
      candles[i].entryprice1 = entryprice1;
      candles[i].entryprice2 = entryprice2;
      if (!(entryprice2 == 0)) { avgprice = (entryprice1 + entryprice2) * 0.5 } else { avgprice = entryprice1 }; // avg price = unitval in loopback
      candles[i].avgprice = avgprice;
      if (avgprice > 0) { netprofit = price - avgprice } else { netprofit = 0 }
      if (log == true) {console.log(candles[i][0], i, roundOff(price, 4), signal, position, entryprice1, entryprice2, avgprice, roundOff(netprofit, 4));};
    }
    //console.log('Calculation bars: ', candles.length)
    //printtrades(candles)
    return (candles);
  }

async function main(symbol, timeframe, psize) {
    const timeoutPromise = (timeout) => new Promise((resolve) => setTimeout(resolve, timeout));
    var exchsymbols = await BINANCE.exchinfo(); //gets details about symbols such as quantity Precision and trade types available (needed for trading)
    var quantPrecision = exchsymbols[symbol].quantityPrecision;
    var strategy = loopback.createstrategy('BINANCE', 'FUT', 'TREND_WEAVER', symbol, quantPrecision, arg_interval);

    while (1) {
        var candles = await BINANCE.getcandles(symbol, timeframe);
        var interval = candles[candles.length - 2][0] - candles[candles.length - 3][0];
        var candles = calchistorical(candles);  //Sending all candles to signal generator and receiving enhanced candles with signals attached
        var price = candles[candles.length - 1].price;
        var currentpos = candles[candles.length - 1].position; //Strategy position
        var signal = candles[candles.length - 1].signal; 
        var lastclosetime = candles[candles.length - 1][0];
        var lastclosedate = new Date(lastclosetime);
        var nextclosetime = lastclosetime + interval;
        var nextclosedate = new Date(nextclosetime);
        var currenttime = new Date();
        var timetillclose = (nextclosetime - currenttime + 1000); //delay by 1s to make sure we get latest bar close

        if (!(signal == '***')) {
            if ((signal == 'LE1') || (signal == 'LE2') || (signal == 'SE1') || (signal == 'SE2')) {
                var units = Number(((psize * currentpos / parseFloat(price))).toFixed(quantPrecision));
                var orders = [{ strategy: strategy, signal: signal, units: parseFloat(units), avgPrice: parseFloat(price), timestamp: lastclosedate }];
                console.log('RAW ORDERs object being fed into sendallstrategyorders: ', orders)
                console.log('Signal:', signal)
                var results = await loopback.sendallstrategyorders(orders);
            }
            if ((signal == 'SEX') || (signal == 'LEX')) {
                var closedpositions = await loopback.closeallstrategyorders([{ strategy: strategy, signal: signal, avgPrice: parseFloat(price), timestamp: lastclosedate }]); //Doesn't have units but can get it from opening order.
                console.log('Close Status:', closedpositions);
            }
            var status = await loopback.synclivepositions([strategy]);
            console.log('Sync status:', status)
        }
        console.log('Current Strategy Position: ' + currentpos + ' unit(s) | ' + symbol);
        await timeoutPromise(timetillclose);
    }
}


const arg_symbol = process.argv[2];
const arg_interval = process.argv[3];
const arg_psize = process.argv[4];
const arg_log = process.argv[5];

if (arg_log == true) { log = true } else { log = false }

console.log(arg_symbol, arg_interval, arg_psize, log);

main(arg_symbol, arg_interval, psize);




/*
TradeRange := (highest(high,TradeLength) - lowest(low,TradeLength))/100
Long_Entry_Price := lowest(low,TradeLength)+TradeRange*LongPercent
Short_Entry_Price := lowest(low,TradeLength)+TradeRange*ShortPercent
Top_Range := lowest(low,TrendLength)+(highest(high,TrendLength)-lowest(low,TrendLength))*.99
Bottom_Range := lowest(low,TrendLength)+(highest(high,TrendLength)-lowest(low,TrendLength))*.01
ShortExit := lowest(low,TradeLength)+TradeRange*ExitShort_Percent
LongExit := lowest(low,TradeLength)+TradeRange*ExitLong_Percent


TrendDirection := high > Top_Range ? 1 : low < Bottom_Range ? -1 : TrendDirection[1]
//plot(TrendDirection * 100)

longCondition = ((TrendDirection == 1 and high > Long_Entry_Price) or (TrendDirection == -1 and high > Top_Range))  ? true : false
shortCondition = ((TrendDirection == -1 and low < Short_Entry_Price) or (TrendDirection == 1 and low < Bottom_Range)) ? true : false
longExit = low < LongExit ? true : false
shortExit = high > ShortExit ? true : false

//and strategy.position_size == 0
// x = high > Long_Entry_Price ? 3000 : shortCondition ? 2500 : 2400
// plot(x)

strategy.entry("buy", strategy.long, 1, when=longCondition)
strategy.entry("sell", strategy.short, 1, when=shortCondition)
// if (longCondition)
//     strategy.entry("LongBreakout", strategy.long)

// if (shortCondition)
//     strategy.entry("ShortBreakout", strategy.short)


strategy.close("exit", when = longExit or shortExit)

 */