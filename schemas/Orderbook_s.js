// Mongoose schema and model definitions
const mongoose = require('mongoose');
const Schema = mongoose.Schema;
// Create the schema for the Orderbook database
const orderSchema = new Schema ({
    ordertype: { type: String, required: false, unique: false },
    price: { type: Number, required: false, unique: false },  
    quantity: { type: Number, required: false, unique: false },
    total: { type: Number, required: false, unique: false}
})

const orderbookSchema = new Schema({
    exch_code: { type: String, required: true, unique: false, index: true },
    market: { type: String, required: true, unique: false, index: true },
    timestamp: {type: Date, default: Date.now},
    exch_fee: {type: Number, default: 0},
    orders: [orderSchema]
    }
    , {
    toObject: {
    virtuals: true
    },
    toJSON: {
    virtuals: true 
    }
});


orderbookSchema
.virtual('bid')
.get(function () {
    var bestbid = 0;
    this.orders.forEach(function (arrayItem) {
        if (arrayItem.ordertype == 'Buy'){
            //var order = {ordertype: arrayItem.ordertype, price: arrayItem.price, quantity: arrayItem.quantity, total: arrayItem.total}
            if (arrayItem.price > bestbid) {bestbid = arrayItem.price}
        }
    })  
  return bestbid;
});

orderbookSchema
.virtual('ask')
.get(function () {
    var bestask = Infinity;
    this.orders.forEach(function (arrayItem) {
        if (arrayItem.ordertype == 'Sell'){
            //var order = {ordertype: arrayItem.ordertype, price: arrayItem.price, quantity: arrayItem.quantity, total: arrayItem.total}
            if (arrayItem.price < bestask) {bestask = arrayItem.price}
        }
    })  
  return bestask;
});

orderbookSchema
.virtual('exmarket')
.get(function () {
    var exmarket = this.exch_code + '_' + this.market
    return exmarket
});

//these need to be validated for USD and BTC base pairs which are flipped on some exchanges.
orderbookSchema
.virtual('v1')
.get(function () {
    var currs = this.market.split("/", 2);
    var from_curr = this.exch_code + '_' + currs[0];
    return from_curr
});

orderbookSchema
.virtual('v2')
.get(function () {
    var currs = this.market.split("/", 2);
    var to_curr = this.exch_code + '_' + currs[1];
    return to_curr
});

module.exports = mongoose.model('Orderbook', orderbookSchema)
//module.exports = mongoose.model('Order', orderSchema)


// abridged order:
// { price: 0.023485,
//     quantity: 22495.37,
//     ordertype: 'Sell',
//     total: '528.30376445' }
//
// full order:
// { exchange: 'LIQU',
//     market: 'ZRX/BTC',
//     timestamp: '2018-10-03 19:04:31',
//     ordertype: 'Sell',
//     price: 0.00010054,
//     quantity: 31.33027247,
//     total: 0.0031499455941338 }

