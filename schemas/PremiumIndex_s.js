// Mongoose schema and model definitions
const mongoose = require('mongoose');
const Schema = mongoose.Schema;

// Create the schema for the funding document collection
const premiumIndex = new Schema({
    symbol: { type: String}, 
    markPrice: {type: Number},
    indexPrice: {type: Number},
    lastFundingRate:{type: Number},
    interestRate: {type:Number},
    nextFundingTime: {type: Number},
    time: {type: Number},
    updated: { type: Date }
    },{
    toObject: {virtuals: true},
    toJSON: {virtuals: true }
});

premiumIndex
.virtual('minutesleft')
.get(function () {
    var remaining = (this.nextFundingTime - this.time)/1000/60;
    return remaining.toFixed(2)
});

module.exports = mongoose.model('PremiumIndex', premiumIndex);

