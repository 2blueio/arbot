// Mongoose schema and model definitions
const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const testPrices = new Schema({
    description: {type: String},
    time: {type: Date},
    price: {type: Number}
});

// Create the schema for the markets document collection
const testOrders = new Schema({
    symbol: { type: String}, 
    fundingRate: { type: Number}, 
    prices: [testPrices]
});

module.exports = mongoose.model('TestOrders', testOrders);

