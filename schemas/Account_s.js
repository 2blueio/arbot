// Mongoose schema and model definitions
const mongoose = require('mongoose');
const Schema = mongoose.Schema;
// Create the schema for the Orderbook database
const accountSchema = new Schema ({
        auth_id: {type: Number, default: 0},
        auth_key: {type: String},
        auth_optional1: {type: String},
        auth_nickname: {type: String},
        exch_name: {type: String},
        auth_secret: {type: String},
        auth_updated: {type: Date, default: Date.now},
        auth_active: {type: Number, default: 0},
        auth_trade: {type: Number, default: 0},
        exch_trade_enabled: {type: Number, default: 0},
        exch_id: {type: Number, unique: false, index: false}
});




module.exports = mongoose.model('account', accountSchema)
// { auth_id: '363693',
//         auth_key: '567e8c3d852749dbb4d3a5b517b8303c',
//         auth_optional1: '',
//         auth_nickname: 'Bittrex',
//         exch_name: 'Bittrex',
//         auth_secret: '************************',
//         auth_updated: '2019-05-30 23:52:38',
//         auth_active: '1',
//         auth_trade: '1',
//         exch_trade_enabled: '1',
//         exch_id: '15' }