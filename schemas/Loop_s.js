// Mongoose schema and model definitions
const mongoose = require('mongoose');
const Schema = mongoose.Schema;

//create subdocument schema for currency pairs
const order = new Schema({
    timestamp: {type: Date, default: Date.now},
    exch_name: String,
    market: String,
    direction: String,
    auth_id: String,
    exch_id: Number,
    mkt_id: Number,
    order_type_id: Number,
    limit_price: Number,
    order_quantity: Number,
    expected_result: Number,
    actual_result: Number,
    status: String,
    //Additional fields for Transfers between exchanges:
    to_exch: String,
    to_exch_id: Number,
    currency: String,
    wfee: Number,
});


// Create the schema for the exchange document collection
const loopSchema = new Schema({
    timestamp: {type: Date, default: Date.now},
    starting_asset: String,
    oldestbook: {type: Date, default: '1900/1/1'}, //oldest book in the loop path
    age_detected: Number, //oldest age of books at time of detection
    quantity_result: Number,
    status: String,
    expected_result: Number,
    actual_result: Number,
    path: [],
    orders: [order]
});

module.exports = mongoose.model('Loop', loopSchema);

