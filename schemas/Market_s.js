// Mongoose schema and model definitions
const mongoose = require('mongoose');
const Schema = mongoose.Schema;


// Create the schema for the markets document collection
const marketSchema = new Schema({
    exch_id: { type: Number}, 
    exch_name: { type: String}, 
    exch_code: { type: String,},
    mkt_id: { type: Number},
    mkt_name: { type: String},
    exchmkt_id: { type: Number}
});

module.exports = mongoose.model('Market', marketSchema);

