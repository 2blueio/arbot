// Mongoose schema and model definitions
const mongoose = require('mongoose');
const Schema = mongoose.Schema;

// const orderSchema = new Schema ({
//     ordertype: { type: String, required: false, unique: false },
//     price: { type: Number, required: false, unique: false },  
//     quantity: { type: Number, required: false, unique: false },
//     total: { type: Number, required: false, unique: false}
// })

// const orderbookSchema = new Schema({
//     exch_code: { type: String, required: true, unique: false },
//     market: { type: String, required: true, unique: false },
//     timestamp: {type: Date, default: Date.now},
//     //timestamp: Date,
//     orders: [orderSchema]
// });

//create subdocument schema for currency pairs
const pairSchema = new Schema({
    name: String,
    from: String,
    to: String,
    enabled: Boolean,
    channel: String,
});

const channelSchema = new Schema({
    market: String,
    code: String
});

const wfeeSchema = new Schema({ name: Number})

// Create the schema for the exchange document collection
const exchangeSchema = new Schema({
    exch_id: { type: Number, required: true, unique: true },  
    exch_code: { type: String, required: true, unique: true, index: true },
    exch_name: { type: String, required: false, unique: false },
    exch_fee: { type: Number, required: true, unique: false, default: .002 },
    exch_balance_enabled: { type: Number, required: false, unique: false, default: 0},
    exch_trade_enabled: { type: Number, required: false, unique: false, default: 0},
    exch_url: { type: String, required: false, unique: false },
    exch_balance: { type: Number, default: 0 },
    enabled: Boolean,
    pairs: [pairSchema], //currently used pairs aggregated from live orderbooks
    wfees: [wfeeSchema],
    channels: []
});

module.exports = mongoose.model('Exchange', exchangeSchema)

