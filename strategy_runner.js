/* Strategy Runner
Javscript Version 1.0

Binance Data Feed and Execution
Pulls Data on time interval

Futures versions should use real-time websocket Data

To Do: Buy down only on lower price, trade dump, commandline input, Join only if underwater, Fixed Fractional position size, Track by Order ID to work with other strategies
maybe funding check and multi symbol support

 */

const LOOPBACK = require('./LOOPBACK.js');
const BINANCE = require('./binancetradeservices.js')
const STRATEGIES = require('./strategies.js');
const inquirer = require('inquirer');
var lastTradeSession = 0; //need to declare globally so we can keep track of trades per session

const maxbarsback = 150;
var log = false;



function printtrades(candles) {
  candles.forEach(function (candle) {
    var date = new Date(candle[0]);
    //alert(date.toString());
    console.log(date.toString(), parseFloat(candle[4]).toFixed(2), candle.signal, candle.position)
  })
}

function calchistorical(strategyname, candles, fundingInfo, logout) {

  var roundOff = (num, places) => {
    const x = Math.pow(10, places);
    return Math.round(num * x) / x;
  }

  var position = 0;
  var entryprice1 = 0;
  var entryprice2 = 0;
  var avgprice = 0;
  var price = 0;
  var netprofit = 0;
  var signal = 0;
  var strategyresult = {};
  var trenddirection = 0;

  for (i = maxbarsback; i < candles.length; i++) { //The reason for this loop is to calculate historical signals all the way up to the present. Nice for analyzing strategy results.

    var candleslice = candles.slice(0, i);

    if (strategyname == 'TACTICALES') {
      strategyresult = STRATEGIES.tactical_es(candleslice, position, entryprice1, logout);
      signal = strategyresult.signal;
    }
    //console.log(strategyresult);

    if (strategyname == 'TRENDWEAVER') {
      strategyresult = STRATEGIES.trendweaver(candleslice, position, trenddirection, logout);
      signal = strategyresult.signal;
    }

    if (strategyname == 'FUNDTRADER') { //On hold. This sends back multiple symbols which isn't supported by strategy runner
      fundingInfo.lastTradeSession = lastTradeSession;
      strategyresult = STRATEGIES.fundtrader(candleslice, position, fundingInfo, logout);
      signal = strategyresult.signal;
      lastTradeSession = strategyresult.lastTradeSession;
    }

    //signal = strategyresult.signal;
    trenddirection = strategyresult.trenddirection;
    //console.log(trenddirection)
    price = parseFloat(candles[i][4]);
    //console.log(signal)
    if (signal == 'LE1') { position = 1; entryprice1 = parseFloat(candles[i][4]) }
    if (signal == 'SE1') { position = -1; entryprice1 = parseFloat(candles[i][4]) }
    if (signal == 'LE2') { position = 2; entryprice2 = parseFloat(candles[i][4]) }
    if (signal == 'SE2') { position = -2; entryprice2 = parseFloat(candles[i][4]) }
    if ((signal == 'LEX') || (signal == 'SEX')) {
      avgprice = 0; position = 0; entryprice1 = 0; entryprice2 = 0;
    }

    candles[i].bartime = candles[i][0];
    candles[i].price = price;
    candles[i].signal = signal;
    candles[i].position = position;
    candles[i].entryprice1 = entryprice1;
    candles[i].entryprice2 = entryprice2;
    candles[i].trenddirection = trenddirection;
    if (!(entryprice2 == 0)) { avgprice = (entryprice1 + entryprice2) * 0.5 } else { avgprice = entryprice1 }; // avg price = unitval in LOOPBACK
    candles[i].avgprice = avgprice;
    if (avgprice > 0) { netprofit = price - avgprice } else { netprofit = 0 }
    if (Math.abs(logout) == 1) { console.log(candles[i][0], i, roundOff(price, 8), signal, position, entryprice1, entryprice2, avgprice, roundOff(netprofit, 8)); }
  }
  //console.log('Calculation bars: ', candles.length)
  //printtrades(candles)
  return (candles);
}



async function main(stratobj) {

  var strategy = await LOOPBACK.initstrategy(stratobj);
  var logout = strategy.log;
  var matchposalways = false;
  const timeoutPromise = (timeout) => new Promise((resolve) => setTimeout(resolve, timeout));
  var tradeid = 0;
  var orderid = 0;
  var tradetype;
  var currentunits = 0;
  var timetillclose = 60000; //initial minute in case there is a network error it will retry every minute
  const signals = ['LE1', 'LE2', 'LEX', 'SE1', 'SE2', 'SEX', 'XXX'];

  if (Math.abs(logout) == 1) { console.log('Strat Info:', strategy) };

  while (1) { //Main Loop for pulling candles every interval and executing new strategy signals
    try {
      var candles = await BINANCE.getcandles(strategy); //get latest candles
      if (Math.abs(logout) == 1) { console.log('Candles Info:', candles) };
      var interval = candles[candles.length - 2][0] - candles[candles.length - 3][0]; //calculate which interval we're dealing with
      var fundingInfo = await BINANCE.symlookup(''); //Adds fundingInfo which is only used for Funding Trader Strategy
      var candles = calchistorical(strategy.strategy, candles, fundingInfo, logout); //Signals, positions and entry prices are generated and added to Candles array as properties
      var price = candles[candles.length - 1].price; //latest closing price
      var currentpos = candles[candles.length - 1].position; //Current strategy position: 1 = Long (LE1), 2 Long (LE2), -1 SE1, -2 SE2 etc. Position means Direction
      var lastpos = candles[candles.length - 2].position; //Last position
      var signal = candles[candles.length - 1].signal; //LE1, LE2, SE1, SE2, SEX, LEX
      var lastclosetime = candles[candles.length - 1][0]; //timestamp of last close
      var lastclosedate = new Date(lastclosetime);
      var nextclosetime = lastclosetime + interval;
      var nextclosedate = new Date(nextclosetime);
      var currenttime = new Date();
      //var clientOrderId = strategy.id;//currenttime.getTime();
      timetillclose = (nextclosetime - currenttime + 1000);
      var currentunits = 0;
      var side;
      var quantity = 0;
      var sync = false;

      quantity = Number(((strategy.psize / parseFloat(price))).toFixed(strategy.quantityPrecision));
      if (logout == 'q') { console.log(strategy.psize, price, strategy.quantityPrecision, quantity) }

      //pecial logout var codes for generating signals manually from commandline arg
      if (logout == 2) { signal = 'LE1' }; if (logout == -2) { signal = 'LEX' }; if (logout == -20) { signal = 'SE1' }; if (logout == 20) { signal = 'SEX' }; if (logout == 99) { signal = 'XXX' }

      //Checks to see if strategy matches with Database or there are open posiitons on first run.
      if (signal == '***') {
        strategy = await LOOPBACK.getstrategy(strategy.id); //refresh data from server
        console.log('Positions (strategy/server):', currentpos, strategy.currentpos);

        if (!(strategy.currentpos == currentpos)) {
          if (matchposalways == false) { //has matchpositions alreaddy been set to true?
            console.log('Positions do not match (strategy/server):', currentpos, strategy.currentpos);
            var syncinquire = await inquirer
              .prompt([
                {
                  name: 'matchpos',
                  message: 'Sync strategy position with local db and live broker? (y/n/a)',
                },
              ]);
              if (syncinquire.matchpos == 'a') { matchposalways = true }
          }
          if ((syncinquire.matchpos == 'y') || (matchposalways == true)) {
            if (currentpos == 0) { signal = 'XXX'; sync = true }
            if (currentpos == 1) { signal = 'LE1'; sync = true }
            if (currentpos == 2) { signal = 'LE2'; sync = true }
            if (currentpos == -1) { signal = 'SE1'; sync = true }
            if (currentpos == -2) { signal = 'SE2'; sync = true }
            console.log('sending sync signal:', signal);
          }
        }
      }

      if ((!(signal == '***')) & ((signal == 'LE1') || (signal == 'SE1') || (signal == 'LE2') || (signal == 'SE2') || (signal == 'LEX') || (signal == 'SEX') || (signal == 'XXX'))) { //Validate signal

        //Set up side paramater
        if ((signal == 'LE1') || (signal == 'LE2') || (signal == 'SEX')) { side = 'BUY' };
        if ((signal == 'SE1') || (signal == 'SE2') || (signal == 'LEX')) { side = 'SELL' };

        //Calculate Quantity
        quantity = Number(((strategy.psize / parseFloat(price))).toFixed(strategy.quantityPrecision));
        if (logout == 'q') { console.log(strategy.psize, price, strategy.quantityPrecision, quantity) }
        if ((signal == 'SE1') || (signal == 'SE2')) { quantity = quantity * -1 }

        //Set up trade type paramater
        if ((lastpos == 0) & ((signal == 'LE1') || (signal == 'SE1'))) { tradeid = lastclosetime; tradetype = 'NEW' }; //generates a new trade id
        if (((lastpos < 0) & (signal == 'LE1')) || ((lastpos > 0) & (signal == 'SE1'))) { tradetype = 'REVERSE' }; //generates a reversal signal so that broker will wipe out current position quantity
        if ((signal == 'LE2') || (signal == 'SE2')) { tradetype = 'ADD' }; //assume this is an ADD unless we get override below
        if (((lastpos < 0) & (signal == 'SE1')) || ((lastpos > 0) & (signal == 'LE1'))) { tradetype = 'DUPE' }; //override type type if there are duplicate signals. should be ignored or joined in loopback routine
        if (((lastpos < -1) & (signal == 'SE2')) || ((lastpos > 1) & (signal == 'LE2'))) { tradetype = 'DUPE' };
        if ((signal == 'SEX') || (signal == 'LEX') || (signal == 'XXX')) { tradetype = 'CLOSEALL'; quantity = 0 }; //overrider to set to 0 position for all signals or sync signals

        if (sync == true) { tradetype = 'SYNC' }; //sync any open positions

        var clientOrderId = strategy.id; //clientOrderId will be used on Binance but we will also receive an orderId after trade is received by Binance
        var order = { strategy: strategy, signal: signal, quantity: parseFloat(quantity), currentpos: currentpos, tradeid: tradeid, clientOrderId: clientOrderId, barclose: parseFloat(price), bartime: lastclosetime, timestamp: lastclosedate, tradetype: tradetype, ordertype: 'MARKET' }; //, clientOrderId: clientOrderId,  tradeid: tradeid }; //using the numeric current time as an order ID
        var traderesult = await LOOPBACK.execorder(order);
        console.log(traderesult);
      }

      strategy = await LOOPBACK.getstrategy(strategy.id); //refresh data from server
      if (!(strategy.units == 0)) {
        var openpl = (price - strategy.avgPrice) * strategy.units;
      } else {
        var openpl = 0;
      }


      //console.log(strategy.id, 'Dir:', currentpos, signal, price.toFixed(strategy.details[0].pricePrecision), strategy.units, openpl.toFixed(strategy.details[0].quotePrecision)); //, 'P/L:', openPl,'Cum:',strategyLB.cumPl.toFixed(2));
      console.log(strategy.id, 'Dir:', currentpos, signal, price.toFixed(6), strategy.units, openpl.toFixed(6));
    } catch (error) {
      console.error('Network Error:', error);
      timetillclose = 60000; //resetting loop interval to 1 minute until connection restored
    }
    //console.log('Last Bar:', lastclosedate.toString(), '\nNext Close: ', nextclosedate.toString(), 'Time till close: ', timetillclose / 60000, exec);
    await timeoutPromise(timetillclose);
  }
}


//Start program with example commandline below:

//node strategy_runner BINANCE_FUT_TRENDWEAVER_ETHUSDT_4h_10_0
//node strategy_runner BINANCE_FUT_TACTICALES_ETHUSDT_4h_10_0
//node strategy_runner BINANCE_SPOT_TRENDWEAVER_ETHBTC_4h_.01_0
//node strategy_runner BINANCE_SPOT_TRENDWEAVER_ETHBTC_1m_.01_1

// 10 = position size($ for futures or baseAsset for spot pairs), 0 = loglevel

var stratobj = LOOPBACK.createstrategyobj(process.argv[2]);

main(stratobj);

