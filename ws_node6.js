//Arbitrage "Gbot" by Gregory Lindberg, copyright 2019
//Part 1 (Arb Matrix):
//Create ArbMatrix outputs for Price and Bid/Ask
//Add Exchange Info
//Add overrides for fees
//Create Node Server and web output of Arb Matrix
//
//Part 2 (Execution of Trades):
//Watch Personal Coinigy Channel to watch balances and Trades
//Create Trade execution Class methods


//8/31/2018: 
//--integrate dynamic exchange fees into arbgraph, make sure minimum fee is applied * done *
//--Check currencies to make sure they're trading on at least 2 exchanges (maybe not needed)
//--make switch for logging level
//--add autolink between USD and USDT * not needed *
//--start working on USD/USDT entry and exit points using BFS [high priority]
//--create more test exchange and asset groups including all
//--add trade size to orderbook depth function. This could be done by filtering the unadjusted results, or modifying the
//edgeweights dyanamically (much more complicated since some nodes may not have paths to USD or BTC initially)
//--Add path printout using descriptions and trade size examples with % gains per loop (can be used for filtering)
//--garbage collector to remove stale orderbooks
//--build historical bars for asset prices and spreads

//9-17-2018
//Thoughts on overall archicture:
//Currently the primary graph is created using a default weight of the bid/ask spread and trade fees. 
//This will give you very optimistic arbitrage but shouldn't be used until order book depth is added to the weights
//The reason you can't use the depth book immediately is becase the Orderbooks come in random order
//So some nodes may be separate from any USD or BTC pair, which leads to a an infinite regression or arbitrary
//assignment of value. E.g. the actual USD worth of coin could be different depending on the path it took to get there
//therefore the varying amount can't be used to get the actual orderbook depth on the coin.
//
//Orderbooks are stored in a sub-graph created by this.storage graph. Current idea is to use GetArbweightedLoops
//to update the entire main graph with order book depth information. The alternative is to generate
//another subgraph using depth info and keep the main graph using only bid/ask.

//Basic output will be weighted arb opportunities using:
//1) bid/ask (base graph, keep base update function so we can revert to base)
//2) bid/ask/fees (base graph with updatefees function)
//3) bid/ask/fees/depth starting from specific exchange 
//4) same as 3 with starting USD or BTC tradesize value
// each output will require rebuilding the entire graph using update or BFupdate using stored orderbooks
//Possibility is to use the same updateOrderbook with different paramaters (base,fees,depth,tradesize) options. Only use depth/tradesize
//option if you're doing BFS since this will require specific trade size

// 9/21: www.arbalerts.net
// Set up graph design so that users can create custom exchanges and currencies and get alerts based on their 
// exchanges, currencies, and wehther they want internal or inter-exchange opps, and % levels
// All graphs should be dynamically generated as much as possible. Store basic graph with orderbooks and 
// And create query system which can dymamically create custom graphs 
const api_credentials = require('./api_credentials.js');
// const Arbgraph = require('./graph/arbgraph.js');
// const Exchanges = require('./graph/exchanges.js'); //Exchange class
// const Orderbooks = require('./graph/Orderbooks.js');
var socketCluster = require('socketcluster-client');
const Dbaccess = require('./db/dbaccess.js')
// const channelcache = 'channelcache.txt';
// const rp = require('request-promise');
const mongoose = require('mongoose');
// db = require('./database')
// let ExchangeModel = require('./Exchange_s')


const rest = {
    orders: 'https://api.coinigy.com/api/v1/orders', 
    markets: 'https://api.coinigy.com/api/v1/markets', 
    exchanges: 'https://api.coinigy.com/api/v1/exchanges',
    accounts: 'https://api.coinigy.com/api/v1/accounts'
};

const allowed_assets = 
[
	'USD','USDT','BTC','ETH','LTC','BCH','XRP','ADA','TRX','ZEN','ZEC','NEO',
	'XMR','STRAT','OMG','SC','ZCL','ZRX','ICX','LSK','REP','XLM','UBQ','BTG',
	'DGB','DASH','SALT','XVG','QTUM','DCR','POLY','NXT','PIVX','EOS','BURST'
]

var  wfees = {
    "KRKN": {"USDT": 5, "BCH": 0.0001, "DASH": 0.005, "EOS": 0.05, "ETH": 0.005, "LTC": 0.001, "BTC": 0.0005, "XLM": 0.00002, "XMR": 0.05, "XRP": 0.02, "ADA": 0.3},
    "BTHM": {"BCH": 0.001, "EOS": 0.1, "ETH": 0.01, "LTC": 0.01, "BTC": 0.001, "XMR": 0.05, "XRP": 1},
    "PLNX": {"USDT": 10, "BCH": 0.0001, "DASH": 0.01, "EOS": 0.00, "ETH": 0.01, "BTC": 0.0005, "XLM": 0.00001, "XMR": 0.015, "XRP": 0.15},
    "BTRX": {"USDT": 5, "BCH": 0.001, "DASH": 0.05, "ETH": 0.006, "LTC": 0.01, "BTC": 0.0005, "XLM": 0.01, "XMR": 0.04, "XRP": 0.02, "ADA": 0.3},
    "BINA": {"USDT": 3.3, "BCH": 0.0001, "DASH": 0.002, "EOS": 0.1, "ETH": 0.01, "LTC": 0.001, "BTC": 0.0005, "XLM": .01, "XMR": 0.0001, "XRP": 0.25, "ADA": 1},
    "HITB": {"USDT": 45, "BCH": 0.0018, "DASH": 0.03, "EOS": 0.01, "ETH": 0.00958, "LTC": 0.003, "BTC": 0.001, "XLM": 40, "XMR": 0.09, "XRP": 0.2},
    "HUBI": {"USDT": 20, "BCH": .0001, "DASH": .002, "EOS": 0.1, "ETH": 0.005, "LTC": 0.001, "BTC": 0.0005, "XLM": 0.01, "XRP": 0.1, "ADA": 1},
    "LIVE": {"BCH": 0.001, "DASH": 0.002,  "ETH": 0.01,  "BTC": 0.001},
    "EXMO": {"USDT": 5, "BCH": .001, "DASH": .01,  "ETH": 0.01, "LTC": 0.01, "BTC": 0.0005, "XLM": .01, "XMR": 0.05, "XRP": 0.02, "ADA": 1},
    "BTCM": {"BCH": .0001, "ETH": 0.001, "LTC": 0.001, "BTC": 0.0001, "XRP": 0.15}
  }
  
//Allowed exchanges [not enabled]
const allowed_exchanges = ['BTRX','BINA','CPIA']

//Primary USD sources [not enabled]
const usd_source = ['BTRX_USDT', 'BTRX_USD','BINA_USDT','CPIA_USDT','KRKN_USD']

//exchanges with reversed pairs data
const reversed_exchanges = ['BITS', 'PLNX']

//Minimum fee in case fee not supplied (need to add manual list)
const minfee = 0.002;
const tradesize_USD = 1000
const tradesize_BTC = 1
const sock = true;

var options = {
    hostname  : "sc-02.coinigy.com",    
    port      : "443",
	secure    : "true",
	autoReconnect: true,
	ackTimeout: 10000000
};



var channels  = [
	"ORDER-BINA--ADA--USDT",
	"ORDER-BINA--ADA--BTC",

	"ORDER-GDAX--BTC--USD",
	"ORDER-GDAX--LTC--USD",
	"ORDER-GDAX--ETH--USD",
	"ORDER-GDAX--ETH--BTC",
    "ORDER-GDAX--LTC--BTC",
    "ORDER-GDAX--XRP--BTC",
	
	"ORDER-BINA--BTC--USDT",
	"ORDER-BINA--ETH--USDT",
	"ORDER-BINA--LTC--USDT",
	"ORDER-BINA--ETH--BTC",
	"ORDER-BINA--LTC--ETH",
    "ORDER-BINA--XRP--BTC",
	
	"ORDER-BTRX--USDT--USD",
	"ORDER-BTRX--BTC--USDT",
	"ORDER-BTRX--ETH--USDT",
	"ORDER-BTRX--LTC--USDT",
	"ORDER-BTRX--ETH--BTC",
	"ORDER-BTRX--LTC--ETH",
    "ORDER-BTRX--LTC--BTC",
    "ORDER-BTRX--XRP--BTC",


]



async function dbo_addexchanges(data){
     // Drop the 'exchanges' collection from the current database first
    mongoose.connection.db.dropCollection('exchanges');
    return new Promise(resolve => {
	if(typeof data !== "undefined") {
		data.forEach(function (arrayItem) {
			let exch = new ExchangeModel({
				exch_code: arrayItem.exch_code,
				exch_id: arrayItem.exch_id,
				exch_name: arrayItem.exch_name,
				exch_fee: arrayItem.exch_fee,
				exch_trade_enabled: arrayItem.exch_trade_enabled,
				exch_balance_enabled: arrayItem.exch_balance_enabled,
				exch_url: arrayItem.exch_url
			  })
			  exch.save()
			  .then(doc => {
				//console.log(doc)
			  })
			  .catch(err => {
				console.error(err)
			  })
		})
    }
    resolve(console.log('successfully added exchanges to db.'));
});	
}

async function dbo_addAllMarkets(data){
    return new Promise(resolve => {
        // Drop the 'markets' collection from the current database first
    mongoose.connection.db.dropCollection('markets');
	if(typeof data !== "undefined") {
		data.forEach(function (arrayItem) {
			let market = new MarketModel({
                exch_id: arrayItem.exch_id,
                exch_name: arrayItem.exch_name,
				exch_code: arrayItem.exch_code,
				mkt_id: arrayItem.mkt_id,
				mkt_name: arrayItem.mkt_name,
				exchmkt_id: arrayItem.exchmkt_id
			  })
			  market.save()
			  .then(doc => {
				console.log(doc)
			  })
			  .catch(err => {
				console.error(err)
			  })
		})
    }
    resolve(console.log('successfully added markets to db.'));
});	
}

async function dbo_upsertorderbook(data){
    if (data[0] !== undefined){
        if ((data[0].exchange !== undefined) && (data[0].market !== undefined)){
        var exchange = data[0].exchange;
        var market = data[0].market;
        var timestamp = data[0].timestamp;
        var ordersarray = []
        data.forEach(function (arrayItem) {
            var order = {ordertype: arrayItem.ordertype, price: arrayItem.price, quantity: arrayItem.quantity, total: arrayItem.total}
            ordersarray.push(order)
        })
        let orderbook = { //<--- need to add mkt_id and exchmkd_id for easy lookup later
            exch_code: exchange,
            market: market,
            timestamp: timestamp,
            orders: ordersarray
        }
        let query = {exch_code: exchange, market: market};
        //console.log(query)
        let options = {upsert:true,setDefaultsOnInsert:true,runValidators:true,new:true}
        var o = OrderbookModel.findOneAndUpdate(query,orderbook,options)
        process.stdout.write(".");
        o.exec()
    }
}
}

async function dbo_addorderbook(data, channel) { //Add new trade data pairs from web sockets to database
    if ((!(data == undefined)) && (!(data[0].exchange == undefined)) && (!(data[0].market == undefined))){
        var exchange = data[0].exchange;
        var market = data[0].market;
        var timestamp = data[0].timestamp;
        var ordersarray = []
        data.forEach(function (arrayItem) {
            var order = {ordertype: arrayItem.ordertype, price: arrayItem.price, quantity: arrayItem.quantity, total: arrayItem.total}
            ordersarray.push(order)
        }) 
        //console.log('creating orderbook Model: ', exchange,timestamp,market,ordersarray.length)
        let orderbook = new OrderbookModel({ //<--- need to add mkt_id and exchmkd_id for easy lookup later
            exch_code: exchange,
            market: market,
            timestamp: timestamp,
            orders: ordersarray
        })
        orderbook.save()
              .then(doc => {
                console.log('saving orderbook to DB: ',  exchange,market,timestamp,ordersarray.length)
              })
              .catch(err => {
                console.error('save failed: ',err, exchange,market,timestamp,ordersarray.length)
              })
        } else {
            console.log('INVALID orderbook: ',  exchange,market,timestamp,ordersarray.length)
        }
}

function dumpfile(data, filename) {
	var fs = require('fs');
		fs.writeFile(filename, JSON.stringify(data, null, 2), function(err) {
			if(err) {
				return console.log(err);
			}
			console.log(data.length," records saved to file!");
		});
  } 



//var exchanges = new Exchanges(allowed_assets, reversed_exchanges); //exchange class (not scheme or DB )
//var Currencies = new Arbgraph(minfee); 
//var Books = new Orderbooks(minfee);     

async function dbo_addaccounts(data){
    // Drop the 'exchanges' collection from the current database first
    mongoose.connection.db.dropCollection('accounts');
    return new Promise(resolve => {
	if(typeof data !== "undefined") {
		data.forEach(function (arrayItem) {
			let account = new AccountModel({
                auth_id: arrayItem.auth_id,
                auth_key: arrayItem.auth_key,
                auth_optional1: arrayItem.auth_optional1,
                auth_nickname: arrayItem.auth_nickname,
                exch_name: arrayItem.exch_name,
                auth_secret: arrayItem.auth_secret,
                auth_updated: arrayItem.auth_updated,
                auth_active: arrayItem.auth_active,
                auth_trade: arrayItem.auth_trade,
                exch_trade_enabled: arrayItem.exch_trade_enabled,
                exch_id: arrayItem.exch_id
			  })
			  account.save()
			  .then(doc => {
				console.log(doc)
			  })
			  .catch(err => {
				console.error(err)
			  })
		})
    }
    resolve(console.log('successfully added exchanges to db.'));
});	
}

async function get_rest(url){
    // This example is in Node ES6 using the request-promise library
    return new Promise(resolve => {
    const rp = require('request-promise');
    const requestOptions = {
      method: 'POST',
      uri: url,
      headers: {
        'Content-Type': 'application/json',
        'X-API-KEY': api_credentials.apiKey,
        'X-API-SECRET': api_credentials.apiSecret
      }
    };
    rp(requestOptions).then(response => {
      resolve(JSON.parse(response));
    }).catch((err) => {
      console.log('API call error:', err.message);
    });
    });
}

async function db_init(){

    var dbo = new Dbaccess()
    return 
}

async function ws_init(options){
    // Setup main socket connection
    return new Promise(resolve => {
        var SCsocket = socketCluster.connect(options);
        SCsocket.on('connect', function (status) {
            console.log(status); 
        SCsocket.on('error', function (err) {
            console.log(err);
        }); 
        SCsocket.emit("auth", api_credentials, function (err, token) { 
            if (!err && token) {
                console.log('authentication successful.')
                //return socket connection
                resolve(SCsocket);
            } else {
                console.log(err)
            }
            });         
        });
    });
}

async function sub_private(SCsocket){
    return new Promise(resolve => {
    var privateCh = SCsocket.subscribe(api_credentials.privateChannel);
    privateCh.watch(function (data) {
        if(data.MessageType !== "Favorite") {
                console.log(data);
            }
        });
    resolve(privateCh);
    });
}

async function subChannels(){

        // Connect to Private channel and console output
        SCsocket.emit("auth", api_credentials, function (err, token) {        
            if (!err && token) {
                
                    var SC_orderbook = []
                    //get all exchanges 
                    SCsocket.emit("exchanges", null, function (err, data) {
                        //SCsocket.emit("markets", null, function (err, data) {
                        if (!err) {
                            var exchdata = JSON.parse(JSON.stringify(data));
                            //console.log(exchdata)
                            //exchanges.addexchanges(exchdata) //add to exchange class
                            //addExchToDB(exchdata)
                            // exchanges.addExchToDB(exchdata, ExchangeModel) //now add to DB
                            
                            // console.log('Exchanges found: ',exchanges.getexchangecount())
                             //dbo.addExchToDB(data)
        
                            // //console.log(exchanges.getexchanges())
                            // console.log('Bitfinex Info: ')
                            //console.log(exchanges.getexchangeinfo('BITF'))
                            //console.log(exchanges.exchangefee('BITF'))
                            //console.log(exchanges.tradeenabled())
                            //console.log(exchanges.getallchannels())
                            
                            // SCsocket.emit("channels", null, function (err, data) {
                            //     if (!err) {
                            // 		var channeldata = JSON.parse(JSON.stringify(data));
                            // 		console.log('Channel Data: ');
                            // 		console.log(channeldata);
                            // 	}});
         
            
                            // 		//exchanges.addmarkets(channeldata)
                            // 		// var count = 0
                            // 		// for (var x in data) {
                                        
                            // 		// 	var y = []
                            // 		// 	y.push(data[x])
                            // 		// 	//console.log(y)
                            // 		// 	dbo.addChannelsToDB(y)
                            // 		// 	console.log(count)
                            // 		// 	count++
                            // 		// }
        
                            // 		//dbo.addChannelsToDB(data)
        
                            // 		//dumpfile(data,channelcache)
                            // 		//dbo.addChannelsFromCache(channelcache)
        
                            // 		//console.log('market data from GDAX:')
                            //         //console.log(exchanges.getallchannels())
                            //         //console.log(exchanges.getmarkets('GDAX'))
                            //         //console.log(exchanges.getallpairs())
                            //         //console.log(exchanges.pair('LTC_BTC'))
                            //         //console.log(exchanges.tradeEnabledChannels())
                                    
                            // 		//Currencies.addExchDetails(exchanges.getexchanges())
                            //         //Currencies.addReversed(exchanges.getReversed())
                            // 		//startchannels(exchanges.tradeEnabledChannels())
        
                                    
                                    //startchannels(channels)							
                            //     } //not err
                            //     else {
                            //         console.log(err)
                            //     } //err
                            // }); //socket     
                        } else {
                                console.log(err)
                        }
                    });
                    
                    //begin order listeners
                    function startchannels(channels){
                    console.log('starting listeners...')
                    for (var pair in channels){
                        //console.log(channels[pair])
                        SC_orderbook[channels[pair]] = SCsocket.subscribe(channels[pair]);
                        //console.log('scanning exchanges...')
                        SC_orderbook[channels[pair]].watch(function (data) {
                            if(typeof data !== "undefined") {
                                console.log(data)
                                //dbo.addOrdersToDB(data, channels[pair])
        
                                //Currencies.addOrderBook(data,0);
                                //if (dbo(data) == -1)  console.log(channels[pair])
                                //dbo(data)
                                
                                //Books.addOrderBook(data)
                                //console.log(data[0])
        
        
        
                                
                                //dbo.addsampleorderbook(data, channels[pair])
                                //dbo(data)
                                //Books.query_currencies(['BTC/USD','BTC/USDT','ETH/BTC','LTC/BTC'])
                                
                                // console.log(Currencies)
                                // if ((Currencies.getnloops().length null)) {
                                //     'Detected undefined nloops'
                                // }
                                //console.log(Currencies.getnloops())
                                //console.log(Currencies.getArbLoops())
                                
                                // if (!(Currencies.getnloops() == 0)) {
                                //     console.log(Currencies.getArbLoops(tradesize_USD,tradesize_BTC))
                                //     //process.exit()
                                //     } 
                                
                            }
                        });
                    }
                    //end order listener
                    }			
                    
        
        
                     
        
        
                    //setInterval(function(){ console.log(Currencies.getArbPairs()) }, 5000); 
                    //setInterval(function(){ console.log(EthBTC.printMatrix) }, 1000);
                    
                } else {
                    console.log(err)
                }   
            });   
        };

async function startchannels(SCsocket,channels){
    var SC_orderbook = []
    console.log('starting orderbook channel listeners...')
    channels.forEach(function(pair){
        console.log('subscribing to:',pair)
        SC_orderbook[channels[pair]] = SCsocket.subscribe(pair);
        SC_orderbook[channels[pair]].watch(function (data) {
            if(typeof data !== "undefined") {
                //dbo_addorderbook(data, pair)
                //console.log(data[0].exchange,data[0].market)
                dbo_upsertorderbook(data)
            }
        });
    });
}

async function dbo_updateChannels(exchange,channels,exchlist){
    return new Promise((resolve,reject) => {
        let query = {exch_code: exchange}
        ExchangeModel.findOne(query,function(err, exch) {
            if(!err) {
                exch.channels = channels;
                exch.save(function(err) {
                    if(!err) {
                        console.log("Updating channels for: " + exch.exch_name);
                        
                        if (exchlist.includes(exchange)) {
                            resolve(channels) 
                        } else {
                            resolve()
                        }
                    }
                    else {
                        console.log("Error: could not save exchange " + exchange);
                        reject(err)
                    }
                });
            }
        });
        
    });
  }

function dbo_getchannels(exchange){
    return new Promise(resolve => {
        console.log('getting channels for:',exchange)
        let query = {exch_code: exchange}
        ExchangeModel.find(query,{channels: true}).then(channels => {
            resolve(channels[0].channels)
          })
    })
  }

async function update_channels(SCsocket,exchlist){
    return new Promise(resolve => {
        SCsocket.emit('channels','',function (err, data) { 
            //console.log(data)
            if (!err && data) {
                console.log('received channel list:',data.length)
                var orderchannels = {};
                data.forEach(function(arrayItem){
                    if (arrayItem.channel.search('ORDER') > -1){
                        let code = arrayItem.channel.split('-',2);
                        let exch = code[1].split('--')[0]
                        if (code[0] == 'ORDER'){
                            if (!(orderchannels.hasOwnProperty(exch))) orderchannels[exch] = [];
                            orderchannels[exch].push(arrayItem.channel)
                        }
                    }
                });
                let keys = Object.keys(orderchannels);
                var promises = []
                keys.forEach(function(exch){
                    //console.log(exch,orderchannels[exch],exchlist)
                     promises.push(dbo_updateChannels(exch,orderchannels[exch],exchlist))
                })
                console.log(promises.length)
                resolve(Promise.all(promises));
                //Promise.all(promises).then(resolve('All done...'))

            } else {
                console.log(err)
            }
   });
});
}

async function dropOrderbooks(){
    OrderbookModel.deleteMany({}, function(err) { 
        console.log('orderbooks purged') 
     });
}
async function dropMarkets(){
    MarketModel.deleteMany({}, function(err) { 
        console.log('markets purged') 
     });
}
async function dropExchanges(){
    ExchangeModel.deleteMany({}, function(err) { 
        console.log('exchanges purged') 
     });
}

async function dropAccounts(){
    AccountModel.deleteMany({}, function(err) { 
        console.log('accounts purged') 
     });
}

function exch_names(exchanges){
    var list = [];
    exchanges.forEach(function (arrayItem){
        if (arrayItem.exch_trade_enabled == 1) {
            list.push(arrayItem.exch_code)
        }
    });
    return(list)
}

async function main(exchlist){
    var soc = await ws_init(options);
    const reducer = (accumulator, currentValue) => accumulator.concat(currentValue);
    var chanlist = [];

    if (process.argv[3] !== 'save') {
        console.log('purging data...');
        await dropOrderbooks();
        await dropMarkets();
        await dropExchanges();
        await dropAccounts();
    }

    //var priv = await sub_private(soc);
    // var orders = await get_rest(rest.orders);

    var accounts = await get_rest(rest.accounts)
    await dbo_addaccounts(accounts.data);
    console.log(accounts)

    var markets = await get_rest(rest.markets)
    await dbo_addAllMarkets(markets.data);
    console.log(markets)

    var exchanges = await get_rest(rest.exchanges)
    await dbo_addexchanges(exchanges.data);
    //console.log(exchanges)

    if (exchlist == 'all') {
        console.log('all exchanges')
        let allexchanges = await exch_names(exchanges.data)
        console.log(allexchanges)
        let chan = await update_channels(soc,allexchanges)
        //console.log(Object.keys(chan))
        let filtered = chan.filter(function (el) {
            return el != null;
          });
        
        chanlist = filtered.reduce(reducer);
        //console.log(filtered)
    } else {
        //console.log('filtered list',exchlist)
        let chan = await update_channels(soc,exchlist)
        let filtered = chan.filter(function (el) {
            return el != null;
          });
        chanlist = filtered.reduce(reducer);
    }

   await startchannels(soc,chanlist)

}

main(process.argv[2]).catch(console.err)



