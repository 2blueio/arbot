const loopback = require('./loopback.js');
const BINANCE = require('./binancetradeservices.js');
const testdata = require('./testdata.js');





async function test() {


    var orders1 = [testorder1];
    var orders2 = [testorder2];
    var orders3 = [testorder3];
    var orders4 = [testorder4];
    //var results = await loopback.sendallstrategyorders(orders4);
    //console.log('Results query', results);
    //console.log('BATCH order 1:',orders1);
    //console.log('BATCH order 2:',orders2);
    //console.log('ORDER result:',results);
    await loopback.getallstrategies().then(function (result) { // (**)
        console.log('GET ALL:', result)
        result.forEach(result => {
            console.log(result.positions)
        });
        //alert(result); // 1
        return result * 2;

    })

    /*      .then(function(result) { // (***)
         
           alert(result); // 2
           return result * 2;
         
         }).then(function(result) {
         
           alert(result); // 4
           return result * 2;
         
         }); */

}


//var stratobj = LOOPBACK.createstrategyobj(process.argv[2]);
/* var strategy1 = loopback.createstrategy("BINANCE", "FUT", "TESTSTRAT1", "EOSUSDT", 3, "1h");
var strategy2 = loopback.createstrategy("BINANCE", "FUT", "TESTSTRAT2", "EOSUSDT", 3, "2h");
var strategy3 = loopback.createstrategy("BINANCE", "FUT", "FUNDING_TRADER", "FILUSDT", 1, "5m");
var strategy4 = loopback.createstrategy("BINANCE", "SPOT", "TACTICAL_ES", "ETHBTC", 3, "1m");
var strategy5 = 'BINANCE_FUT_TACTICALES_ETHUSDT_1m'; */
//var strategy = loopback.createstrategy('BINANCE', 'FUT', 'TACTICAL_ES', symbol, quantPrecision, arg_interval)
//var strategies = [strategy1, strategy2, strategy3];

function filterbyasset(strategies){ //curate list of strategies by removing same asset strategies
    assets = [];
    result = [];
    strategies.forEach( strategy => {
    var sym = strategy.symbol;
     assets[sym] = strategy;
    });
    for (const key in assets) {
        result.push(assets[key]);
    }
    return (result);
}

async function genorders () {
 var a = await loopback.getallstrategies();
 var s = filterbyasset(a);
return(s)
}

async function getinfo(exchtype){
var i = await BINANCE.exchinfo('SPOT');
var q = i['ETHBTC'].quantityPrecision;
console.log(q);
}

var roundOff = (num, places) => {
    const x = Math.pow(10, places);
    return Math.round(num * x) / x;
  }

async function calcposition(strategy) {
    var strategy = await loopback.getstrategy(strategy);

    var netunits = 0;
    var totalunits = 0;
    var val = 0;
    var pos = 0;
    strategy.positions.forEach(position => {
        if (!(position.executedQty == undefined )){
            console.log(parseFloat(position.executedQty));
            if (position.side == 'SELL') { pos = parseFloat(position.executedQty) *  -1 } else {(pos = parseFloat(position.executedQty))};
            val = val + Math.abs((parseFloat(position.avgPrice) * parseFloat(position.executedQty)));
            netunits = netunits + pos;
            totalunits = totalunits + Math.abs(parseFloat(position.executedQty));
        }
    });
    strategy.avgPrice = val / totalunits;
    strategy.units = roundOff(netunits,strategy.quantityPrecision);
    return (strategy);
}

function calcprofit(positions) {
    var val = 0;
    var dir = 0;
    var exitprice = 0;
    var netprofit = 0;
    var units = 0;
    positions.forEach(trade => {
        if ((trade.signal == 'LE1') || (trade.signal == 'LE2')) {
            val = val + (parseFloat(trade.executedQty) * parseFloat(trade.avgPrice));
            units = units + parseFloat(trade.executedQty);
            dir = 1;
        }
        if ((trade.signal == 'SE1') || (trade.signal == 'SE2')) {
            val = val + (parseFloat(trade.executedQty) * parseFloat(trade.avgPrice));
            units = units + parseFloat(trade.executedQty);
            dir = -1;
        }
        if ((trade.signal == 'LEX') || (trade.signal == 'SEX')) {
            exitprice = parseFloat(trade.avgPrice);
        }
    });

    netprofit = (exitprice * units) - val;
    if (dir == -1) {netprofit = netprofit * -1}
    return (netprofit)
}

var testpos = [
    {
      "signal": "LE1",
      "quantity": 0.021,
      "tradeid": 1605718860000,
      "clientOrderId": "BINANCE_FUT_TACTICALES_ETHUSDT_1m",
      "barclose": 471.93,
      "bartime": 1605718860000,
      "timestamp": "2020-11-18T17:01:00.000Z",
      "tradetype": "NEW",
      "ordertype": "MARKET",
      "symbol": "ETHUSDT",
      "side": "BUY",
      "orderId": 4264626750,
      "status": "FILLED",
      "price": "0",
      "avgPrice": "471.93000",
      "origQty": "0.021",
      "executedQty": "0.021",
      "cumQty": "0.021",
      "cumQuote": "9.91053",
      "timeInForce": "GTC",
      "type": "MARKET",
      "reduceOnly": false,
      "closePosition": false,
      "positionSide": "BOTH",
      "stopPrice": "0",
      "workingType": "CONTRACT_PRICE",
      "priceProtect": false,
      "origType": "MARKET",
      "updateTime": 1605718863210
    },
    {
      "signal": "LEX",
      "quantity": 0.021,
      "tradeid": 1605718860000,
      "clientOrderId": "BINANCE_FUT_TACTICALES_ETHUSDT_1m",
      "barclose": 474.21,
      "bartime": 1605719280000,
      "timestamp": "2020-11-18T17:08:00.000Z",
      "tradetype": "CLOSEALL",
      "ordertype": "MARKET",
      "symbol": "ETHUSDT",
      "side": "SELL",
      "orderId": 4264722490,
      "status": "FILLED",
      "price": "0",
      "avgPrice": "474.23000",
      "origQty": "0.021",
      "executedQty": "0.021",
      "cumQty": "0.021",
      "cumQuote": "9.95883",
      "timeInForce": "GTC",
      "type": "MARKET",
      "reduceOnly": false,
      "closePosition": false,
      "positionSide": "BOTH",
      "stopPrice": "0",
      "workingType": "CONTRACT_PRICE",
      "priceProtect": false,
      "origType": "MARKET",
      "updateTime": 1605719282479
    }
  ];

  var testpos2 = [
    {
      "signal": "SE1",
      "quantity": 0.021,
      "tradeid": 1605729480000,
      "clientOrderId": "BINANCE_FUT_TACTICALES_ETHUSDT_1m",
      "barclose": 473.25,
      "bartime": 1605729480000,
      "timestamp": "2020-11-18T19:58:00.000Z",
      "tradetype": "NEW",
      "ordertype": "MARKET",
      "symbol": "ETHUSDT",
      "side": "SELL",
      "orderId": 4266846988,
      "status": "FILLED",
      "price": "0",
      "avgPrice": "473.01000",
      "origQty": "0.021",
      "executedQty": "0.021",
      "cumQty": "0.021",
      "cumQuote": "9.93321",
      "timeInForce": "GTC",
      "type": "MARKET",
      "reduceOnly": false,
      "closePosition": false,
      "positionSide": "BOTH",
      "stopPrice": "0",
      "workingType": "CONTRACT_PRICE",
      "priceProtect": false,
      "origType": "MARKET",
      "updateTime": 1605729483241
    },
    {
      "signal": "SEX",
      "quantity": 0.021,
      "tradeid": 1605729480000,
      "clientOrderId": "BINANCE_FUT_TACTICALES_ETHUSDT_1m",
      "barclose": 471.4,
      "bartime": 1605729840000,
      "timestamp": "2020-11-18T20:04:00.000Z",
      "tradetype": "CLOSEALL",
      "ordertype": "MARKET",
      "symbol": "ETHUSDT",
      "side": "BUY",
      "orderId": 4266924397,
      "status": "FILLED",
      "price": "0",
      "avgPrice": "471.41000",
      "origQty": "0.021",
      "executedQty": "0.021",
      "cumQty": "0.021",
      "cumQuote": "9.89961",
      "timeInForce": "GTC",
      "type": "MARKET",
      "reduceOnly": false,
      "closePosition": false,
      "positionSide": "BOTH",
      "stopPrice": "0",
      "workingType": "CONTRACT_PRICE",
      "priceProtect": false,
      "origType": "MARKET",
      "updateTime": 1605729843486
    }
  ]

async function flattenpos(exch){
    var x = await getallstrategies();

}
const mode = process.argv[2];


if (mode == 'ga') {console.log('Getting all strategies'); loopback.getallstrategies().then(console.log) }
if (mode == 'da') {console.log('Deleting all Strategies'); loopback.deleteallstrategies().then(console.log)}
if (mode == 's') {console.log('Synching Strategies with live'); loopback.synclivepositions().then(console.log)}
if (mode == 'gs') {console.log('Getting Strategy'); loopback.getstrategy(strategy5).then(console.log)};

if ( mode == 'exf') {console.log('Get Futures Exchange Info'); BINANCE.exchinfo('FUT').then(console.log)}
if ( mode == 'exs') {console.log('Get Spot Exchange Info'); BINANCE.exchinfo('SPOT').then(console.log)}
if (mode == 'c') { console.log('Get Candles ETHBTC'); BINANCE.getcandles(strategy4).then(console.log)}

if (mode == 'calc') { console.log('calc'); calcposition(strategy5).then(console.log)}

if (mode == 'net') { console.log(calcprofit(testpos2))}
if (mode == 'l') { BINANCE.symlookup('BTCUSDT').then(console.log)}
if (mode == 'ff') { loopback.flattenstrategies({exchtype: 'FUT'}).then(console.log)}
if (mode  == 'gp') {BINANCE.getpositions(1).then(console.log)};
if (mode == 'd1') { loopback.deletestrategy({id:process.argv[3]}).then(console.log)}

