const api_credentials = require('./api_credentials.js');
var request = require('request');

// Subscribe to Private Channel (on website)n to get real-time updates of trades

// Private channel ID: 4C9A8CE2-64D3-979B-BAFA-3821C3E25630

// https://coinigy.docs.apiary.io/#reference/account-functions/list-order-types/ordertypes

// [
//     {
//       "data": {
//         "order_types": [
//           {
//             "order_type_id": "1",
//             "name": "Buy",
//             "order": "5"
//           },
//           {
//             "order_type_id": "2",
//             "name": "Sell",
//             "order": "10"
//           }
//         ],
//         "price_types": [
//           {
//             "price_type_id": "3",
//             "name": "Limit",
//             "order": "15"
//           },
//           {
//             "price_type_id": "6",
//             "name": "Stop (Limit)",
//             "order": "30"
//           },
//           {
//             "price_type_id": "8",
//             "name": "Limit (Margin)",
//             "order": "50"
//           },
//           {
//             "price_type_id": "9",
//             "name": "Stop Limit (Margin)",
//             "order": "60"
//           }
//         ]
//       },
//       "notifications": []
//     }
//   ]
//  https://api.coinigy.com/api/v1/exchanges
// [
//     {
//       "data": [
//         {
//           "exch_id": "2",
//           "exch_name": "BTC-e",
//           "exch_code": "BTCE",
//           "exch_fee": "0.003",
//           "exch_trade_enabled": "1",
//           "exch_balance_enabled": "1",
//           "exch_url": "https://btc-e.com/"
//         }
//     }
//  ]

function get_markets(exch_code,mkt_name){
    request({
        method: 'POST',
        url: 'https://api.coinigy.com/api/v1/markets',
        headers: {
          'Content-Type': 'application/json',
          'X-API-KEY': api_credentials.apiKey,
          'X-API-SECRET': api_credentials.apiSecret
        },
        body: "{  \"exchange_code\": " + JSON.stringify(exch_code) + "}"
      }, function (error, response, body) {
        console.log('Status:', response.statusCode);
        // console.log('Headers:', response.headers);
        // console.log('Response:', JSON.parse(body));
        var result = JSON.parse(body).data;
        result.forEach(function (arrayItem) {
            if (mkt_name !== undefined){
                if (mkt_name == arrayItem.mkt_name) console.log(arrayItem);
            } else {
                console.log(arrayItem);
            }
        });
      });
}

// [
//     {
//       "data": [
//         {
//           "exch_id": "62",
//           "exch_name": "Global Digital Asset Exchange",
//           "exch_code": "GDAX",
//           "mkt_id": "139",
//           "mkt_name": "BTC/CAD",
//           "exchmkt_id": "7432"
//         },
//         {
//           "exch_id": "62",
//           "exch_name": "Global Digital Asset Exchange",
//           "exch_code": "GDAX",
//           "mkt_id": "117",
//           "mkt_name": "BTC/EUR",
//           "exchmkt_id": "7433"
//         }
//     ]
// }
// ]

//https://api.coinigy.com/api/v1/accounts
function get_accounts(){
    request({
        method: 'POST',
        url: 'https://api.coinigy.com/api/v1/accounts',
        headers: {
          'Content-Type': 'application/json',
          'X-API-KEY': api_credentials.apiKey,
          'X-API-SECRET': api_credentials.apiSecret
        }
    }, 
        function (error, response, body) {
        console.log('Status:', response.statusCode);
        console.log('Headers:', response.headers);
        console.log('Response:', JSON.parse(body));
      });
}
//Response: auth_id 
// [
//     {
//       "data": [
//         {
//           "auth_id": "1234",
//           "auth_key": "77pWZtwrLACvMPLtWVTHFrK9FPCIhb2VXq9KhoVG7HW",
//           "auth_optional1": "",
//           "auth_nickname": "My Bitfinex Account",
//           "exch_name": "Bitfinex",
//           "auth_secret": "************************",
//           "auth_updated": "2016-02-20 05:08:55",
//           "auth_active": "1",
//           "auth_trade": "1",
//           "exch_trade_enabled": "1",
//           "exch_id": "7"
//         },


function mock_request (){
    request({
  method: 'POST',
  url: 'https://private-anon-babbd25894-coinigy.apiary-mock.com/api/v1/addOrder',
  headers: {
    'Content-Type': 'application/json',
    'X-API-KEY': api_credentials.apiKey,
    'X-API-SECRET': api_credentials.apiSecret
  },
  body: "{  \"auth_id\": 1234,  \"exch_id\": 2,  \"mkt_id\": 125,  \"order_type_id\": 2,  \"price_type_id\": 3,  \"limit_price\": 755,  \"order_quantity\": 0.01}"
}, function (error, response, body) {
  console.log('Status:', response.statusCode);
  console.log('Headers:', response.headers);
  console.log('Response:', JSON.parse(body));
});
}

function debug_add_order (){
    request({
        method: 'POST',
        url: 'https://private-anon-babbd25894-coinigy.apiary-proxy.com/api/v1/addOrder',
        headers: {
          'Content-Type': 'application/json',
          'X-API-KEY': api_credentials.apiKey,
          'X-API-SECRET': api_credentials.apiSecret
        },
        body: "{  \"auth_id\": 1234,  \"exch_id\": 62,  \"mkt_id\": 125,  \"order_type_id\": 2,  \"price_type_id\": 3,  \"limit_price\": 755,  \"order_quantity\": 0.01}"
      }, function (error, response, body) {
        console.log('Status:', response.statusCode);
        console.log('Headers:', response.headers);
        console.log('Response:', JSON.parse(body));
      });
}

function add_order(order){
    request({
        method: 'POST',
        url: 'https://api.coinigy.com/api/v1/addOrder',
        headers: {
          'Content-Type': 'application/json',
          'X-API-KEY': api_credentials.apiKey,
          'X-API-SECRET': api_credentials.apiSecret
        },
        // body: "{  \"auth_id\": 1234,  \"exch_id\": 62,  \"mkt_id\": 125,  \"order_type_id\": 1,  \"price_type_id\": 3,  \"limit_price\": 755,  \"order_quantity\": 0.01}"
        body: JSON.stringify(order)
      }, function (error, response, body) {
        console.log('Status:', response.statusCode);
        console.log('Headers:', response.headers);
        console.log('Response:', JSON.parse(body));
      });
}

// Return Data from http function:
// Response: { data: { internal_order_id: '210368174' },
//   notifications:
//    [ { notification_id: '125560923',
//        notification_type_title: 'Order Placed',
//        notification_type_message:
//         'Your order to %1$s %2$s %3$s on %4$s @ %5$s %3$s/%6$s has been placed',
//        notification_style: 'success',
//        notification_vars: 'Buy,0.10000000,LTC,Bittrex,0.01300000,BTC',
//        notification_title_vars: '2019-05-31 20:54:25',
//        notification_pinned: '0',
//        notification_sound: '0',
//        notification_sound_id: 'alertSound' } ] }

// Response: { data: { internal_order_id: '215121405' },
//   notifications:
//    [ { notification_id: '126788914',
//        notification_type_title: 'Order Error',
//        notification_type_message:
//         'There was an error placing your order to %1$s %2$s %3$s on %4$s @ %5$s %6$s<br/><br/> %7$s',
//        notification_style: 'error',
//        notification_vars:
//         'Buy,0.01000000,LTC,Bittrex,0.01200000,BTC,MIN_TRADE_REQUIREMENT_NOT_MET',
//        notification_title_vars: '2019-06-24 23:06:14',
//        notification_pinned: '0',
//        notification_sound: '0',
//        notification_sound_id: 'alertSound' } ] }

// Return data on websockets private channel:
// { MessageType: 'Notification',
//   Data:
//    { notification_id: 125560923,
//      type: 1,
//      title: 'Order Placed',
//      message:
//       'Your order to Buy 0.10000000 LTC on Bittrex @ 0.01300000 LTC/BTC has been placed',
//      style: 'success',
//      time: '2019-05-31T20:54:25',
//      message_vars: 'Buy,0.10000000,LTC,Bittrex,0.01300000,BTC',
//      title_vars: '2019-05-31 20:54:25',
//      pinned: false,
//      sound: false,
//      sound_id: 'alertSound',
//      sound_override: '' } }

// { MessageType: 'Notification',
//   Data:
//    { notification_id: 126788815,
//      type: 2,
//      title: 'Order Canceled',
//      message:
//       'Your order to Sell 0.0500000000 LTC on Bittrex @ 0.0124000000 LTC/BTC has been canceled',
//      style: 'success',
//      time: '2019-06-24T23:02:49',
//      message_vars: 'Sell,0.0500000000,LTC,Bittrex,0.0124000000,LTC/BTC,BTC',
//      title_vars: '2019-06-24 23:02:49',
//      pinned: false,
//      sound: false,
//      sound_id: 'alertSound',
//      sound_override: '' } }

// { MessageType: 'Notification',
//   Data:
//    { notification_id: 126788842,
//      type: 7,
//      title: 'Cancel Failed 2019-06-24 23:03:42',
//      message:
//       'Your order to Buy 0.1000000000 LTC on Bittrex has failed to cancel.  Please try again. <br/><br/> INVALID_ORDER',
//      style: 'error',
//      time: '2019-06-24T23:03:42',
//      message_vars:
//       'Buy,0.1000000000,LTC,Bittrex,0.0130000000,LTC/BTC,BTC,INVALID_ORDER',
//      title_vars: '2019-06-24 23:03:42',
//      pinned: false,
//      sound: false,
//      sound_id: 'alertSound',
//      sound_override: '' } }

// { MessageType: 'Notification',
//   Data:
//    { notification_id: 126788914,
//      type: 20,
//      title: 'Order Error',
//      message:
//       'There was an error placing your order to Buy 0.01000000 LTC on Bittrex @ 0.01200000 BTC<br/><br/> MIN_TRADE_REQUIREMENT_NOT_MET',
//      style: 'error',
//      time: '2019-06-24T23:06:14',
//      message_vars:
//       'Buy,0.01000000,LTC,Bittrex,0.01200000,BTC,MIN_TRADE_REQUIREMENT_NOT_MET',
//      title_vars: '2019-06-24 23:06:14',
//      pinned: false,
//      sound: false,
//      sound_id: 'alertSound',
//      sound_override: '' } }

function get_balances(auth_id){
    request({
        method: 'POST',
        url: 'https://api.coinigy.com/api/v1/refreshBalance',
        headers: {
          'Content-Type': 'application/json',
          'X-API-KEY': api_credentials.apiKey,
          'X-API-SECRET': api_credentials.apiSecret
        },
        body: JSON.stringify({'auth_id': auth_id}) //"{  \"auth_id\": auth_id}"
      }, function (error, response, body) {
        // console.log('Status:', response.statusCode);
        // console.log('Headers:', JSON.parse(response.headers));
        console.log('Response:', JSON.parse(body));
      });
}

function get_balance(auth_id,curr){
  return new Promise(resolve => {
  request({
      method: 'POST',
      url: 'https://api.coinigy.com/api/v1/refreshBalance',
      headers: {
        'Content-Type': 'application/json',
        'X-API-KEY': api_credentials.apiKey,
        'X-API-SECRET': api_credentials.apiSecret
      },
      body: JSON.stringify({'auth_id': auth_id}) //"{  \"auth_id\": auth_id}"
    }, function (error, response, body) {
        let result = JSON.parse(body)
        result.data.forEach(function (arrayItem) {
          if (arrayItem.balance_curr_code == curr){
            //console.log(arrayItem.balance_curr_code,arrayItem.balance_amount_avail)
            resolve(arrayItem.balance_amount_avail)
          }
        })
        resolve(-1)
    });
  });      
}

// Response: { data:
//   [ { balance_curr_code: 'BTC',
//       balance_amount_avail: '1.18202905',
//       balance_amount_held: '0',
//       balance_amount_total: '1.18202905',
//       btc_balance: '1.18202905',
//       last_price: '1.000000000000' },
//     { balance_curr_code: 'LTC',
//       balance_amount_avail: '8.05',
//       balance_amount_held: '0',
//       balance_amount_total: '8.05',
//       btc_balance: '0.098907774',
//       last_price: '0.012286680000' },
//     { balance_curr_code: 'USD',
//       balance_amount_avail: '0.00112937',
//       balance_amount_held: '0',
//       balance_amount_total: '0.00112937',
//       btc_balance: '0.0000001023886842',
//       last_price: '0.000090660000' } ],
//  notifications: [] }

function orders(){
    request({
        method: 'POST',
        url: 'https://api.coinigy.com/api/v1/orders',
        headers: {
          'Content-Type': 'application/json',
          'X-API-KEY': api_credentials.apiKey,
          'X-API-SECRET': api_credentials.apiSecret
        }}, function (error, response, body) {
        console.log('Status:', response.statusCode);
        console.log('Headers:', response.headers);
        //console.log('Response:', JSON.parse(body));
        var open_orders = JSON.parse(body).data.open_orders;
        console.log(open_orders)
      });
}

function cancel_order(internal_order_id){
    request({
        method: 'POST',
        url: 'https://api.coinigy.com/api/v1/cancelOrder',
        headers: {
          'Content-Type': 'application/json',
          'X-API-KEY': api_credentials.apiKey,
          'X-API-SECRET': api_credentials.apiSecret
        },
        body: JSON.stringify({'internal_order_id': internal_order_id}) //"{  \"internal_order_id\": 1234567}"
      }, function (error, response, body) {
        console.log('Status:', response.statusCode);
        console.log('Headers:', JSON.stringify(response.headers));
        console.log('Response:', body);
      });
}
//websocket response:
// { MessageType: 'Notification',
//   Data:
//    { notification_id: 126790011,
//      type: 2,
//      title: 'Order Canceled',
//      message:
//       'Your order to Sell 0.0500000000 LTC on Bittrex @ 0.0124000000 LTC/BTC has been canceled',
//      style: 'success',
//      time: '2019-06-24T23:43:33',
//      message_vars: 'Sell,0.0500000000,LTC,Bittrex,0.0124000000,LTC/BTC,BTC',
//      title_vars: '2019-06-24 23:43:32',
//      pinned: false,
//      sound: false,
//      sound_id: 'alertSound',
//      sound_override: '' } }

// http response:
// [
//     {
//       "data": [],
//       "notifications": [
//         {
//           "notification_id": "2493267",
//           "notification_type_title": "Order Canceled",
//           "notification_type_message": "Your order to %1$s %2$s %3$s on %4$s @ %5$s %6$s has been canceled",
//           "notification_style": "success",
//           "notification_vars": "Sell,0.0100000000,BTC,Global Digital Asset Exchange,755.0000000000,BTC/USD,USD",
//           "notification_title_vars": "2016-07-02 20:30:10",
//           "notification_pinned": "0",
//           "notification_sound": "0",
//           "notification_sound_id": "alertSound"
//         }
//       ]
//     }
//   ]

async function tradeorder(auth_id,mkt_id,from_curr,to_curr,amt,price,dir){
  var bal = await get_balance(auth_id,'LTC')
  console.log("Bal:",bal)
  if (bal >= amt) {
    var order_type_id = 0;
    console.log('Balance:',bal,'Tradesize:',amt)
    if (dir == 'Sell') {
      order_type_id = 2;
    } else {
      order_type_id = 1
    }
    var order = {  'auth_id': auth_id,  'exch_id': 15,  'mkt_id': mkt_id,  'order_type_id': 2,  'price_type_id': 3,  'limit_price': price,  'order_quantity': amt};
    console.log(order)
  }
  else {
    console.log('Not enough funds to execute order.')
  }
}
//var order = {  auth_id: 363693,  exch_id: 15,  mkt_id: 1,  order_type_id: 1,  price_type_id: 3,  limit_price: 0.012,  order_quantity: 0.05};
//var order = {  auth_id: 363693,  exch_id: 15,  mkt_id: 1,  order_type_id: 2,  price_type_id: 3,  limit_price: 0.0122,  order_quantity: 0.05};

// debug_request();
 //console.log(get_markets('BINA','LTC/BTC'));
//  get_accounts();
//get_balances(363693);
//get_balances(410776);
//console.log(get_balance(363693,'LTC'));
add_order(order);
//orders();
//cancel_order(215124066)
//tradeorder(363693,1,'LTC','BTC',0.1,0.0122,'Sell')

get_balance(363693,'LTC').then(console.log)
