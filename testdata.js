var x = [
  {
    "id": "BINANCE_FUT_TACTICALES_ETHUSDT_1m",
    "exchange": "BINANCE",
    "exchtype": "FUT",
    "strategy": "TACTICALES",
    "quantityPrecision": 3,
    "interval": "1m",
    "symbol": "ETHUSDT",
    "units": 0,
    "psize": 10,
    "avgPrice": 475.71059499999996,
    "openPl": 0,
    "cumPl": 0,
    "positions": [
      {
        "signal": "SE1",
        "quantity": 0.021,
        "tradeid": 1605732240000,
        "clientOrderId": "BINANCE_FUT_TACTICALES_ETHUSDT_1m",
        "barclose": 470.99,
        "bartime": 1605732240000,
        "timestamp": "2020-11-18T20:44:00.000Z",
        "tradetype": "NEW",
        "ordertype": "MARKET",
        "symbol": "ETHUSDT",
        "side": "SELL",
        "orderId": 4267471973,
        "status": "FILLED",
        "price": "0",
        "avgPrice": "470.98000",
        "origQty": "0.021",
        "executedQty": "0.021",
        "cumQty": "0.021",
        "cumQuote": "9.89058",
        "timeInForce": "GTC",
        "type": "MARKET",
        "reduceOnly": false,
        "closePosition": false,
        "positionSide": "BOTH",
        "stopPrice": "0",
        "workingType": "CONTRACT_PRICE",
        "priceProtect": false,
        "origType": "MARKET",
        "updateTime": 1605732242958
      },
      {
        "signal": "SEX",
        "quantity": 0.021,
        "tradeid": 1605732240000,
        "clientOrderId": "BINANCE_FUT_TACTICALES_ETHUSDT_1m",
        "barclose": 471.66,
        "bartime": 1605733200000,
        "timestamp": "2020-11-18T21:00:00.000Z",
        "tradetype": "CLOSEALL",
        "ordertype": "MARKET",
        "symbol": "ETHUSDT",
        "side": "BUY",
        "orderId": 4267690499,
        "status": "FILLED",
        "price": "0",
        "avgPrice": "471.66000",
        "origQty": "0.021",
        "executedQty": "0.021",
        "cumQty": "0.021",
        "cumQuote": "9.90486",
        "timeInForce": "GTC",
        "type": "MARKET",
        "reduceOnly": false,
        "closePosition": false,
        "positionSide": "BOTH",
        "stopPrice": "0",
        "workingType": "CONTRACT_PRICE",
        "priceProtect": false,
        "origType": "MARKET",
        "updateTime": 1605733203371
      },
      {
        "signal": "SE1",
        "quantity": 0.021,
        "tradeid": 1605733320000,
        "clientOrderId": "BINANCE_FUT_TACTICALES_ETHUSDT_1m",
        "barclose": 472.66,
        "bartime": 1605733320000,
        "timestamp": "2020-11-18T21:02:00.000Z",
        "tradetype": "NEW",
        "ordertype": "MARKET",
        "symbol": "ETHUSDT",
        "side": "SELL",
        "orderId": 4267713826,
        "status": "FILLED",
        "price": "0",
        "avgPrice": "472.65000",
        "origQty": "0.021",
        "executedQty": "0.021",
        "cumQty": "0.021",
        "cumQuote": "9.92565",
        "timeInForce": "GTC",
        "type": "MARKET",
        "reduceOnly": false,
        "closePosition": false,
        "positionSide": "BOTH",
        "stopPrice": "0",
        "workingType": "CONTRACT_PRICE",
        "priceProtect": false,
        "origType": "MARKET",
        "updateTime": 1605733322342
      },
      {
        "signal": "SEX",
        "quantity": 0.021,
        "tradeid": 1605733320000,
        "clientOrderId": "BINANCE_FUT_TACTICALES_ETHUSDT_1m",
        "barclose": 471.73,
        "bartime": 1605733980000,
        "timestamp": "2020-11-18T21:13:00.000Z",
        "tradetype": "CLOSEALL",
        "ordertype": "MARKET",
        "symbol": "ETHUSDT",
        "side": "BUY",
        "orderId": 4267843550,
        "status": "FILLED",
        "price": "0",
        "avgPrice": "471.73000",
        "origQty": "0.021",
        "executedQty": "0.021",
        "cumQty": "0.021",
        "cumQuote": "9.90633",
        "timeInForce": "GTC",
        "type": "MARKET",
        "reduceOnly": false,
        "closePosition": false,
        "positionSide": "BOTH",
        "stopPrice": "0",
        "workingType": "CONTRACT_PRICE",
        "priceProtect": false,
        "origType": "MARKET",
        "updateTime": 1605733982462
      },
      {
        "signal": "SE1",
        "quantity": 0.021,
        "tradeid": 1605734640000,
        "clientOrderId": "BINANCE_FUT_TACTICALES_ETHUSDT_1m",
        "barclose": 472.69,
        "bartime": 1605734640000,
        "timestamp": "2020-11-18T21:24:00.000Z",
        "tradetype": "NEW",
        "ordertype": "MARKET",
        "symbol": "ETHUSDT",
        "side": "SELL",
        "orderId": 4267938457,
        "status": "FILLED",
        "price": "0",
        "avgPrice": "472.68000",
        "origQty": "0.021",
        "executedQty": "0.021",
        "cumQty": "0.021",
        "cumQuote": "9.92628",
        "timeInForce": "GTC",
        "type": "MARKET",
        "reduceOnly": false,
        "closePosition": false,
        "positionSide": "BOTH",
        "stopPrice": "0",
        "workingType": "CONTRACT_PRICE",
        "priceProtect": false,
        "origType": "MARKET",
        "updateTime": 1605734642589
      },
      {
        "signal": "SEX",
        "quantity": 0.021,
        "tradeid": 1605734640000,
        "clientOrderId": "BINANCE_FUT_TACTICALES_ETHUSDT_1m",
        "barclose": 471.96,
        "bartime": 1605735120000,
        "timestamp": "2020-11-18T21:32:00.000Z",
        "tradetype": "CLOSEALL",
        "ordertype": "MARKET",
        "symbol": "ETHUSDT",
        "side": "BUY",
        "orderId": 4268011413,
        "status": "FILLED",
        "price": "0",
        "avgPrice": "471.96000",
        "origQty": "0.021",
        "executedQty": "0.021",
        "cumQty": "0.021",
        "cumQuote": "9.91116",
        "timeInForce": "GTC",
        "type": "MARKET",
        "reduceOnly": false,
        "closePosition": false,
        "positionSide": "BOTH",
        "stopPrice": "0",
        "workingType": "CONTRACT_PRICE",
        "priceProtect": false,
        "origType": "MARKET",
        "updateTime": 1605735122742
      },
      {
        "signal": "LE1",
        "quantity": 0.021,
        "tradeid": 1605736380000,
        "clientOrderId": "BINANCE_FUT_TACTICALES_ETHUSDT_1m",
        "barclose": 473.26,
        "bartime": 1605736380000,
        "timestamp": "2020-11-18T21:53:00.000Z",
        "tradetype": "NEW",
        "ordertype": "MARKET",
        "symbol": "ETHUSDT",
        "side": "BUY",
        "orderId": 4268226768,
        "status": "FILLED",
        "price": "0",
        "avgPrice": "473.26000",
        "origQty": "0.021",
        "executedQty": "0.021",
        "cumQty": "0.021",
        "cumQuote": "9.93846",
        "timeInForce": "GTC",
        "type": "MARKET",
        "reduceOnly": false,
        "closePosition": false,
        "positionSide": "BOTH",
        "stopPrice": "0",
        "workingType": "CONTRACT_PRICE",
        "priceProtect": false,
        "origType": "MARKET",
        "updateTime": 1605736382743
      },
      {
        "signal": "LEX",
        "quantity": 0.021,
        "tradeid": 1605736380000,
        "clientOrderId": "BINANCE_FUT_TACTICALES_ETHUSDT_1m",
        "barclose": 473.82,
        "bartime": 1605736680000,
        "timestamp": "2020-11-18T21:58:00.000Z",
        "tradetype": "CLOSEALL",
        "ordertype": "MARKET",
        "symbol": "ETHUSDT",
        "side": "SELL",
        "orderId": 4268259100,
        "status": "FILLED",
        "price": "0",
        "avgPrice": "473.81000",
        "origQty": "0.021",
        "executedQty": "0.021",
        "cumQty": "0.021",
        "cumQuote": "9.95001",
        "timeInForce": "GTC",
        "type": "MARKET",
        "reduceOnly": false,
        "closePosition": false,
        "positionSide": "BOTH",
        "stopPrice": "0",
        "workingType": "CONTRACT_PRICE",
        "priceProtect": false,
        "origType": "MARKET",
        "updateTime": 1605736682681
      },
      {
        "signal": "LE1",
        "quantity": 0.021,
        "tradeid": 1605737340000,
        "clientOrderId": "BINANCE_FUT_TACTICALES_ETHUSDT_1m",
        "barclose": 473.66,
        "bartime": 1605737340000,
        "timestamp": "2020-11-18T22:09:00.000Z",
        "tradetype": "NEW",
        "ordertype": "MARKET",
        "symbol": "ETHUSDT",
        "side": "BUY",
        "orderId": 4268325450,
        "status": "FILLED",
        "price": "0",
        "avgPrice": "473.67000",
        "origQty": "0.021",
        "executedQty": "0.021",
        "cumQty": "0.021",
        "cumQuote": "9.94707",
        "timeInForce": "GTC",
        "type": "MARKET",
        "reduceOnly": false,
        "closePosition": false,
        "positionSide": "BOTH",
        "stopPrice": "0",
        "workingType": "CONTRACT_PRICE",
        "priceProtect": false,
        "origType": "MARKET",
        "updateTime": 1605737342997
      },
      {
        "signal": "LEX",
        "quantity": 0.021,
        "tradeid": 1605737340000,
        "clientOrderId": "BINANCE_FUT_TACTICALES_ETHUSDT_1m",
        "barclose": 474.3,
        "bartime": 1605738180000,
        "timestamp": "2020-11-18T22:23:00.000Z",
        "tradetype": "CLOSEALL",
        "ordertype": "MARKET",
        "symbol": "ETHUSDT",
        "side": "SELL",
        "orderId": 4268422012,
        "status": "FILLED",
        "price": "0",
        "avgPrice": "474.30000",
        "origQty": "0.021",
        "executedQty": "0.021",
        "cumQty": "0.021",
        "cumQuote": "9.96030",
        "timeInForce": "GTC",
        "type": "MARKET",
        "reduceOnly": false,
        "closePosition": false,
        "positionSide": "BOTH",
        "stopPrice": "0",
        "workingType": "CONTRACT_PRICE",
        "priceProtect": false,
        "origType": "MARKET",
        "updateTime": 1605738183274
      },
      {
        "signal": "LE1",
        "quantity": 0.021,
        "tradeid": 1605741420000,
        "clientOrderId": "BINANCE_FUT_TACTICALES_ETHUSDT_1m",
        "barclose": 478.59,
        "bartime": 1605741420000,
        "timestamp": "2020-11-18T23:17:00.000Z",
        "tradetype": "NEW",
        "ordertype": "MARKET",
        "symbol": "ETHUSDT",
        "side": "BUY",
        "orderId": 4268976834,
        "status": "FILLED",
        "price": "0",
        "avgPrice": "478.59000",
        "origQty": "0.021",
        "executedQty": "0.021",
        "cumQty": "0.021",
        "cumQuote": "10.05039",
        "timeInForce": "GTC",
        "type": "MARKET",
        "reduceOnly": false,
        "closePosition": false,
        "positionSide": "BOTH",
        "stopPrice": "0",
        "workingType": "CONTRACT_PRICE",
        "priceProtect": false,
        "origType": "MARKET",
        "updateTime": 1605741422846
      },
      {
        "signal": "LE2",
        "quantity": 0.021,
        "tradeid": 1605741420000,
        "clientOrderId": "BINANCE_FUT_TACTICALES_ETHUSDT_1m",
        "barclose": 478.01,
        "bartime": 1605741600000,
        "timestamp": "2020-11-18T23:20:00.000Z",
        "tradetype": "ADD",
        "ordertype": "MARKET",
        "symbol": "ETHUSDT",
        "side": "BUY",
        "orderId": 4269037433,
        "status": "FILLED",
        "price": "0",
        "avgPrice": "477.87000",
        "origQty": "0.021",
        "executedQty": "0.021",
        "cumQty": "0.021",
        "cumQuote": "10.03527",
        "timeInForce": "GTC",
        "type": "MARKET",
        "reduceOnly": false,
        "closePosition": false,
        "positionSide": "BOTH",
        "stopPrice": "0",
        "workingType": "CONTRACT_PRICE",
        "priceProtect": false,
        "origType": "MARKET",
        "updateTime": 1605741602595
      },
      {
        "signal": "LEX",
        "quantity": 0.042,
        "tradeid": 1605741420000,
        "clientOrderId": "BINANCE_FUT_TACTICALES_ETHUSDT_1m",
        "barclose": 479.34,
        "bartime": 1605742620000,
        "timestamp": "2020-11-18T23:37:00.000Z",
        "tradetype": "CLOSEALL",
        "ordertype": "MARKET",
        "symbol": "ETHUSDT",
        "side": "SELL",
        "orderId": 4269245622,
        "status": "FILLED",
        "price": "0",
        "avgPrice": "479.34000",
        "origQty": "0.042",
        "executedQty": "0.042",
        "cumQty": "0.042",
        "cumQuote": "20.13228",
        "timeInForce": "GTC",
        "type": "MARKET",
        "reduceOnly": false,
        "closePosition": false,
        "positionSide": "BOTH",
        "stopPrice": "0",
        "workingType": "CONTRACT_PRICE",
        "priceProtect": false,
        "origType": "MARKET",
        "updateTime": 1605742622805
      },
      {
        "signal": "LE1",
        "quantity": 0.021,
        "tradeid": 1605743820000,
        "clientOrderId": "BINANCE_FUT_TACTICALES_ETHUSDT_1m",
        "barclose": 479.56,
        "bartime": 1605743820000,
        "timestamp": "2020-11-18T23:57:00.000Z",
        "tradetype": "NEW",
        "ordertype": "MARKET",
        "symbol": "ETHUSDT",
        "side": "BUY",
        "orderId": 4269405200,
        "status": "FILLED",
        "price": "0",
        "avgPrice": "479.58000",
        "origQty": "0.021",
        "executedQty": "0.021",
        "cumQty": "0.021",
        "cumQuote": "10.07118",
        "timeInForce": "GTC",
        "type": "MARKET",
        "reduceOnly": false,
        "closePosition": false,
        "positionSide": "BOTH",
        "stopPrice": "0",
        "workingType": "CONTRACT_PRICE",
        "priceProtect": false,
        "origType": "MARKET",
        "updateTime": 1605743822991
      },
      {
        "signal": "LEX",
        "quantity": 0.021,
        "tradeid": 1605743820000,
        "clientOrderId": "BINANCE_FUT_TACTICALES_ETHUSDT_1m",
        "barclose": 480.67,
        "bartime": 1605744120000,
        "timestamp": "2020-11-19T00:02:00.000Z",
        "tradetype": "CLOSEALL",
        "ordertype": "MARKET",
        "symbol": "ETHUSDT",
        "side": "SELL",
        "orderId": 4269471675,
        "status": "FILLED",
        "price": "0",
        "avgPrice": "480.62190",
        "origQty": "0.021",
        "executedQty": "0.021",
        "cumQty": "0.021",
        "cumQuote": "10.09306",
        "timeInForce": "GTC",
        "type": "MARKET",
        "reduceOnly": false,
        "closePosition": false,
        "positionSide": "BOTH",
        "stopPrice": "0",
        "workingType": "CONTRACT_PRICE",
        "priceProtect": false,
        "origType": "MARKET",
        "updateTime": 1605744123157
      },
      {
        "signal": "LE1",
        "quantity": 0.021,
        "tradeid": 1605744600000,
        "clientOrderId": "BINANCE_FUT_TACTICALES_ETHUSDT_1m",
        "barclose": 478.05,
        "bartime": 1605744600000,
        "timestamp": "2020-11-19T00:10:00.000Z",
        "tradetype": "NEW",
        "ordertype": "MARKET",
        "symbol": "ETHUSDT",
        "side": "BUY",
        "orderId": 4269579990,
        "status": "FILLED",
        "price": "0",
        "avgPrice": "478.12000",
        "origQty": "0.021",
        "executedQty": "0.021",
        "cumQty": "0.021",
        "cumQuote": "10.04052",
        "timeInForce": "GTC",
        "type": "MARKET",
        "reduceOnly": false,
        "closePosition": false,
        "positionSide": "BOTH",
        "stopPrice": "0",
        "workingType": "CONTRACT_PRICE",
        "priceProtect": false,
        "origType": "MARKET",
        "updateTime": 1605744603217
      },
      {
        "signal": "LEX",
        "quantity": 0.021,
        "tradeid": 1605744600000,
        "clientOrderId": "BINANCE_FUT_TACTICALES_ETHUSDT_1m",
        "barclose": 477.57,
        "bartime": 1605746460000,
        "timestamp": "2020-11-19T00:41:00.000Z",
        "tradetype": "CLOSEALL",
        "ordertype": "MARKET",
        "symbol": "ETHUSDT",
        "side": "SELL",
        "orderId": 4270011794,
        "status": "FILLED",
        "price": "0",
        "avgPrice": "477.63000",
        "origQty": "0.021",
        "executedQty": "0.021",
        "cumQty": "0.021",
        "cumQuote": "10.03023",
        "timeInForce": "GTC",
        "type": "MARKET",
        "reduceOnly": false,
        "closePosition": false,
        "positionSide": "BOTH",
        "stopPrice": "0",
        "workingType": "CONTRACT_PRICE",
        "priceProtect": false,
        "origType": "MARKET",
        "updateTime": 1605746462761
      },
      {
        "signal": "SE1",
        "quantity": 0.021,
        "tradeid": 1605746520000,
        "clientOrderId": "BINANCE_FUT_TACTICALES_ETHUSDT_1m",
        "barclose": 478.12,
        "bartime": 1605746520000,
        "timestamp": "2020-11-19T00:42:00.000Z",
        "tradetype": "NEW",
        "ordertype": "MARKET",
        "symbol": "ETHUSDT",
        "side": "SELL",
        "orderId": 4270029434,
        "status": "FILLED",
        "price": "0",
        "avgPrice": "478.09000",
        "origQty": "0.021",
        "executedQty": "0.021",
        "cumQty": "0.021",
        "cumQuote": "10.03989",
        "timeInForce": "GTC",
        "type": "MARKET",
        "reduceOnly": false,
        "closePosition": false,
        "positionSide": "BOTH",
        "stopPrice": "0",
        "workingType": "CONTRACT_PRICE",
        "priceProtect": false,
        "origType": "MARKET",
        "updateTime": 1605746523266
      },
      {
        "signal": "SEX",
        "quantity": 0.021,
        "tradeid": 1605746520000,
        "clientOrderId": "BINANCE_FUT_TACTICALES_ETHUSDT_1m",
        "barclose": 478.33,
        "bartime": 1605747840000,
        "timestamp": "2020-11-19T01:04:00.000Z",
        "tradetype": "CLOSEALL",
        "ordertype": "MARKET",
        "symbol": "ETHUSDT",
        "side": "BUY",
        "orderId": 4270273216,
        "status": "FILLED",
        "price": "0",
        "avgPrice": "478.33000",
        "origQty": "0.021",
        "executedQty": "0.021",
        "cumQty": "0.021",
        "cumQuote": "10.04493",
        "timeInForce": "GTC",
        "type": "MARKET",
        "reduceOnly": false,
        "closePosition": false,
        "positionSide": "BOTH",
        "stopPrice": "0",
        "workingType": "CONTRACT_PRICE",
        "priceProtect": false,
        "origType": "MARKET",
        "updateTime": 1605747843016
      }
    ],
    "details": [
      {
        "symbol": "ETHUSDT",
        "status": "TRADING",
        "maintMarginPercent": "2.5000",
        "requiredMarginPercent": "5.0000",
        "baseAsset": "ETH",
        "quoteAsset": "USDT",
        "marginAsset": "USDT",
        "pricePrecision": 2,
        "quantityPrecision": 3,
        "baseAssetPrecision": 8,
        "quotePrecision": 8,
        "underlyingType": "COIN",
        "underlyingSubType": [],
        "settlePlan": 0,
        "triggerProtect": "0.0500",
        "filters": [
          {
            "minPrice": "0.01",
            "maxPrice": "100000",
            "filterType": "PRICE_FILTER",
            "tickSize": "0.01"
          },
          {
            "stepSize": "0.001",
            "filterType": "LOT_SIZE",
            "maxQty": "10000",
            "minQty": "0.001"
          },
          {
            "stepSize": "0.001",
            "filterType": "MARKET_LOT_SIZE",
            "maxQty": "10000",
            "minQty": "0.001"
          },
          {
            "limit": 200,
            "filterType": "MAX_NUM_ORDERS"
          },
          {
            "limit": 100,
            "filterType": "MAX_NUM_ALGO_ORDERS"
          },
          {
            "multiplierDown": "0.8500",
            "multiplierUp": "1.1500",
            "multiplierDecimal": "4",
            "filterType": "PERCENT_PRICE"
          }
        ],
        "orderTypes": [
          "LIMIT",
          "MARKET",
          "STOP",
          "STOP_MARKET",
          "TAKE_PROFIT",
          "TAKE_PROFIT_MARKET",
          "TRAILING_STOP_MARKET"
        ],
        "timeInForce": [
          "GTC",
          "IOC",
          "FOK",
          "GTX"
        ]
      }
    ],
    "timestamp": "2020-11-19T01:04:02.399Z"
  }
]


var combinedorder = { strategy:
    { id: 'BINANCE_SPOT_TRENDWEAVER_ETHBTC_15m',
      exchange: 'BINANCE',
      exchtype: 'SPOT',
      strategy: 'TRENDWEAVER',
      quantityPrecision: 3,
      interval: '15m',
      symbol: 'ETHBTC',
      units: 0,
      currentpos: 0,
      psize: 0.001,
      avgPrice: 0,
      openPl: 0,
      cumPl: 0,
      positions: [],
      historical: [],
      details: [ [Object] ],
      timestamp: '2020-11-23T00:18:08.631Z' },
   signal: 'LE1',
   quantity: 0.032,
   currentpos: 1,
   tradeid: 0,
   clientOrderId: 'BINANCE_SPOT_TRENDWEAVER_ETHBTC_15m',
   barclose: 0.03131,
   bartime: 1606103100000,
   timestamp: '',
   tradetype: 'SYNC',
   ordertype: 'MARKET',
   symbol: 'ETHBTC',
   side: 'BUY',
   orderId: 1063295445,
   orderListId: -1,
   transactTime: 1606103648242,
   price: '0.00000000',
   origQty: '0.03200000',
   executedQty: '0.03200000',
   cummulativeQuoteQty: '0.00100201',
   status: 'FILLED',
   timeInForce: 'GTC',
   type: 'MARKET',
   fills:
    [ { price: '0.03131300',
        qty: '0.03200000',
        commission: '0.00003200',
        commissionAsset: 'ETH',
        tradeId: 202538081 } ] }

exports.x = x;
exports.combinedorder = combinedorder;